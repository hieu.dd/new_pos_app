export const DEFAULT_CONFIG = {
  factor: 0.81818181818,
  commission: [
    {
      range: {
        max: { value: 100000000, inclusive: false },
      },
      percentage: 3,
    },
    {
      range: {
        min: { value: 100000000, inclusive: true },
        max: { value: 200000000, inclusive: false },
      },
      percentage: 3.5,
    },
    {
      range: {
        min: { value: 200000000, inclusive: true },
        max: { value: 500000000, inclusive: false },
      },
      percentage: 4,
    },
    {
      range: {
        min: { value: 500000000, inclusive: true },
      },
      percentage: 5,
    },
  ],
};
