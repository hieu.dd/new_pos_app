// APP common short messages
export const loadingData = 'Đang tải dữ liệu';
export const load_data_error = 'Có lỗi xảy ra khi tải dữ liệu!';
export const no_matching_results = 'Không có kết quả tương ứng!';
export const request_timed_out = 'Thời gian server phản hồi quá lâu';
export const encounter_error = 'Gặp lỗi khi xử lý!';
export const see_detail = 'Xem chi tiết';
export const agree = 'Đồng ý';
export const retry = 'Thử lại';
export const ok = 'OK';
export const close = 'Đóng';
export const confirm = 'Xác nhận';
export const choose = 'Chọn';
export const no = 'Không';
export const yes = 'Có';
export const sure = 'Chắc chắn';
export const error = 'Lỗi';
export const back = 'Quay lại';
export const save = 'Lưu';
export const update = 'Cập nhật';
export const restart = 'Khởi động lại';
export const ignore = 'Bỏ qua';
export const later = 'Để sau';
export const setup = 'Cài đặt';
export const cancel = 'Hủy';
export const warning = 'Cảnh báo';
export const success = 'Thành công';
export const loading = 'Đang tải...';
export const something_went_wrong = 'Có lỗi xảy ra';
export const logout = 'Đăng xuất';
export const inProcess = 'Tính năng đang được phát triển';
export const logout_waring = 'Bạn có chắc muốn đăng xuất khỏi ứng dụng?';

// CUSTOMER
export const searching_customer = 'Đang tìm khách hàng';
export const customer_not_found = 'Không tìm thấy thông tin khách hàng';
export const checking_customer = 'Đang kiểm tra thông tin khách hàng';
export const creating_new_customer = 'Đang tạo mới khách hàng';
export const caption_gcafe_id = 'GCafe ID';
export const caption_customer_phone = 'Số điện thoại *';
export const caption_customer_name = 'Họ và tên *';
export const caption_receiver_phone = 'SĐT người nhận *';
export const caption_receiver_name = 'Tên người nhận *';
export const caption_receiver_province = 'Tỉnh/Thành phố *';
export const caption_receiver_district = 'Quận/Huyện *';
export const caption_receiver_street = 'Địa chỉ *';
export const caption_receiver_date = 'Chọn ngày *';
export const caption_receiver_time = 'Chọn giờ *';
export const caption_receiver_tax_code = 'Mã số thuế *';
export const caption_receiver_company_name = 'Tên công ty *';
export const caption_receiver_company_address = 'Địa chỉ cty *';
export const placeholder_gcafe_id = 'Nhập ID';
export const placeholder_customer_phone = '039 123 4567';
export const placeholder_customer_name = 'Nguyễn Văn A';
export const placeholder_receiver_phone = '039 123 4567';
export const placeholder_receiver_name = 'Nguyễn Văn A';
export const placeholder_receiver_province = 'Nhập tên tỉnh/thành phố';
export const placeholder_receiver_district = 'Nhập tên quận/huyện';
export const placeholder_receiver_street = 'Số nhà, ngõ, đường/phố';
export const placeholder_receiver_tax_code = 'Mã số thuế';
export const placeholder_receiver_company_name = 'Tên công ty';
export const placeholder_receiver_company_address = 'Địa chỉ cty';

// SALE
export const change_promotion_warning_w_voucher = 'Bạn cần bỏ chọn phiếu mua hàng để có thể chọn khuyến mãi';
export const apply_voucher_warning =
  'Các khuyến mãi khác có thể bị mất nếu áp dụng chương trình này. Bạn có muốn tiếp tục không?';
export const apply_voucher_code_warning = 'Các khuyến mãi khác có thể bị mất nếu áp mã giảm giá. Bạn có muốn tiếp tục không?';
export const use_voucher_code_warning = 'Các khuyến mãi khác có thể bị loại bỏ nếu sử dụng mã giảm giá';
export const sale_out_of_stock = 'Kiểm tra lại thông tin CTKM (sản phẩm, số lượng KM,...).';
export const product_sale_changed = 'Chương trình khuyến mãi không còn khả dụng:';
export const sale_error_checking_quantity = 'Gặp lỗi khi kiểm tra số lượng khuyến mãi.\nVui lòng thử lại!';
export const please_recheck_cart = 'Vui lòng kiểm tra lại giỏ hàng!';
export const add_product_with_voucher_warning =
  'Khuyến mãi trên sản phẩm không được dùng chung với phiếu mua hàng/mã giảm giá. Vui lòng bỏ phiếu mua hàng/mã giảm giá để có thể sử dụng chương trình khuyến mãi sản phẩm';
export const checking_propose_value = 'Đang kiểm tra giá trị xin giảm giá';
export const check_propose_value_failure = 'Có lỗi xảy ra khi kiểm tra giá trị xin giảm giá. Xin vui lòng thử lại';
export const invalid_cart_revenue = 'Đơn hàng không đảm bảo lợi nhuận, Xin kiểm tra lại';

// PRODUCT
export const exceed_promotion_quantity = 'Vượt quá số lượng khuyến mãi';
export const refresh_exceed_product_promotion = 'Kiểm tra lại thông tin CTKM (sản phẩm, số lượng KM,...).';

// CART
export const invalid_cart_invoice = 'Không thể đặt đơn với giỏ hàng có giá trị bằng 0';
export const maximum_quantity = 'Số lượng sản phẩm không được phép lớn hơn 999';
export const empty_cart = 'Bạn không có sản phẩm nào trong giỏ.';
export const shopping_now = 'Bắt đầu mua sắm';
export const grand_discount_warning = 'Giá trị xin giảm giá toàn đơn phải nhỏ hơn 10% giá trị đơn hàng!';
export const propose_without_import_price =
  'Bạn không thể xin giảm giá cho SP này vì chưa có giá nhập, liên hệ ngành hàng để bổ sung';

// ORDER
export const creating_order = 'Đang gửi đơn hàng';
export const creating_approval_order = 'Đang gửi đơn hàng chờ phê duyệt';
export const creating_quotation = 'Đang gửi báo giá';
export const create_order_success = 'Gửi đơn hàng thành công';
export const create_order_failure = 'Gửi đơn hàng thất bại';
export const create_order_error = 'Lỗi tạo đơn hàng';
export const create_quotation_success = 'Gửi báo giá thành công';
export const create_quotation_failure = 'Gửi báo giá thất bại';
export const creating_approval_order_success = 'Gửi đơn hàng chờ phê duyệt thành công';
export const creating_approval_order_failure = 'Gửi đơn hàng chờ phê duyệt thất bại';
export const approving_order = 'Đang duyệt đơn hàng';
export const approve_order_success = 'Duyệt đơn hàng thành công';
export const approve_order_failure = 'Duyệt đơn hàng thất bại';
export const rejecting_order = 'Đang từ chối đơn hàng';
export const reject_order_success = 'Từ chối hàng thành công';
export const reject_order_failure = 'Từ chối đơn hàng thất bại';
export const magento_error = 'Tạo đơn trên magento thất bại, liên hệ đội kỹ thuật để xử lý';

// COMBO
export const warning_change_combo = 'Tổng giá trị sản phẩm lớn hơn giá trị combo đã chọn. Bạn có muốn chọn combo khác không?';
export const remove_voucher_when_create_combo = 'Phiếu mua hàng sẽ không được áp dụng khi chọn combo';

// ENUMS...
export const screen_title = {
  product_list: 'Danh mục sản phẩm',
  product_detail: 'Chi tiết sản phẩm',
  select_category: 'Chọn danh mục sản phẩm',
};

export const tab_titles = {
  home: 'Trang chủ',
  cart: 'Giỏ hàng',
  wallet: 'Ví',
  user: 'Cá nhân',
  sale: 'Bán hàng',
};
