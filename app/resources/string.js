// APP common short messages
export const loading = 'Đang tải dữ liệu';

export const screen_title = {
  product_list: 'Danh mục sản phẩm',
  product_detail: 'Chi tiết sản phẩm',
  select_category: 'Chọn danh mục sản phẩm',
};
