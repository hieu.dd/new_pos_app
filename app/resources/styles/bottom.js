import { StyleSheet } from 'react-native';
import { screen } from './common';
import { scale } from '../../utils/scaling';

export default StyleSheet.create({
  sampleStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: screen.width,
    height: screen.height * 0.17,
    borderColor: screen.lineColor,
  },
});
