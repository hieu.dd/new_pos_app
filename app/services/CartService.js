import api from './api';
import url, { parse, format } from 'url';
import config from '../config';
import * as utils from '../utils';

class CartService {
  async createOrderOM(params) {
    return api.post(
      url.format({
        protocol: 'http',
        host: config.hosts.om,
        pathname: '/rpc',
      }),
      {
        method: 'Create',
        jsonrpc: '2.0',
        params,
      },
      {
        AuthorizationByBearerToken: true,
      }
    );
  }

  async creatPayNVPAYQR(params) {
    return api.post(
      url.format({
        protocol: 'http',
        host: config.hosts.om,
        pathname: '/rpc',
      }),
      {
        method: 'PayVNPAYQR',
        jsonrpc: '2.0',
        params,
      },
      {
        AuthorizationByBearerToken: true,
      }
    );
  }
}

export default new CartService();
