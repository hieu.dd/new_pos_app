import Axios from 'axios';
import Base64 from 'base-64';
import config from '../config';
import * as string from '../resources/string';
import Logger from '../utils/logger';

const FETCH_TIMEOUT = 30000;
const UPLOAD_TIMEOUT = 120000;

class Api {
  uid = 0;
  accessToken = 'c9b78a70fe1e468cac6365cb01abb847';
  authHeader = '';

  constructor() {}

  setUidAndToken(uid, accessToken) {
    this.uid = uid;
    this.accessToken = accessToken;
    this.authHeader = 'Basic ' + Base64.encode(this.accessToken + ':' + ''); // username is token, and password is empty
    Logger.write(`API initialized (access_token = ${this.accessToken}, uid = ${this.uid}, auth_header = ${this.authHeader})`);
  }

  getAccessToken() {
    return this.accessToken;
  }

  areHeadersReady() {
    return this.accessToken && this.authHeader && this.uid;
  }

  makeHeader(headers) {
    let h = { ...headers };
    if (headers['TEKO-UAT']) {
      h['TEKO-UAT'] = this.accessToken;
    }
    if (headers.Authorization) {
      h['Authorization'] = this.authHeader;
    }
    if (headers.AuthorizationByBearerToken) {
      h['Authorization'] = 'Bearer ' + this.getAccessToken();
      delete h.AuthorizationByBearerToken;
    }
    if (headers.BasicAuth) {
      h['Authorization'] = 'Basic ' + Base64.encode('admin:ewqdsacxz');
    }
    // if (headers.uid) {
    //   h['uid'] = this.uid;
    // }
    Logger.write('Headers: ', h);
    return h;
  }

  logRequest(method, url, params) {
    Logger.write(`REQUEST[${method}] for URL: ${url}`);
    Logger.write('Params: ', params);
  }

  _fetch(config) {
    return new Promise((resolve, reject) => {
      let finished = false;

      Axios({ ...config, timeout: FETCH_TIMEOUT })
        .then(onSuccessCallback)
        .catch(onErrorCallback);

      function onSuccessCallback(response) {
        logResponse(response, '');
        if (response.status >= 200 && response.status <= 206) {
          if (!finished) {
            resolve({
              status: response.status,
              headers: response.headers,
              data: response.data,
            });
            finished = true;
          }
        } else {
          if (!finished) {
            reject({
              status: response.status,
              headers: response.headers,
              data: response.data,
            });
            finished = true;
          }
        }
      }

      function onErrorCallback(error) {
        let errorString = JSON.stringify(error);
        let errorJson = JSON.parse(errorString);
        logResponse(errorJson, error.message);
        if (!finished) {
          reject({
            status: -1,
            headers: {},
            data: errorString,
          });
          finished = true;
        }
      }

      function logResponse(response, text) {
        Logger.write(`RESPONSE for URL: `, response);
        try {
          let jsonText = JSON.parse(text);
          Logger.write('Data: ', jsonText);
        } catch (error) {
          Logger.write(`Data: ${text}`);
        }
      }
    })
      .then(response => {
        return {
          ok: true,
          ...response,
        };
      })
      .catch(error => {
        return {
          ok: false,
          ...error,
        };
      });
  }

  async get(url, headers = {}) {
    this.logRequest('GET', url, {});
    return this._fetch({
      url,
      method: 'get',
      headers: this.makeHeader(headers),
    });
  }

  async post(url, params, headers = {}) {
    this.logRequest('POST', url, params);
    return this._fetch({
      url,
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...this.makeHeader(headers),
      },
      data: params,
    });
  }

  async postFD(url, params, headers = {}) {
    this.logRequest('POST', url, params);
    let fd = new FormData();
    for (let param of params) {
      fd.append(param.key, param.value);
    }
    return this._fetch({
      url,
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data',
        ...this.makeHeader(headers),
      },
      data: fd,
    });
  }

  async put(url, params, headers = {}) {
    this.logRequest('PUT', url, params);
    return this._fetch({
      url,
      method: 'put',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...this.makeHeader(headers),
      },
      data: params,
    });
  }

  async patch(url, params, headers = {}) {
    this.logRequest('PATCH', url, params);
    return this._fetch(url, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...this.makeHeader(headers),
      },
      body: params,
    });
  }

  async upload(url, params) {
    this.logRequest('UPLOAD', url, params);
    let fd = new FormData();
    fd.append('file', params);

    return this._fetch({
      url,
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: fd,
    });
  }
}

export default new Api();
