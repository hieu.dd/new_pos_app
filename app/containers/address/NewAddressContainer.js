import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Switch } from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import * as strings from '../../resources/strings';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { TextInput } from 'react-native-gesture-handler';
import Space from '../../components/Space';
import Button from '../../components/button/Button';
import * as customerUtils from '../../utils/customer';
import { connect } from 'react-redux';
import * as customerActions from '../../stores/customer/actions';
import * as cartActions from '../../stores/cart/actions';
import { Util } from 'teko-js-sale-library';
import InputRow from '../../components/InputRow';
import KeyboardTextInput from '../../components/modal/KeyboardTextInput';
import Header from '../../components/header/Header';
import HeaderIcon from '../../components/header/HeaderIcon';
import TrapezoidButton from '../../components/button/TrapezoidButton';
export class NewAddressContainer extends Component {
  state = {
    province: 'Bấm để chọn',
    district: 'Bấm để chọn',
    province_code: '',
    district_code: '',
    commune: 'Bấm để chọn',
    commune_code: '',
    address: '',
    isDefaltAddress: false,
    keyboardTextInput: {
      visible: false,
      title: '',
      onEndEditing: null,
      onChangeText: null,
      keyboardType: 'default',
      placeholder: '',
      additionalProps: {},
      getValue: () => '',
      accessibilityLabel: '',
    },
  };

  onPress = () => {
    let { customer_id, chooseContact } = this.props.navigation.state.params;
    let { province_code, district_code, address, commune_code } = this.state;
    let contact = { province: province_code, district: district_code, commune: commune_code, address };
    this.props.addContact(customer_id, contact);
    chooseContact && chooseContact(contact, 0);
    this.props.navigation.goBack();
  };

  onSelectProvince = (code, name) => {
    let districts = Util.GeoLoc.getDistrictsOfProvince(code);
    let temp;
    if (districts.length > 0) {
      let communes = Util.GeoLoc.getCommunesOfDistrict(districts[0].key);
      temp = { districts, district: districts[0].label, district_code: districts[0].key, communes };
    }
    this.setState({
      province: name,
      province_code: code,
      ...temp,
    });
  };

  onSelectDistrict = (code, name) => {
    let communes = Util.GeoLoc.getCommunesOfDistrict(code);

    this.setState({ district: name, district_code: code, communes });
  };

  renderKeyboardTextInput() {
    let keyboardTextInput = this.state.keyboardTextInput;
    let value = keyboardTextInput.getValue ? keyboardTextInput.getValue() : '';
    let title = keyboardTextInput.title;
    let additionalProps = keyboardTextInput.additionalProps || {};
    return (
      <KeyboardTextInput
        value={value}
        visible={keyboardTextInput.visible}
        title={title}
        placeholder={keyboardTextInput.placeholder}
        onClose={this.onClose}
        onChangeText={keyboardTextInput.onChangeText}
        onEndEditing={keyboardTextInput.onEndEditing}
        keyboardType={keyboardTextInput.keyboardType}
        accessibilityLabel={keyboardTextInput.accessibilityLabel}
        {...additionalProps}
      />
    );
  }

  onClose = () => {
    this.setState({ keyboardTextInput: { visible: false } });
  };

  onGoBack = () => {
    this.props.navigation.goBack();
  };
  onChangeDefaultAddress = () => {
    this.setState({
      isDefaltAddress: !this.state.isDefaltAddress,
    });
  };

  renderLeftSection = () => {
    return (
      <TrapezoidButton
        color={colors.lightGold}
        iconSize={scale(24)}
        // title={'QUAY LẠI'}
        // textStyle={[textStyles.small, { fontFamily: 'sale-text-bold', color: 'black' }]}
        width={scale(70)}
        height={scale(50)}
        right={scale(12)}
        iconLeftName="chevron-left"
        iconType="material"
        iconColor="black"
        onPress={this.onGoBack}
      />
    );
  };

  onGotoMap = () => {
    this.props.navigation.navigate('Map');
  };

  renderHeader = () => {
    return (
      <Header
        gradient={false}
        color={colors.primary}
        title={'Thêm địa chỉ'}
        titleTextStyle={[textStyles.price, { color: 'white' }]}
        leftSection={this.renderLeftSection()}
        rightSection={
          <HeaderIcon name="map-marker-radius" type="material-community" color={'white'} onPress={this.onGotoMap} />
        }
      />
    );
  };

  onChangeText = text => {
    this.setState({ address: text });
  };

  render() {
    let { province, district, commune } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: colors.lightGray }}>
        {this.renderHeader()}
        <View style={{ padding: scale(10) }}>
          <InputRow
            ref={ref => (this.ref_receiver_province = ref)}
            inputType="picker"
            rowStyle={{ backgroundColor: 'white', paddingLeft: scale(12), marginBottom: scale(10) }}
            caption={'Tỉnh/Thành phố'}
            inputValue={province}
            captionStyle={{ flex: 4 }}
            onEndEditing={this.onSelectProvince}
            placeholder={strings.placeholder_receiver_province}
            inputStyle={{ flex: 5 }}
            hintColor={province === 'Bấm để chọn' ? colors.gray : colors.black}
            editable={true}
            pickerData={Util.GeoLoc.getProvinces()}
            rightIcon={<Icon name="chevron-right" type="material" color={colors.primary} size={scale(16)} />}
          />
          <InputRow
            ref={ref => (this.ref_receiver_district = ref)}
            inputType="picker"
            rowStyle={{ backgroundColor: 'white', paddingLeft: scale(12), marginBottom: scale(10) }}
            caption={'Quận/Huyện'}
            inputValue={district}
            onEndEditing={this.onSelectDistrict}
            placeholder={strings.placeholder_receiver_district}
            hintColor={district === 'Bấm để chọn' ? colors.gray : colors.black}
            editable={true}
            inputStyle={{ flex: 5 }}
            pickerData={this.state.districts}
            rightIcon={<Icon name="chevron-right" type="material" color={colors.primary} size={scale(16)} />}
          />
          <InputRow
            ref={ref => (this.ref_receiver_commune = ref)}
            inputType="picker"
            rowStyle={{ backgroundColor: 'white', paddingLeft: scale(12), marginBottom: scale(10) }}
            caption={'Phường/Xã'}
            inputValue={commune}
            onEndEditing={(code, name) => {
              this.setState({ commune: name, commune_code: code });
            }}
            placeholder={'Nhập tên phường/xã'}
            hintColor={commune === 'Bấm để chọn' ? colors.gray : colors.black}
            editable={true}
            inputStyle={{ flex: 5 }}
            pickerData={this.state.communes}
            rightIcon={<Icon name="chevron-right" type="material" color={colors.primary} size={scale(16)} />}
          />
          <View style={{ paddingHorizontal: scale(12), backgroundColor: 'white', paddingTop: scale(12) }}>
            <Text style={textStyles.body1}>Địa chỉ cụ thể</Text>
            <Text style={textStyles.footnote}>Số nhà, ngõ/ngách, tên đường, ...</Text>
            <View style={{ height: scale(50) }}>
              <TextInput
                style={[textStyles.body1, { flex: 1, textAlignVertical: 'center' }]}
                placeholder={'Nhập địa chỉ cụ thể'}
                placeholderTextColor={colors.darkGray}
                value={this.state.address}
                underlineColorAndroid={'transparent'}
                onChangeText={this.onChangeText}
              />
            </View>
          </View>
          {/* <View style={styles.row}>
            <Text style={[textStyles.body1, { flex: 1, textAlignVertical: 'center' }]}>Đặt làm địa chỉ mặc định</Text>
            <TouchableOpacity onPress={this.onChangeDefaultAddress}>
              <Icon
                size={scale(24)}
                type="font-awesome"
                name={this.state.isDefaltAddress ? 'toggle-on' : 'toggle-off'}
                color={this.state.isDefaltAddress ? colors.primary : colors.lightGray}
              />
            </TouchableOpacity>
          </View> */}
          {/* <View style={[styles.row, { alignItems: 'center' }]}>
            <TouchableOpacity>
              <Text style={[textStyles.body1, { color: colors.primary }]}>Xóa địa chỉ</Text>
            </TouchableOpacity>
          </View> */}
        </View>
        {this.renderKeyboardTextInput()}
        <Button
          onPress={this.onPress}
          color={colors.primary}
          title={'XONG'}
          containerStyle={{ position: 'absolute', height: scale(50), bottom: 0, borderRadius: 0, width: screen.width }}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    addContact: (customer_id, contact) => dispatch(customerActions.addContact(customer_id, contact)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewAddressContainer);

const styles = StyleSheet.create({
  row: {
    marginTop: scale(12),
    paddingHorizontal: scale(12),
    flexDirection: 'row',
    backgroundColor: 'white',
    height: scale(48),
    alignItems: 'center',
  },
});
