import React from 'react';
import { MapView } from 'expo';
import { Platform, StyleSheet, Image, PixelRatio, View } from 'react-native';
import TrapezoidButton from '../../components/button/TrapezoidButton';
import { Icon } from 'react-native-elements';
import { Location, Permissions, Constants } from 'expo';
import * as strings from '../../resources/strings';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { TextInput } from 'react-native-gesture-handler';
const LOCATION_UPDATE_TIME_INTERVAL = 60000;
const LOCATION_UPDATE_DISTANCE_INTERVAL = 2000;
import { connect } from 'react-redux';
import * as userActions from '../../stores/user/actions';
class MapContainer extends React.Component {
  state = {
    permissionLocation: true,
    initialRegion: {
      latitude: 21.009423,
      longitude: 105.823618,
      latitudeDelta: 0.2,
      longitudeDelta: 0.2,
    },
    search: 'Số 1 Thái Hà',
    marker: null,
  };

  componentDidMount() {
    this._watchPositionAsync();
  }

  _watchPositionAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({ permissionLocation: false });
    } else {
      this.setState({ permissionLocation: true });
    }
  };

  onGoBack = () => {
    this.props.navigation.goBack();
  };

  renderLeftSection = () => {
    return (
      <TrapezoidButton
        color={colors.lightGold}
        iconSize={scale(24)}
        width={scale(70)}
        height={scale(50)}
        right={scale(12)}
        iconLeftName="chevron-left"
        iconType="material"
        iconColor="black"
        onPress={this.onGoBack}
      />
    );
  };

  changeTextSearch = text => {
    this.setState({
      search: text,
    });
  };

  addMarker(coordinates) {
    let location = { latitude: coordinates.latitude, longitude: coordinates.longitude };
    this.setState(
      {
        marker: location,
      },
      () => this.props.getCountryByLocation(location, this.searchLocationResult)
    );
  }

  searchLocationResult = result => {};

  renderSearchHeader = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: scale(8),
          alignItems: 'center',
          flex: 1,
        }}
      >
        <Icon name="search" type="material" color={colors.primary} size={scale(24)} />
        <TextInput
          numberOfLines={1}
          underlineColorAndroid={'transparent'}
          style={[textStyles.body1, { flex: 1, textAlignVertical: 'center', paddingHorizontal: scale(8) }]}
          value={this.state.search}
          onChangeText={this.changeTextSearch}
        />
        <Icon name={'close-circle'} type={'material-community'} color={colors.darkGray} size={scale(14)} />
      </View>
    );
  };

  renderHeader = () => {
    return (
      <View
        style={{
          height: scale(50),
          backgroundColor: 'white',
          flexDirection: 'row',
          width: screen.width,
        }}
      >
        {this.renderLeftSection()}
        {this.renderSearchHeader()}
      </View>
    );
  };

  render() {
    let { marker } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {this.renderHeader()}
        <MapView
          style={{ flex: 1 }}
          showsUserLocation={this.state.permissionLocation}
          initialRegion={this.state.initialRegion}
          onPress={event => this.addMarker(event.nativeEvent.coordinate)}
        >
          {this.state.marker ? (
            <MapView.Marker coordinate={{ latitude: marker.latitude, longitude: marker.longitude }} />
          ) : null}
        </MapView>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    getCountryByLocation: (location, callback) => dispatch(userActions.getCountryByLocation(location, callback)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapContainer);
