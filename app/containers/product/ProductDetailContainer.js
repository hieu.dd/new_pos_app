import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, BackHandler, Alert } from 'react-native';
import { connect } from 'react-redux';
import { StackActions } from 'react-navigation';
import { colors } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import * as cartUtils from '../../utils/cart';
import DetailComponent from '../../components/DetailComponent';
import { DposHeader } from '../../components/header/DposHeader';

class ProductDetailComponent extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onGoBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onGoBack);
  }

  onGoBack = () => {
    this.props.navigation.goBack();
    return true;
  };

  onGoToCart = () => {
    this.props.navigation.navigate('Cart', {}, StackActions.popToTop());
  };

  renderHeader() {
    return <DposHeader onPress={this.onGoBack} />;
  }

  renderLeftSection = () => {
    return <View />;
  };

  renderRightSelection = () => {
    let { cartState } = this.props;

    let currentCart = cartUtils.getCurrentCart(cartState);
    return <View />;
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderHeader()}

        <DetailComponent
          sku={this.props.navigation.state.params.sku}
          seo_name={this.props.navigation.state.params.seo_name || null}
          item={this.props.navigation.state.params.item}
          onGoBack={this.onGoBack}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(ProductDetailComponent);
