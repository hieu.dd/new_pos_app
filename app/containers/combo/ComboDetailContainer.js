import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, ScrollView, BackHandler, Animated, StatusBar, Image, Text } from 'react-native';
import uuidv4 from 'uuid/v4';
import PromoManager from 'teko-promotion-parser';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../../config/TrackingPrototype';
import {
  combo_status,
  checkComboNew,
  checkComboStatus,
  isGrandTotalCombo,
  hasSameProductCondition,
  suggestProductsInCartForCombo,
  mapPromotionForCombo,
  getProductQuantityInCombo,
  getProductsConditionInCombo,
  getMaxProductQuantityInCombo,
  getProductsLeftsIgnoreProductsInCombo,
} from '../../modules/promotion/combo';

import { getPromotionQuantityLeft, getNow } from '../../modules/promotion';
import { getBenefitFromPromoNew } from '../../modules/promotion/promotionsNew';
import { changeOneAllOfBenefitQuantity } from '../../modules/promotion/benefit';
import * as utils from '../../utils';
import * as cartUtils from '../../utils/cart';
import { getCurrentCart } from '../../utils/cart';
import * as promotionUtils from '../../modules/promotion/utils';
import * as cartActions from '../../stores/cart/actions';
import * as saleActions from '../../stores/sale/actions';

import { showLoadingModal } from '../../stores/appState/actions';
import { changeCurrentCategory } from '../../stores/product/actions';
import * as strings from '../../resources/strings';
import { colors, screen, textStyles } from '../../resources/styles/common';

import GradientButton from '../../components/button/GradientButton';
import TrapezoidButton from '../../components/button/TrapezoidButton';
import Price from '../../components/common/Price';

import ComboItemInfo from '../../components/combo/ComboItemInfo';
import ComboPromotion from '../../components/combo/ComboPromotion';
import ConditionProducts from '../../components/combo/ConditionProducts';
import NewConditionProducts from '../../components/combo/NewConditionProducts';
import ExtraProducts from '../../components/combo/ExtraProducts';
import ChangeComboModal from '../../components/combo/ChangeComboModal';
import ChangeValueModal from '../../components/cart/ChangeValueModal';
import { scale } from '../../utils/scaling';
import { Icon } from 'react-native-elements';

export class ComboDetailContainer extends Component {
  constructor(props) {
    super(props);

    this.moveAnimation = new Animated.ValueXY({ x: 0, y: screen.height });
  }

  _movePriceDown = () => {
    Animated.spring(this.moveAnimation, {
      toValue: { x: 0, y: screen.height },
    }).start();
  };
  _movePriceUp = () => {
    Animated.spring(this.moveAnimation, {
      toValue: { x: 0, y: screen.height - scale(48) },
    }).start();
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isFirstLoad) {
      return { isFirstLoad: false };
    }
    let combo = nextProps.combo || nextProps.navigation.state.params.combo;
    let { uuid, hasChangedCombo } = {} || nextProps.navigation.state.params;
    if (!combo) return { combo: null };

    const { promotionsNew, currentCart } = nextProps;
    let comboInCart = null;
    let { products, extraProducts, quantity } = prevState;

    if (uuid && currentCart) {
      comboInCart = currentCart.combos.find(item => item.uuid === uuid);
    }

    if (comboInCart !== prevState.comboInCart) {
      products = comboInCart ? comboInCart.products : prevState.products;
      extraProducts = comboInCart ? comboInCart.extraProducts : prevState.extraProducts;
      quantity = comboInCart ? comboInCart.quantity : prevState.quantity;
      comboPrice = (comboInCart && comboInCart.price) || cartUtils.calculateComboPrice({ products, extraProducts });
    }

    let benefit = prevState.benefit;
    if (combo !== prevState.combo) {
      benefit = getBenefitFromPromoNew(combo, combo.programKey, 1);

      let productsInCartForCombo = suggestProductsInCartForCombo(currentCart.items, combo).map(item => ({
        ...item,
        productInSaleQuantity: item.quantity,
      }));

      products = products.length > 0 || hasChangedCombo ? products : productsInCartForCombo;
    }

    let comboPrice = cartUtils.calculateComboPrice({ products, extraProducts });
    let maxProductQuantityInCombo = getMaxProductQuantityInCombo(combo);

    let quantityLeft = getPromotionQuantityLeft('promotion_new', combo.programKey, combo.key, {
      promotionsNew,
      promotionsOld: {},
    });
    quantity = Math.min(quantity, quantityLeft);

    let derivedState = {
      uuid,
      combo,
      comboPrice,
      quantity,
      comboInCart,
      benefit,
      products,
      extraProducts,
      quantityLeft,
      maxProductQuantityInCombo,
    };
    if (!utils.isSubset(derivedState, prevState)) {
      return derivedState;
    }

    return null;
  }

  state = {
    isFirstLoad: true,
    uuid: null,
    combo: null,
    comboPrice: 0,
    quantity: 1,
    products: [],
    extraProducts: [],
    maxProductQuantityInCombo: -1,
    hasAlterCombos: false,
    comboStatus: null,
    comboInCart: null,
    oldProductsInCombo: [],
    asiaPromotions: {
      products: {},
      extraProducts: {},
    },
    benefit: null,
    canQuickApplyProducts: false,
    groupedProductSale: [],
    currentSelectedProduct: null,
    currentSelectedExtraProduct: null,
    visibleChangeComboModal: false,
    visiblePriceBottom: false,
    changeValueModal: {
      title: '',
      placeholder: '',
      unitText: '',
      visible: false,
      value: 0,
      onSubmit: null,
      properValue: utils.onlyNumber,
    },
  };

  componentDidMount() {
    let state = ComboDetailContainer.getDerivedStateFromProps(this.props, this.state);
    if (state) {
      this.setState(state);
    }
    this.checkComboStatus();
    BackHandler.addEventListener('hardwareBackPress', this.onPressBack);

    if (state.combo) {
      let { products } = state;
      let productsCondition = getProductsConditionInCombo(state.combo);
      let quickApplyProducts = [];

      for (let condition of productsCondition) {
        let addedProduct = products.find(product => promotionUtils.getValidConditionForProduct(product, condition, false));

        if (!addedProduct) {
          let quickApplyProduct = promotionUtils.getQuickApplyProduct(condition);
          if (quickApplyProduct) {
            quickApplyProducts.push({ ...quickApplyProduct, isQuickApplyProduct: true });
          }
        }
      }

      if (quickApplyProducts.length > 0) {
        products = [...products, ...quickApplyProducts];
        let canQuickApplyProducts = products.some(product => product.isQuickApplyProduct);
        this.setState({ canQuickApplyProducts, products });
      }
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onPressBack);
  }

  componentDidUpdate(prevProps, prevState) {
    const { products, extraProducts } = this.state;
    if (!utils.isEqualObjectByKeys(this.state, prevState, 'products', 'extraProducts', 'combo')) {
      let comboPrice = cartUtils.calculateComboPrice({ products, extraProducts });
      if (this.state.comboPrice !== comboPrice) {
        this.setState({ comboPrice });
      }

      this.checkComboStatus();
    }
  }

  onChangeBenefit = path => {
    let newBenefit = changeOneAllOfBenefitQuantity(this.state.benefit, path);
    this.setState({ benefit: newBenefit });
  };

  onPressBack = () => {
    this.props.navigation.goBack();
    this.props.stopAddingProductToCombo();

    return true;
  };

  checkComboStatus = () => {
    let { combo, products, extraProducts } = this.state;
    if (!combo) return;

    let comboStatus = checkComboStatus(combo, products, extraProducts);
    this.setState({ comboStatus });
  };

  //================================================================
  // combo quantity
  onUpdateComboQuantity = quantity => {
    this.setState({ quantity });
  };

  onPressComboQuantity = () => {
    this.setState({
      changeValueModal: {
        ...this.state.changeValueModal,
        visible: true,
        title: 'Số lượng sản phẩm',
        placeholder: 'Nhập số lượng',
        unitText: '',
        value: 0,
        onSubmit: this.onSubmitComboQuantity,
      },
    });
  };

  onSubmitComboQuantity = quantity => {
    if (quantity <= 0) {
      this.props.showLoadingModal({ message: 'Số lượng không hợp lệ' });
    } else if (quantity > 999) {
      this.props.showLoadingModal({ message: strings.maximum_quantity });
    } else if (quantity > this.state.quantityLeft) {
      this.props.showLoadingModal({ message: 'Vượt quá số lượng còn lại' });
    } else {
      this.setState({ quantity });
    }
  };
  //==================================================================================

  //==================================================================================
  // apply combo
  onApplyCombo = () => {
    this.props.clearCart();
    const { comboStatus } = this.state;
    let isValidCombo = comboStatus === combo_status.valid || comboStatus === combo_status.larger_than;

    if (comboStatus === combo_status.invalid_product) {
      this.props.showLoadingModal({ message: 'Combo có chứa sản phẩm giá 0đ' });
    } else if (!isValidCombo) {
      this.props.showLoadingModal({ message: 'Combo không đủ điều kiện để áp dụng' });
    } else {
      this.applyComboWithCheckValue();
    }
  };

  applyComboWithCheckValue = () => {
    if (this.state.comboStatus === combo_status.larger_than) {
      let sameCombos = PromoManager.getAllActiveCombos(null, getNow(), 'agent').filter(
        combo => combo.key !== this.state.combo.key && hasSameProductCondition(combo, this.state.combo)
      );
      if (sameCombos.length > 0) {
        this.props.showLoadingModal({
          message: strings.warning_change_combo,
          actions: [
            { text: 'Không', onPress: this.applyComboWithCheckVoucher },
            { text: 'Chọn lại', highlight: true, onPress: this.changeCombo },
          ],
        });
      }
    } else {
      this.applyComboWithCheckVoucher();
    }
  };

  changeCombo = () => {
    this.setState({ visibleChangeComboModal: true });
    Tracking.trackEvent(TRACK.EVENT.PRESS_BUTTON, 'change_combo_option');
  };

  applyComboWithCheckVoucher = () => {
    if (this.props.currentCart.voucher) {
      this.props.showLoadingModal({
        message: strings.remove_voucher_when_create_combo,
        actions: [{ text: 'Không' }, { text: 'Tiếp tục', highlight: true, onPress: this.applyCombo }],
      });
    } else {
      this.applyCombo();
    }
  };

  applyCombo = () => {
    const { benefit, products, extraProducts, quantity, combo, comboPrice, comboStatus } = this.state;

    if (this.props.currentCart.voucher) {
      this.props.changeVoucher(null);
    }

    let uuid = this.state.uuid || uuidv4();
    let promotion = mapPromotionForCombo(combo, benefit);
    this.props.changeCombo({
      uuid,
      name: combo.name,
      description: combo.description,
      price: comboPrice,
      products,
      extraProducts,
      promotion,
      quantity,
    });
    this.props.navigation.navigate('OrderDetail', { comboPrice });
    this.props.stopAddingProductToCombo();
    // this.props.sendOrderToBE({});
    Tracking.trackEvent(TRACK.EVENT.APPLY_COMBO, comboStatus);
  };
  //==================================================================================

  //==================================================================================
  // condition products
  onChooseConditionProduct = (product, condition) => {
    let { products, currentSelectedProduct } = this.state;
    if (products.find(item => item.sku === product.sku && item.uuid === item.uuid)) {
      // product has been added
      return;
    }

    let addedCondition = promotionUtils.getValidConditionForProduct(product, condition);

    if (addedCondition) {
      if (currentSelectedProduct) {
        products = products.filter(item => item.sku !== currentSelectedProduct.sku);
      }

      let quantity = addedCondition.quantity ? addedCondition.quantity : 1;

      products = [
        ...products,
        {
          uuid: product.uuid,
          sku: product.sku,
          name: product.name,
          category: product.category,
          price: product.price,
          originalPrice: product.originalPrice,
          image: product.image,
          asiaPromotion: product.asiaPromotion,
          warranty: product.warranty,
          quantity,
        },
      ];

      this.setProducts(products, 'products', currentSelectedProduct, product);
    }
  };

  onAddConditionProduct = condition => {
    this.onSelectConditionProduct(condition, null);
  };

  onEditConditionProduct = (condition, product) => {
    this.onSelectConditionProduct(condition, product);
  };

  onSelectConditionProduct = (condition, product = null) => {
    let { products, extraProducts, quantity } = this.state;

    let productsLeftInCart = getProductsLeftsIgnoreProductsInCombo(
      this.props.currentCart.items,
      products,
      extraProducts,
      quantity
    );
    let satisfiedProductsInCart = productsLeftInCart.filter(product =>
      promotionUtils.getValidConditionForProduct(product, condition, true)
    );

    this.setState({ currentSelectedProduct: product });
    if (satisfiedProductsInCart.length > 0) {
      this.props.navigation.navigate('ChooseProductInCartForCombo', {
        productsInCart: satisfiedProductsInCart,
        onChooseProduct: product => this.onChooseConditionProduct(product, condition),
        onChooseOther: () => this.selectConditionProduct(condition),
      });
    } else {
      this.selectConditionProduct(condition);
    }
  };

  onDeleteConditionProduct = conditionProduct => {
    let { products } = this.state;
    products = products.filter(item => item.sku !== conditionProduct.sku);

    this.setProducts(products, 'products');
  };

  selectConditionProduct = condition => {
    let productAndCategory = promotionUtils.getProductAndCategory(condition);
    let products = productAndCategory.filter(item => item.hasOwnProperty('sku'));
    let categoriesInCondition = productAndCategory.filter(item => item.hasOwnProperty('category'));

    if (products.length > 0) {
      this.props.startAddingProductToCombo(product => this.onChooseConditionProduct(product, condition));
      this.props.navigation.navigate('ChooseProductForCombo', {
        products,
        onChooseProduct: product => this.onChooseConditionProduct(product, condition),
      });
    } else if (categoriesInCondition.length > 0) {
      this.selectProductFromCategory(condition, categoriesInCondition);
    }
  };

  onPressProductQuantity = product => {
    this.setState({
      currentSelectedProduct: product,
      changeValueModal: {
        ...this.state.changeValueModal,
        visible: true,
        title: 'Số lượng sản phẩm',
        placeholder: 'Nhập số lượng',
        unitText: '',
        value: 0,
        onSubmit: this.onUpdateProductQuantity,
      },
    });
  };

  onUpdateProductQuantity = (quantity, product) => {
    if (quantity < 0) {
      this.props.showLoadingModal({ message: 'Số lượng không hợp lệ' });
      return;
    }

    let { products } = this.state;
    let selectedProduct = product || this.state.currentSelectedProduct;
    let hasChangeQuantity = false;
    products = products.map(item => {
      if (item.sku === selectedProduct.sku && item.quantity !== quantity) {
        hasChangeQuantity = true;
        return {
          ...item,
          quantity: quantity,
          productInSaleQuantity: quantity * (item.productInSaleQuantity || 0),
        };
      } else {
        return item;
      }
    });

    if (hasChangeQuantity) {
      this.setProducts(products, 'products');
    }
  };
  //===========================================================================

  //===========================================================================
  // extra products
  onAddExtraProduct = () => {
    this.selectExtraProduct(this.onSelectExtraProduct, null);
  };

  onPressExtraProductQuantity = product => {
    this.setState({
      currentSelectedExtraProduct: product,
      changeValueModal: {
        ...this.state.changeValueModal,
        visible: true,
        title: 'Số lượng sản phẩm',
        placeholder: 'Nhập số lượng',
        unitText: '',
        value: 0,
        onSubmit: this.onUpdateExtraProductQuantity,
      },
    });
  };

  onUpdateExtraProductQuantity = (quantity, product) => {
    if (quantity < 0) {
      this.props.showLoadingModal({ message: 'Số lượng không hợp lệ' });
      return;
    }

    let { extraProducts } = this.state;
    let currentSelectedExtraProduct = product || this.state.currentSelectedExtraProduct;
    let hasChangeQuantity = false;
    extraProducts = extraProducts.map(extraProduct => {
      if (extraProduct.sku === currentSelectedExtraProduct.sku && extraProduct.quantity !== quantity) {
        hasChangeQuantity = true;
        return {
          ...extraProduct,
          quantity: quantity,
          productInSaleQuantity: quantity * (extraProduct.productInSaleQuantity || 0),
        };
      } else {
        return extraProduct;
      }
    });

    if (hasChangeQuantity) {
      this.setProducts(extraProducts, 'extraProducts');
    }
  };

  onPressEditExtraProduct = product => {
    this.selectExtraProduct(this.onSelectExtraProduct, product);
  };

  selectExtraProduct = (onSelectExtraProduct, selectedExtraProduct = null) => {
    let { products, extraProducts, quantity } = this.state;
    let productsLeftInCart = getProductsLeftsIgnoreProductsInCombo(
      this.props.currentCart.items,
      products,
      extraProducts,
      quantity
    );

    this.setState({
      currentSelectedExtraProduct: selectedExtraProduct,
    });
    if (productsLeftInCart.length > 0) {
      this.props.navigation.navigate('ChooseProductInCartForCombo', {
        productsInCart: productsLeftInCart,
        onChooseProduct: product => this.onSelectExtraProduct(product),
        onChooseOther: () => this.onSelectExtraProductFromSearch(),
      });
    } else {
      this.setState({
        currentSelectedExtraProduct: selectedExtraProduct,
      });
      this.onSelectExtraProductFromSearch();
    }
  };

  onSelectExtraProductFromSearch = () => {
    this.props.changeCurrentCategory('-1');
    this.props.navigation.navigate({ routeName: 'Search', key: 'search-product-for-combo' });
    this.props.startAddingProductToCombo(product => this.onSelectExtraProduct(product), false);
  };

  onSelectAllProductsFromCart = products => {
    let { extraProducts } = this.state;
    products = products.map(product => ({
      sku: product.sku,
      name: product.name,
      price: product.price,
      originalPrice: product.originalPrice,
      quantity: product.quantity,
      productInSaleQuantity: product.quantity,
      addedToCombo: product.addedToCombo,
      category: product.category,
      warranty: product.warranty,
    }));
    extraProducts.forEach(extraProduct => {
      let newProduct = products.find(item => item.sku === extraProduct.sku);
      if (newProduct) {
        newProduct.quantity += extraProduct.quantity;
        newProduct.productInSaleQuantity += extraProduct.productInSaleQuantity;
      } else {
        products.push(extraProduct);
      }
    });

    this.setState({ extraProducts: products });
  };

  onSelectExtraProduct = product => {
    let { extraProducts, currentSelectedExtraProduct } = this.state;

    let existedProduct = extraProducts.find(
      extraProduct =>
        extraProduct.sku === product.sku &&
        (!currentSelectedExtraProduct || (currentSelectedExtraProduct && extraProduct.sku !== currentSelectedExtraProduct.sku))
    );
    if (existedProduct) {
      this.props.showLoadingModal({ message: 'Bạn đã chọn sản phẩm này rồi' });
      return;
    }

    let newProduct = {
      uuid: product.uuid,
      sku: product.sku,
      name: product.name,
      price: product.price,
      originalPrice: product.originalPrice,
      image: product.image,
      asiaPromotion: product.asiaPromotion,
      quantity: 1,
      productInSaleQuantity: product.isFromCart ? 1 : 0,
      addedToCombo: product.addedToCombo,
      category: product.category,
      warranty: product.warranty,
    };

    if (currentSelectedExtraProduct) {
      extraProducts = extraProducts.map(item => {
        if (item.sku === currentSelectedExtraProduct.sku) {
          return newProduct;
        } else {
          return item;
        }
      });
    } else {
      extraProducts = [...extraProducts, newProduct];
    }

    this.setProducts(extraProducts, 'extraProducts', currentSelectedExtraProduct, product);
  };

  onPressDeleteExtraProduct = product => {
    let { extraProducts } = this.state;
    extraProducts = extraProducts.filter(extraProduct => extraProduct.sku !== product.sku);
    this.setState({ extraProducts });
  };
  //==========================================================================

  selectProductFromCategory = (condition, categories) => {
    this.props.changeCurrentCategory(categories[0].category);
    this.props.startAddingProductToCombo(product => this.onChooseConditionProduct(product, condition));
    this.props.navigation.navigate({
      routeName: 'ProductList',
      key: 'add-product-for-combo',
    });
  };

  setProducts = (productsToSet, productsKey, oldProduct, newProduct) => {
    let { oldProductsInCombo, comboSaleInCart } = this.state;
    if (comboSaleInCart && oldProduct && oldProduct.addedToCombo) {
      oldProductsInCombo = [...oldProductsInCombo, { ...oldProduct, quantity: comboSaleInCart.quantity * oldProduct.quantity }];
    }
    if (comboSaleInCart && newProduct && newProduct.addedToCombo) {
      oldProductsInCombo = oldProductsInCombo.filter(item => item.sku !== newProduct.sku);
    }
    this.setState({ [productsKey]: productsToSet, oldProductsInCombo, changeCount: this.state.changeCount + 1 });
  };

  renderChangeValueModal = () => {
    const { changeValueModal } = this.state;

    return (
      <ChangeValueModal
        title={changeValueModal.title}
        placeholder={changeValueModal.placeholder}
        value={changeValueModal.value}
        unitText={changeValueModal.unitText}
        visible={changeValueModal.visible}
        properValue={changeValueModal.properValue}
        onSubmit={changeValueModal.onSubmit}
        onClose={this.onCloseChangeValueModal}
      />
    );
  };

  onCloseChangeValueModal = () => {
    this.setState({ changeValueModal: { ...this.changeValueModal, visible: false } });
  };

  onCloseChangeComboModal = () => {
    this.setState({ visibleChangeComboModal: false });
  };

  filterCombo = item => {
    const currentCombo = this.state.combo;
    const { combo } = item;

    return combo.key !== currentCombo.key && hasSameProductCondition(combo, currentCombo);
  };

  onHasAlterCombos = hasAlterCombos => {
    if (this.state.hasAlterCombos !== hasAlterCombos) {
      this.setState({ hasAlterCombos });
    }
  };

  onSubmitChangeCombo = combo => {
    this.props.navigation.setParams({ combo, hasChangedCombo: true });
    Tracking.trackEvent(TRACK.EVENT.PRESS_BUTTON, 'submit_change_combo');
  };

  onPressEditCombo = () => {
    this.setState({ visibleChangeComboModal: true });
    Tracking.trackEvent(TRACK.EVENT.PRESS_BUTTON, 'change_combo');
  };

  renderConditionProducts = () => {
    const { combo, products, extraProducts, maxProductQuantityInCombo } = this.state;

    if (checkComboNew(combo)) {
      let productQuantityInCombo = getProductQuantityInCombo(products, extraProducts);
      return (
        <NewConditionProducts
          combo={this.state.combo}
          products={this.state.products}
          maxProductQuantityInCombo={maxProductQuantityInCombo}
          productQuantityInCombo={productQuantityInCombo}
          onAddConditionProduct={this.onAddConditionProduct}
          onPressProductQuantity={this.onPressProductQuantity}
          onUpdateProductQuantity={this.onUpdateProductQuantity}
          onEditConditionProduct={this.onEditConditionProduct}
          onDeleteConditionProduct={this.onDeleteConditionProduct}
        />
      );
    } else {
      return (
        <ConditionProducts
          combo={this.state.combo}
          products={this.state.products}
          onSetProducts={this.onSetProducts}
          onAddConditionProduct={this.onAddConditionProduct}
          onEditConditionProduct={this.onEditConditionProduct}
          onDeleteConditionProduct={this.onDeleteConditionProduct}
        />
      );
    }
  };

  onSetProducts = products => {
    this.setState({ products });
  };

  renderExtraProducts = () => {
    if (!checkComboNew(this.state.combo) && isGrandTotalCombo(this.state.combo)) {
      return (
        <ExtraProducts
          products={this.state.extraProducts}
          onAddExtraProduct={this.onAddExtraProduct}
          onPressEditExtraProduct={this.onPressEditExtraProduct}
          onPressDeleteExtraProduct={this.onPressDeleteExtraProduct}
          onPressExtraProductQuantity={this.onPressExtraProductQuantity}
          onUpdateExtraProductQuantity={this.onUpdateExtraProductQuantity}
        />
      );
    } else {
      return null;
    }
  };

  onScrollToTop = () => {
    this.props.scrollToTop && this.props.scrollToTop();
  };

  onGoBack = () => {
    this.props.navigation.goBack();
  };

  renderHeader = () => {
    return (
      <View>
        <StatusBar />
        <View
          style={{
            height: scale(50),
            backgroundColor: colors.primary,
            flexDirection: 'row',
            alignItems: 'center',
            elevation: 5,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TrapezoidButton
              color={colors.gold}
              // title={'QUAY LẠI'}
              // textStyle={[textStyles.small, { color: 'black', fontFamily: 'sale-text-bold' }]}
              width={scale(70)}
              height={scale(50)}
              right={scale(12)}
              iconSize={scale(24)}
              iconLeftName="chevron-left"
              iconType="material"
              iconColor="black"
              onPress={this.onGoBack}
            />
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image
              ref={image => {
                this._image = image;
              }}
              style={{ width: scale(24), height: scale(24) }}
              source={require('../../resources/images/logo_phong_vu.png')}
            />
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', paddingRight: scale(12) }}>
            {/* <Icon name="home" size={scale(24)} type={'material-commnuity'} color="white" /> */}
          </View>
        </View>
      </View>
    );
  };

  getPositionPriceView = position => {
    if (position.fy < 0 && !this.state.visiblePriceBottom) {
      this.setState(
        {
          visiblePriceBottom: true,
        },
        this._movePriceUp
      );
    } else if (position.fy > 0 && this.state.visiblePriceBottom) {
      this.setState(
        {
          visiblePriceBottom: false,
        },
        this._movePriceDown
      );
    }
  };

  renderButtomPrice = () => {
    let { comboPrice } = this.state;
    return (
      <Animated.View
        ref={view => {
          this.refPrice = view;
        }}
        style={[
          {
            flexDirection: 'row',
            alignItems: 'center',
            // paddingTop: scale(8),
            position: 'absolute',
            bottom: 0,
            left: 0,
            width: screen.width,
            height: scale(48),
          },
          this.moveAnimation.getLayout(),
        ]}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: colors.dark_slate_blue,
            paddingHorizontal: scale(12),
            height: scale(48),
            justifyContent: 'center',
            paddingVertical: scale(12),
          }}
        >
          <Text style={[textStyles.footnote, { color: 'white' }]}>Thành tiền: </Text>
          <Price color={colors.squash} style={[textStyles.price]} price={comboPrice} />
        </View>
        <View style={{ position: 'absolute', top: 0, right: 0 }}>
          <TrapezoidButton
            color={colors.primary}
            title={'MUA NGAY'}
            textStyle={[textStyles.body1, { color: 'white', fontFamily: 'sale-text-bold' }]}
            width={scale(140)}
            height={scale(48)}
            left={scale(12)}
            iconSize={scale(16)}
            iconRightName="chevron-right"
            iconType="material-community"
            iconColor="white"
            onPress={this.onApplyCombo}
          />
        </View>
      </Animated.View>
    );
  };

  render() {
    const { combo, comboPrice, quantity, quantityLeft, benefit, hasAlterCombos } = this.state;
    if (!combo) return null;
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        <ScrollView
          style={{ flex: 1 }}
          scrollEventThrottle={16}
          onScroll={event => {
            this.comboItemRef._measure();
          }}
        >
          <ComboItemInfo
            ref={ref => {
              this.comboItemRef = ref;
            }}
            getPositionPriceView={this.getPositionPriceView}
            combo={combo}
            comboPrice={comboPrice}
            quantity={quantity}
            quantityLeft={quantityLeft}
            hasAlterCombos={hasAlterCombos}
            onPressEdit={this.onPressEditCombo}
            onUpdateComboQuantity={this.onUpdateComboQuantity}
            onPressComboQuantity={this.onPressComboQuantity}
            applyCombo={this.onApplyCombo}
          />
          {this.renderConditionProducts()}
          {this.renderExtraProducts()}
          <View style={{ marginTop: scale(-10) }}>
            <ComboPromotion benefit={benefit} onChangeBenefit={this.onChangeBenefit} />
          </View>
        </ScrollView>
        {this.renderButtomPrice()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentCart: getCurrentCart(state.cart),
    promotionsNew: state.firebase.promotionsNew,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showLoadingModal: ({ message, actions }) => dispatch(showLoadingModal({ message, actions })),
    changeCombo: combo => dispatch(cartActions.changeCombo(combo)),
    changeVoucher: voucher => dispatch(cartActions.changeVoucher(voucher)),
    changeCurrentCategory: category => dispatch(changeCurrentCategory(category)),
    startAddingProductToCombo: (addProduct, hideCategoryFilter = true) =>
      dispatch(cartActions.startAddingProductToCombo(addProduct, hideCategoryFilter)),
    stopAddingProductToCombo: () => dispatch(cartActions.stopAddingProductToCombo()),
    sendOrderToBE: ({ onSuccess, onFailure }) => dispatch(saleActions.sendOrderToBE({ onSuccess, onFailure })),
    clearCart: () => dispatch(cartActions.clearCart()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ComboDetailContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightGray,
  },
});
