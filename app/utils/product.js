export const SERVICE_SKUS = ['1702079', '1206838'];

export function isNotService(sku) {
  return !SERVICE_SKUS.includes(sku);
}

export function isService(sku) {
  return SERVICE_SKUS.includes(sku);
}

export function getImageSource(productDetail) {
  return (productDetail && productDetail && productDetail.image && productDetail.image.base_image_smallsquare) || '';
}

export function getProductDetailBySku(sku, products) {
  let product = products[sku];

  return (product && !product.error && !product.isLoading && product.detail) || null;
}

export function getProductPrice(product) {
  return (product && product.price_w_vat) || 0;
}

export function hasImportPrice(sku, importPrices) {
  return importPrices[sku] && importPrices[sku].price !== -1;
}

export function groupProductsBySaleAction(products, productStatuses, productDetails, statusConfig) {
  let groups = { deny_sale: [], allow_sale: [], limit_quantity: [] };

  for (let product of products) {
    let action = getProductSaleAction(product, productStatuses, productDetails, statusConfig);
    if (action === 'TBD') continue;

    groups[action].push(product.sku);
  }

  return groups;
}

export function getProductSaleAction(product, productStatuses, productDetails, statusConfig) {
  let productStatus = getProductStatus(product, productStatuses);
  let action = (statusConfig[productStatus] && statusConfig[productStatus].action) || 'deny_sale';

  if (action === 'limit_quantity') {
    let detail = getProductDetailBySku(product.sku, productDetails);
    if (detail && detail.sale_in_stock >= product.quantity) {
      action = 'allow_sale';
    }
  }

  return action;
}

export function getProductStatus(product, productStatuses) {
  if (Array.isArray(productStatuses)) {
    let statusObj = productStatuses.find(item => item.sku === product.sku);

    return (statusObj && statusObj.status) || -1;
  } else {
    return (productStatuses[product.sku] && productStatuses[product.sku].status) || -1;
  }
}
