class GeoLocation {
  constructor() {
    this.data = {
      provinces: [],
      districts: {},
      communes: {}
    };
  }

  update(geoData) {
    this.data = geoData;
  }

  getProvinces() {
    return this.data.provinces;
  }

  getDistrictsOfProvince(provinceCode) {
    return this.data.districts[provinceCode] ? this.data.districts[provinceCode] : [];
  }

  getCommunesOfDistrict(districtCode) {
    return this.data.communes[districtCode] ? this.data.communes[districtCode] : [];
  }

  getProvince(query) {
    let { key, value } = this.parseQuery(query);
    return key && value ? this.data.provinces.find(d => d[key] === value) : null;
  }

  getDistrict(query) {
    let { key, value } = this.parseQuery(query);
    for (let province_id in this.data.districts) {
      let district = this.data.districts[province_id].find(d => d[key] === value);
      if (district) {
        let code = this.getGeoCodes(district.key);
        let province = this.getProvince({ code: code.province });
        return {
          ...district,
          province
        };
      }
    }

    return null;
  }

  getCommune(query) {
    let { key, value } = this.parseQuery(query);
    for (let district_id in this.data.communes) {
      let commune = this.data.communes[district_id].find(d => d[key] === value);
      if (commune) {
        let code = this.getGeoCodes(commune.key);
        let district = this.getDistrict({ code: code.district });
        return {
          ...commune,
          district
        };
      }
    }

    return null;
  }

  parseQuery(query) {
    let key = null;
    let value = null;
    if ('name' in query) {
      key = 'label';
      value = query.name;
    }
    if ('code' in query) {
      key = 'key';
      value = query.code;
    }

    return { key, value };
  }

  getGeoCodes(code) {
    if (code.length === 6) {
      return { province: code.slice(0, 2), district: code.slice(0, 4), commune: code };
    } else if (code.length === 4) {
      return { province: code.slice(0, 2), district: code.slice(0, 4) };
    } else if (code.length === 2) {
      return { province: code };
    }
  }

  makeFullAddress = ({ address, province_id, district_id, commune_id }) => {
    let addrs = [];

    address && addrs.push(address);
    let commune = this.getCommune({ code: commune_id });
    if (commune) {
      addrs.push(commune.label);
      addrs.push(commune.district.label);
      addrs.push(commune.district.province.label);
    } else {
      let district = this.getDistrict({ code: district_id });
      if (district) {
        addrs.push(district.label);
        addrs.push(district.province.label);
      } else {
        let province = this.getProvince({ code: province_id });
        if (province) {
          addrs.push(province.label);
        }
      }
    }

    return addrs.join(', ');
  };
}

export default new GeoLocation();
