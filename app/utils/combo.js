import { equalPromotion } from '../modules/promotion';
import { arraysEqual, isEqualObjectByKeys } from './index';

export function combosEqual(c1, c2) {
  return (
    equalProductsInCombo(c1.products, c2.products) &&
    equalProductsInCombo(c1.extraProducts, c2.extraProducts) &&
    equalPromotion(c1.promotion, c2.promotion)
  );
}

export function equalProductsInCombo(products1, products2) {
  return arraysEqual(products1, products2, equalProductInCombo);
}

export function equalProductInCombo(p1, p2) {
  return isEqualObjectByKeys(p1, p2, 'sku', 'quantity');
}
