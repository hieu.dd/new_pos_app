import { AsyncStorage } from 'react-native';
import config from '../config';

export const constants = {
  CART_STORAGE_KEY: 'CART_STORAGE_KEY',
  CART_PRODUCT_STORAGE_KEY: 'CART_PRODUCT_STORAGE_KEY',
};

class Storage {
  user_id = null;
  flag = {};
  flagLoaded = false;

  constructor() {}

  setUserId(user_id) {
    this.user_id = String(user_id);
  }

  getUserKeyName(key) {
    return `${key}_${config.env}`;
  }

  getEnvKeyName(key) {
    return `${key}_${config.env}`;
  }
}

export default new Storage();
