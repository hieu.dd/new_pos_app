import { Amplitude } from 'expo';

class Analytics {
  constructor() {}

  async logEvent(name, data) {
    return Amplitude.logEventWithProperties(name, data);
  }
}

export default new Analytics();