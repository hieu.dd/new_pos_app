import uuidv4 from 'uuid/v4';

import {
  getNow,
  equalPromotion,
  getNoPromotion,
  getAsiaPromotion,
  mergeAsiaPromotions,
  flatBenefitFromPromotionWithQuantity,
  getPromotionQuantityLeft,
} from '../modules/promotion';
import { flatBenefit } from '../modules/promotion/benefit';
import { calculateVoucherDiscount } from '../modules/promotion/voucher';
import { getObjectByKeys } from './index';
import * as productUtils from './product';

export function checkEmptyCart(cart) {
  if (!cart) return true;

  const { items, combos } = cart;
  return items.length === 0 && combos.length === 0;
}

export function shouldStoreCart(cart) {
  return !cart.editingCart;
}

export function isEditingCart(cartState) {
  return !!cartState.editingCart;
}

export function checkEditCart(user) {
  return user && user.userInfo && user.userInfo.user_permissions && user.userInfo.user_permissions.order_propose;
}

export function getCurrentCart(cartState) {
  return cartState.editingCart || cartState;
}

export function countProductInCart(cart) {
  const { items, combos } = cart;
  let quantity =
    items.reduce((quantity, item) => quantity + item.quantity, 0) +
    combos.reduce((quantity, item) => quantity + item.quantity * item.productQuantityInCombo, 0);

  return quantity;
}

export function calculateFinalCartInvoice(cart) {
  return calculateTotalCartInvoice(cart) - getTotalCartDiscount(cart);
}

export function calculateTotalCartInvoice(cart) {
  let { items, combos } = cart;

  return (
    items.reduce((invoice, item) => invoice + item.quantity * item.price, 0) +
    combos.reduce((invoice, item) => invoice + item.quantity * (item.price || 0), 0)
  );
}

export function getAppliedPromotionQuantityInCart(cart, promotionFrom, programKey, promotionKey) {
  if (!cart) return 0;

  let { items, combos } = cart;
  let appliedQuantityInCart = 0;
  for (let item of items) {
    appliedQuantityInCart += hasCartItemUsedPromotion(item, promotionFrom, programKey, promotionKey) ? item.quantity : 0;
  }
  for (let combo of combos) {
    appliedQuantityInCart += hasCartItemUsedPromotion(combo, promotionFrom, programKey, promotionKey) ? combo.quantity : 0;
  }

  return appliedQuantityInCart;
}

function hasCartItemUsedPromotion(cartItem, promotionFrom, programKey, promotionKey) {
  return (
    cartItem.promotion &&
    cartItem.promotion.from === promotionFrom &&
    cartItem.promotion.programKey === programKey &&
    cartItem.promotion.key === promotionKey
  );
}

export function getProductsInComboToAddToCart(combo) {
  let { products, extraProducts, quantity } = combo;
  let productsInCombo = [];
  for (let product of products) {
    productsInCombo.push(mapProductInComboToAddToCart(product, quantity));
  }
  for (let product of extraProducts) {
    productsInCombo.push(mapProductInComboToAddToCart(product, quantity));
  }

  return productsInCombo;
}

export function mapProductInComboToAddToCart(product, comboQuantity) {
  return {
    uuid: uuidv4(),
    sku: product.sku,
    name: product.name,
    category: product.category,
    image: product.image,
    price: product.price,
    originalPrice: product.originalPrice,
    asiaPromotion: product.asiaPromotion,
    promotion: getNoPromotion(),
    quantity: product.quantity * comboQuantity,
  };
}

export function calculateComboPrice({ products, extraProducts }) {
  let comboPrice = products.reduce((price, item) => price + item.price * item.quantity, 0);
  comboPrice += extraProducts.reduce((price, item) => price + item.price * item.quantity, 0);

  return comboPrice;
}

export function getAsiaPromotionInCombo(combo) {
  let allAsiaPromotions = getAllAsiaPromotionsInComboProducts(combo);

  return mergeAsiaPromotions(allAsiaPromotions);
}

export function getAllAsiaPromotionsInComboProducts(combo) {
  const { products, extraProducts } = combo;
  let allAsiaPromotions = products.map(item => item.asiaPromotion).filter(item => item);
  allAsiaPromotions = allAsiaPromotions.concat(extraProducts.map(item => item.asiaPromotion).filter(item => item));

  return allAsiaPromotions;
}

export function getVoucherValueFromCart(cart) {
  const { voucher, totalInvoice } = cart;

  return calculateVoucherDiscount(voucher, totalInvoice);
}

export function getAdditionalGrandDiscount(cart) {
  return getAdditionalDiscount(cart);
}

export function getFinalCartInvoice(cart) {
  return cart.totalInvoice - getTotalCartDiscount(cart);
}

export function getFinalCartInvoiceIgnoreAdditional(cart) {
  return cart.totalInvoice - getTotalCartDiscountWithoutAdditional(cart);
}

export function getTotalCartDiscountWithoutAdditional(cart) {
  return getTotalCartDiscount(cart, true);
}

export function getTotalCartDiscount(cart, ignoreAdditional) {
  const { items, combos } = cart;
  let totalDiscount = 0;
  totalDiscount = items.reduce((discount, item) => discount + item.quantity * getTotalItemDiscount(item, ignoreAdditional), 0);
  totalDiscount += combos.reduce(
    (discount, item) => discount + item.quantity * getTotalItemDiscount(item, ignoreAdditional),
    0
  );
  totalDiscount += ignoreAdditional ? 0 : getAdditionalDiscount(cart);
  totalDiscount += calculateVoucherDiscount(cart.voucher, cart.totalInvoice);

  return totalDiscount;
}

export function getFinalItemPrice(item) {
  return item.price - getTotalItemDiscount(item);
}

export function getTotalItemDiscount(item, ignoreAdditional = false) {
  let promotionDiscount = getTotalPromotionDiscountItem(item);
  let additionalDiscount = ignoreAdditional ? 0 : getAdditionalDiscount(item);

  return promotionDiscount + additionalDiscount;
}

export function getAdditionalDiscount(item) {
  return (item && item.additional && item.additional.discount && item.additional.discount.value) || 0;
}

export function getAdditionalDiscountReason(item) {
  return (
    (item.additional &&
      item.additional.discount.value > 0 &&
      item.additional.discount.reason &&
      item.additional.discount.reason.label) ||
    null
  );
}

export function getTotalPromotionDiscountItem(item) {
  const { price, promotion } = item;
  const flattedBenefits = (promotion && (promotion.flattedBenefits, flatBenefit(promotion.benefit))) || [];
  let totalDiscount = 0;
  for (let benefit of flattedBenefits) {
    totalDiscount += getPromotionDiscountFromBenefit(benefit, price);
  }

  return totalDiscount;
}

export function getPromotionDiscountFromBenefit(benefit, price) {
  if (!benefit.value) return 0;

  if (benefit.value.promotion_discount) {
    return benefit.value.promotion_discount;
  } else if (benefit.value.promotion_price) {
    return price - benefit.value.promotion_price;
  } else if (benefit.value.grand_discount) {
    return benefit.value.grand_discount;
  } else if (benefit.value.discount_percentage) {
    return Math.floor((price * benefit.value.discount_percentage) / 100);
  } else {
    return 0;
  }
}

export function hasAdditionalDiscount(item) {
  return item.additional && item.additional.discount && item.additional.discount.value !== 0;
}

export function checkAdditionalInCart(cart) {
  if (!cart) return false;

  const { items, combos } = cart;

  return items.some(hasAdditionalDiscount) || combos.some(hasAdditionalDiscount) || hasAdditionalDiscount(cart);
}

export function getAdditionalOrderInfo(customerInfo) {
  return {
    technical_support: customerInfo.technical_support,
    ship_date: customerInfo.ship_date,
    note: customerInfo.note,
  };
}

export function getTotalProductQuantityInCombo(combo) {
  const { products, extraProducts } = combo;
  let quantity = products.reduce((quantity, item) => quantity + item.quantity, 0);
  quantity += extraProducts.reduce((quantity, item) => quantity + item.quantity, 0);

  return quantity;
}

export function verifyPromotionsInCart(cart, { promotionsOld, promotionsNew, vouchers }, productDetailList) {
  const { items, combos } = cart;
  let verifiedData = [];

  for (let item of items) {
    verifiedData = verifiedData.concat(verifyPromotionsInItem(item, 'item', cart, { promotionsOld, promotionsNew }));
    verifiedData = verifiedData.concat(verifyAsiaPromotionsInItem(item, productDetailList));
  }

  for (let combo of combos) {
    verifiedData = verifiedData.concat(verifyPromotionsInItem(combo, 'combo', cart, { promotionsOld, promotionsNew }));
    verifiedData = verifiedData.concat(verifyAsiaPromotionsInCombo(combo, productDetailList));
  }

  return verifiedData;
}

function verifyPromotionsInItem(item, type, cart, { promotionsOld, promotionsNew }) {
  const { promotion } = item;
  let verifiedData = [];

  if (promotion && promotion.from !== 'none') {
    let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
      promotionsNew,
      promotionsOld,
    });
    let promoQuantityInCart = getAppliedPromotionQuantityInCart(cart, promotion.from, promotion.programKey, promotion.key);

    if (promoQuantityInCart > quantityLeft) {
      verifiedData.push({
        uuid: item.uuid,
        type,
        messages: [`Khuyến mãi của chương trình "${promotion.name}" không còn đủ số lượng trong giỏ hàng`],
      });
    }
  }

  return verifiedData;
}

function verifyAsiaPromotionsInCombo(combo, productDetailList) {
  const { products, extraProducts } = combo;
  let verifiedData = [];

  for (let product of products) {
    verifiedData = verifiedData.concat(verifyAsiaPromotionsInItem(product, productDetailList));
  }

  for (let product of extraProducts) {
    verifiedData = verifiedData.concat(verifyAsiaPromotionsInItem(product, productDetailList));
  }

  if (verifiedData.length > 0) {
    return [
      {
        uuid: combo.uuid,
        type: 'combo',
        messages: [''],
      },
    ];
  } else {
    return [];
  }
}

function verifyAsiaPromotionsInItem(item, productDetailList) {
  let verifiedData = [];
  let productDetail = productUtils.getProductDetailBySku(item.sku, productDetailList);

  if (!productDetail) return [];

  let asiaPromotion = getAsiaPromotion(productDetail, null, 1, getNow());

  if (item.price !== productDetail.price_w_vat || !equalPromotion(asiaPromotion, item.asiaPromotion)) {
    verifiedData.push({
      type: 'item',
      sku: item.sku,
      uuid: item.uuid,
      messages: [`Khuyến mãi asia của sản phẩm ${item.sku} không còn nữa`],
    });
  }

  return verifiedData;
}

export function verifyProductInventory(cart, productDetails) {
  const { items, combos } = cart;
  let validateData = [];

  for (let item of items) {
    validateData = validateData.concat(verifyItemInventory(item, 'item', productDetails));
  }

  for (let combo of combos) {
    validateData = validateData.concat(verifyItemInventory(combo, 'combo', productDetails));
  }

  return validateData;
}

function verifyItemInventory(item, type, productDetails) {
  let validateData = [];
  let mapItemFunction = type === 'item' ? mapItemInCartToOrderlines : mapComboInCartToOrderlines;
  let productsInItem = mapItemFunction(item, productDetails);

  for (let product of productsInItem) {
    let productDetail = productDetails[product.product] && productDetails[product.product].detail;
    if (!productDetail) {
      validateData.push({
        uuid: item.uuid,
        type,
        messages: [`Sản phẩm ${product.product} không có thông tin`],
      });

      break;
    } else if (productDetail.sale_in_stock <= 0) {
      validateData.push({
        uuid: item.uuid,
        type,
        messages: [`Sản phẩm ${product.product} đã hết hàng`],
      });

      break;
    }
  }

  return validateData;
}

export function getProductsInCartToCheckRevenue(cart) {
  // hiện tại mới chỉ lấy sản phẩm chính, chưa có sản phẩm xin thêm nên gọi luôn hàm getProductsInCart
  return getProductsInCart(cart);
}

export function getProductsInCart(cart) {
  const { items, combos } = cart;

  return combos
    .reduce((products, combo) => combineProducts([...products, ...getProductsInCombo(combo)]), items)
    .filter(item => item.quantity > 0)
    .map(product => getObjectByKeys(product, 'sku', 'name', 'category', 'quantity'));
}

export function getProductsInCombo(combo) {
  const { products, extraProducts } = combo;

  return combineProducts([...products, ...extraProducts]).map(item => ({
    ...item,
    quantity: item.quantity * combo.quantity,
  }));
}

function combineProducts(products) {
  let combinedProduct = [];
  for (let product of products) {
    if (!combinedProduct.find(item => String(item.sku) === String(product.sku))) {
      let sameProducts = products.filter(item => String(item.sku) === String(product.sku));
      combinedProduct.push({ ...product, quantity: countProductsQuantity(sameProducts) });
    }
  }

  return combinedProduct;
}

function countProductsQuantity(products) {
  return products.reduce((quantity, product) => quantity + product.quantity, 0);
}

export function makeOrderlinesFromCart(cart, productDetails) {
  const { items, combos } = cart;

  let orderlinesFromItems = items
    .filter(item => item.quantity > 0)
    .reduce((orderlines, item) => [...orderlines, ...mapItemInCartToOrderlines(item, productDetails)], []);

  let orderlinesFromCombos = combos
    .filter(item => item.quantity > 0)
    .reduce((orderlines, item) => [...orderlines, ...mapComboInCartToOrderlines(item, productDetails)], []);

  return [...orderlinesFromItems, ...orderlinesFromCombos];
}

function mapItemInCartToOrderlines(item, productDetails) {
  let orderlines = [];
  let productDetail = productUtils.getProductDetailBySku(item.sku, productDetails) || {};
  let partialPromotions = partialPromotionInCartItem(item);

  orderlines.push({
    product: String(item.sku),
    quantity: item.quantity,
    price: parseInt(item.price),
    name: item.name,
    source_url: item.image,
    discount_item: getAdditionalDiscount(item),
    discount_pv: partialPromotions.discount,
    description: productDetail.description,
    bundle_meta: getAdditionalDiscountReason(item),
    // note: api.chickenCipher(`${p.price},${p.price_asia},${p.price_gamenet},${p.status}`)
  });

  orderlines = orderlines.concat(partialPromotions.gifts.map(gift => mapGiftToOrderline(gift, productDetails)));

  return orderlines;
}

function mapComboInCartToOrderlines(combo, productDetails) {
  const { products, extraProducts, quantity, price } = combo;

  let asiaPromotion = getAsiaPromotionInCombo(combo);
  let partialPromotions = partialPromotionInCartItem({ ...combo, asiaPromotion });
  let orderlines = [...getOrderlinesFromComboProducts(products, 1), ...getOrderlinesFromComboProducts(extraProducts, 1)];
  if (partialPromotions.discount > 0) {
    orderlines = applyGrandDiscountToOrderlines(orderlines, partialPromotions.discount, 'discount_pv');
  }

  // add additionalDiscount to combo
  let additionalDiscount = getAdditionalDiscount(combo);
  let reason = getAdditionalDiscountReason(combo);
  if (additionalDiscount !== 0) {
    orderlines = applyGrandDiscountToOrderlines(orderlines, additionalDiscount, 'discount_item');
    orderlines = orderlines.map(item => ({ ...item, bundle_meta: reason }));
  }

  orderlines = orderlines.map(item => ({ ...item, quantity: item.quantity * quantity }));
  orderlines = orderlines.concat(partialPromotions.gifts.map(gift => mapGiftToOrderline(gift, productDetails)));

  return orderlines;
}

export function getOrderlinesFromComboProducts(products) {
  return products.map(item => ({
    product: String(item.sku),
    quantity: item.quantity,
    price: parseInt(item.price),
    name: item.name,
    source_url: item.image,
    discount_item: 0,
    discount_pv: 0,
    description: item.description,
    // note: api.chickenCipher(`${p.price},${p.price_asia},${p.price_gamenet},${p.status}`)
  }));
}

export function mapGiftToOrderline(gift, productDetails) {
  let giftDetail = productUtils.getProductDetailBySku(gift.sku, productDetails) || {};

  return {
    product: String(gift.sku),
    quantity: gift.quantity,
    price: parseInt(0),
    name: giftDetail.name || '',
    source_url: productUtils.getImageSource(giftDetail),
    discount_item: 0,
    discount_pv: 0,
    is_gift: true,
    description: giftDetail.description,
    // note: api.chickenCipher(`${p.price},${p.price_asia},${p.price_gamenet},${p.status}`)
  };
}

function partialPromotionInCartItem(cartItem) {
  let { quantity, promotion, asiaPromotion, price } = cartItem;

  // PV promotions
  let flattedBenefits = flatBenefitFromPromotionWithQuantity(promotion, quantity);
  let discount = getDiscountFromFlattedBenefit(flattedBenefits, price);
  let gifts = getGiftsFromFlattedBenefit(flattedBenefits);

  // asia promotion
  let flattedBenefitsAsia = flatBenefitFromPromotionWithQuantity(asiaPromotion, quantity);
  gifts = [...gifts, ...getGiftsFromFlattedBenefit(flattedBenefitsAsia)];

  return { discount, gifts };
}

export function getDiscountFromFlattedBenefit(flattedBenefits, price) {
  let discount = 0;
  for (let benefit of flattedBenefits) {
    if (benefit.discount_percentage) {
      discount += Math.floor((benefit.discount_percentage * price) / 100);
    } else if (benefit.promotion_discount) {
      discount += benefit.promotion_discount;
    } else if (benefit.grand_discount) {
      discount += benefit.grand_discount;
    } else if (benefit.promotion_price) {
      return price - benefit.promotion_price;
    }
  }

  return discount;
}

export function getGiftsFromFlattedBenefit(flattedBenefits) {
  return flattedBenefits
    .filter(benefit => benefit.sku && benefit.quantity > 0)
    .map(benefit => ({
      sku: benefit.sku,
      quantity: benefit.quantity,
    }));
}

function applyGrandDiscountToOrderlines(orderlines, discount, discountKey) {
  if (discount === 0) return orderlines;

  let orderlinesValue = calculateOrderlinesValue(orderlines);
  let orderlinesToApply = orderlines.filter(item => getOrderlineItemValue(item) > 0).sort((a, b) => b.quantity - a.quantity);

  let remainDiscount = discount;
  orderlinesToApply.forEach((item, index) => {
    if (discount > 0) {
      let itemValue = getOrderlineItemValue(item);
      let itemDiscount = 0;
      let unit = 1000;
      if (itemValue > 0) {
        if (index === orderlinesToApply.length - 1) {
          itemDiscount = remainDiscount;
          unit = 1;
        } else {
          itemDiscount = parseInt((itemValue / orderlinesValue) * discount);
          unit = 1000;
        }

        if (itemDiscount > itemValue) {
          itemDiscount = itemValue;
        }

        if (itemDiscount > remainDiscount) {
          itemDiscount = getValueForOneItem(remainDiscount, item.quantity, unit);
        } else {
          itemDiscount = getValueForOneItem(itemDiscount, item.quantity, unit);
        }

        remainDiscount -= itemDiscount;
        item[discountKey] += itemDiscount / item.quantity;
      }
    }
  });

  return orderlines;
}

export function calculateOrderlinesValue(orderlines) {
  return orderlines.reduce((value, item) => value + item.quantity * getFinalOrderlineItemPrice(item), 0);
}

function getOrderlineItemValue(item) {
  return item.quantity * getFinalOrderlineItemPrice(item);
}

function getFinalOrderlineItemPrice(item) {
  return item.price - item.discount_item - item.discount_pv;
}

function getValueForOneItem(totalValue, quantity, unit) {
  return parseInt(parseInt(totalValue / unit / quantity) * unit) * quantity;
}

export function calculateOrderlinesValueWithoutDiscountItem(orderlines) {
  return orderlines.reduce((value, item) => value + item.quantity * getFinalOrderlineItemPriceWithoutDiscountItem(item), 0);
}

function getFinalOrderlineItemPriceWithoutDiscountItem(item) {
  return item.price - item.discount_pv;
}

export function checkHasPromotionInCart(cart) {
  const { items, combos } = cart;
  return items.some(checkHasPromotionInItem) || combos.some(checkHasPromotionInItem);
}

export function checkHasPromotionInItem(item) {
  return item.promotion && item.promotion.from !== 'none';
}

export function getCartWithImportPrice(cart, importPrices) {
  let { items, combos } = cart;

  items = items.filter(item => productUtils.hasImportPrice(item.sku, importPrices));
  combos = combos.filter(combo => hasComboImportPrice(combo, importPrices));

  return { ...cart, items, combos, totalInvoice: calculateTotalCartInvoice({ items, combos }) };
}

export function hasComboImportPrice(combo, importPrices) {
  const { products, extraProducts } = combo;

  return (
    products.every(product => productUtils.hasImportPrice(product.sku, importPrices)) &&
    extraProducts.every(product => productUtils.hasImportPrice(product.sku, importPrices))
  );
}

export function verifyCartImportPrice(cart, importPrices) {
  let invalidData = [];

  let productsInCart = getProductsInCart(cart);
  for (let product of productsInCart) {
    if (!productUtils.hasImportPrice(product.sku, importPrices)) {
      invalidData.push({
        sku: product.sku,
        messages: [`Sản phẩm ${product.sku} không có giá nhập`],
      });
    }
  }

  return invalidData;
}

export function groupProductsInCartBySaleAction(cart, productStatuses, productDetails, statusConfig) {
  let productsInCart = getProductsInCart(cart);

  return productUtils.groupProductsBySaleAction(productsInCart, productStatuses, productDetails, statusConfig);
}

export function standardize(obj) {
  return JSON.parse(
    JSON.stringify(obj, (key, value) => {
      return value === null || value === undefined ? undefined : value;
    })
  );
}
