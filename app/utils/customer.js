import moment from 'moment';
import { Util } from 'teko-js-sale-library';

export function makeSearchCustomerParams(field, value) {
  let params = {};
  params[field] = value;
  return params;
}

/**
 *
 * @param {string} source:  ['OFFSALE', 'CRM']
 * @param {object} results: search results from server
 * @return {array} list customer
 */
export function transformCustomerSearchResult(source, searchResult) {
  if (source === 'OFFSALE') {
    return transformBECustomerSearchResult(searchResult);
  } else if (source === 'CRM') {
    return transformCRMCustomerSearchResult(searchResult);
  }

  return [];
}

function transformBECustomerSearchResult(searchResult) {
  return (searchResult && searchResult.results) || [];
}

function transformCRMCustomerSearchResult(searchResult) {
  let customers = (searchResult && searchResult && searchResult.result.customers) || [];
  return customers.map(guessProvinceDistrictFromFullAddress);
}

function guessProvinceDistrictFromFullAddress(customer) {
  if (customer && customer.address && !customer.district && !customer.province) {
    let lowerCaseAddress = customer.address.toLocaleLowerCase('vi-VN');
    let provinces = Util.GeoLoc.getProvinces();
    for (let p of provinces) {
      if (lowerCaseAddress.indexOf(p.label.toLocaleLowerCase('vi-VN')) !== -1) {
        customer.province = p.key;
        let districts = Util.GeoLoc.getDistrictsOfProvince(p.key);
        for (let d of districts) {
          let districtName = d.label
            .replace('Quận ', '')
            .replace('Huyện ', '')
            .replace('Thị xã ', '')
            .replace('Thành phố ', '')
            .toLocaleLowerCase('vi-VN');
          if (lowerCaseAddress.indexOf(districtName) !== -1) {
            customer.district = d.key;
          }
        }
      }
    }
  }
  if (customer.contacts) {
    customer.contacts = customer.contacts
      .filter(contact => contact.district && contact.province && contact.ward)
      .map(item => {
        return { ...item, commune: item.ward };
      });
  }
  return customer;
}

export function getExistOpportunity(Opportunities, username = 'Bùi Minh Thành') {
  let existOpportunity = null;
  let handledByMe = false;
  let myExistingOpportunity = Opportunities.find(e => e.stage !== 'won' && !e.lost && e.saleman.indexOf(username) !== -1);
  let otherExistingOpportunity = Opportunities.find(e => e.stage !== 'won' && !e.lost && e.saleman.indexOf(username) === -1);

  if (myExistingOpportunity) {
    existOpportunity = myExistingOpportunity;
    handledByMe = true;
  } else if (otherExistingOpportunity) {
    existOpportunity = otherExistingOpportunity;
    // throw new Error(sprintf(string.customer_already_handled, existOpportunity.saleman));
  }

  return { existOpportunity, handledByMe };
}

export function mapCustomerInfoFromState(state) {
  let customerInfo = {
    id: state.id,
    asia_crm_id: state.asia_crm_id,
    name: state.customer_name && state.customer_name.trim(),
    phone: state.customer_phone && state.customer_phone.trim(),
    district_code: state.receiver_district_code,
    street: state.receiver_street,
    gcafe_id: (state.GcafeId && parseInt(state.GcafeId.trim())) || null,
    technical_support: state.technical_installation,
    ship_date: moment(state.receiver_date + ' - ' + state.receiver_time, 'DD/MM/YYYY - HH:mm A').unix(),
    require_vat: state.require_vat,
    billing: {
      // taxcode: state.require_vat ? state.receiver_tax_code : '',
      phone: state.customer_phone && state.customer_phone.trim(),
      name: state.customer_name && state.customer_name.trim(),
      province: state.receiver_province,
      province_code: state.receiver_province_code,
      district: state.receiver_district,
      district_code: state.receiver_district_code,
      street: state.require_vat ? state.receiver_company_address : state.receiver_street,
      company: state.require_vat ? state.receiver_company_name : '',
    },

    note: state.note,
  };
  if (state.indexContact !== -1) {
    customerInfo.shipping = {
      phone: state.receiver_phone,
      name: state.receiver_name,
      province: state.receiver_province,
      province_code: state.receiver_province_code,
      commune: state.receiver_commune,
      commune_code: state.receiver_commune_code,
      district: state.receiver_district,
      district_code: state.receiver_district_code,
      street: state.receiver_street,
    };
  }
  return customerInfo;
}

export function mapCRMparamsFromCustomer(customer) {
  if (customer) {
    customer.contacts = customer.contacts.map(item => {
      if (item.commune) {
        item.ward = item.commune;
      }
      return item;
    });
    return { customer };
  }
  return null;
}

export function mapCRMparamsFromState(state) {
  let customer = {
    name: state.customer_name && state.customer_name.trim(),
    phone: state.customer_phone && state.customer_phone.trim(),
    is_company: false,
  };
  if (state.id) {
    customer.id = state.id;
  }
  let contact = {
    phone: state.receiver_phone,
    name: state.receiver_name,
    province: state.receiver_province_code,
    district: state.receiver_district_code,
    address: state.receiver_street,
  };

  let tax = {
    name: state.require_vat ? state.receiver_company_name : '',
    taxcode: state.require_vat ? state.receiver_tax_code : '',
    is_company: true,
    tax_address: state.require_vat ? state.receiver_company_address : state.receiver_street,
  };
  let params = { contact, customer };
  if (state.require_vat) {
    params.tax = tax;
  }
  return params;
}

export function mapStateFromCustomerInfo(customer) {
  return {
    id: customer.id,
    customer_name: customer.name || '',
    phone: customer.phone || '',
    receiver_district_code: customer.district_code || '',
    receiver_street: customer.street || '',
    gcafe_id: customer || '',
    technical_installation: customer.technical_support,
    receiver_date: customer.ship_date ? moment(customer.ship_date).format('DD/MM/YYYY') : moment().format('DD/MM/YYYY'),
    receiver_time: customer.ship_date ? moment(customer.ship_date).format('HH:mm A') : moment().format('HH:mm A'),
    require_vat: customer.require_vat,
    taxcode: customer.require_vat && customer.billing ? customer.billing.taxcode : '',
    customer_phone: customer.billing && customer.billing.phone && customer.billing.phone.trim(),
    receiver_province:
      (customer.billing && customer.billing.province) || (customer.shipping && customer.shipping.province) || '',
    receiver_province_code:
      (customer.billing && customer.billing.province_code) || (customer.shipping && customer.shipping.province_code) || '',
    receiver_district:
      (customer.billing && customer.billing.district) || (customer.shipping && customer.shipping.district) || '',
    receiver_company_address:
      (customer.require_vat ? customer.billing && customer.billing.street : customer.billing && customer.billing.street) || '',
    receiver_company_name: customer.require_vat && customer.billing ? customer.billing.company : '',
    receiver_phone: customer.shipping ? customer.shipping.phone : '',
    receiver_name: customer.shipping ? customer.shipping.name : '',
    note: customer.note || '',
  };
}

export function getFullAddressOfContact(contact) {
  let { address, district, province } = contact;
  let fullAddress = address;
  if (district) {
    fullAddress += `, ${Util.GeoLoc.getDistrict({ code: district }).label}`;
    // fullAddress += `, ${district}`;
  }
  if (province) {
    fullAddress += `, ${Util.GeoLoc.getProvince({ code: province }).label}`;
  }
  return fullAddress || 'Không có địa chỉ ^^';
}

export function getFullAddressOfShipping(shipping) {
  let { street, district_code, province_code } = shipping;
  let fullAddress = street;
  if (district_code) {
    fullAddress += `, ${Util.GeoLoc.getDistrict({ code: district_code }).label}`;
    // fullAddress += `, ${district}`;
  }
  if (province_code) {
    fullAddress += `, ${Util.GeoLoc.getProvince({ code: province_code }).label}`;
  }
  return fullAddress || 'Không có địa chỉ ^^';
}
