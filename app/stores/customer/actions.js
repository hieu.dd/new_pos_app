import CustomerService from 'teko-js-sale-library/packages/Service/CustomerService';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../../config/TrackingPrototype';
import * as strings from '../../resources/strings';
import * as cartActions from '../cart/actions';
import * as customerActions from '../customer/actions';
import * as customerUtils from '../../utils/customer';

import * as appStateActions from '../appState/actions';
import * as customerActionTypes from './action-types';

export function searchCustomer(search, fieldSearch, onSearchResult, onChooseCustomer) {
  return async (dispatch, getState) => {
    try {
      dispatch(appStateActions.showLoadingModal({ loadingText: strings.searching_customer }));
      let params = customerUtils.makeSearchCustomerParams(fieldSearch, search);
      // let ret = await CustomerService.searchCustomer(params);
      let ret = await CustomerService.getCustomerFromCrm(params);
      if (ret.ok && ret.data) {
        let customers = customerUtils.transformCustomerSearchResult('CRM', ret.data);
        if (customers.length === 0) {
          throw new Error('404');
        }
        dispatch(appStateActions.hideLoadingModal());
        dispatch(customerActions.setSearchResults(customers));

        // tam thoi luon lay customer dau tien
        // onChooseCustomer(customers[0]);
        customers.length === 1 ? onChooseCustomer(customers[0]) : onSearchResult(true, customers);
      } else {
        throw new Error('404');
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'searchCustomer', error.message, error.stack);
      dispatch(appStateActions.showLoadingModal({ message: strings.customer_not_found }));
      onSearchResult(false);
    }
  };
}

export function addContact(customer_id, contact) {
  return {
    type: customerActionTypes.ADD_CONTACT,
    customer_id,
    contact,
  };
}

export function saveCustomerToCRM(params) {
  return async function(dispatch, getState) {
    let ret = await CustomerService.saveCustomerToCrm(params);
  };
}

export function setSearchResults(searchResults) {
  return {
    type: customerActionTypes.SET_SEARCH_RESULTS,
    searchResults,
  };
}

export function getCustomerOrCreate(onSuccess, onFailure) {
  return async function(dispatch, getState) {
    let customerId = null;

    try {
      let { customerInfo } = getState().cart;
      customerId = customerInfo.id;

      if (!customerId) {
        dispatch(appStateActions.showLoadingModal({ loadingText: strings.creating_new_customer }));
        let createCustomerRet = await CustomerService.createCustomer(customerInfo);

        if (createCustomerRet.ok) {
          customerId = createCustomerRet.data.id;
        } else {
          throw new Error(JSON.stringify(createCustomerRet.data));
        }
      }

      dispatch(cartActions.saveCustomerInfo({ ...customerInfo, id: customerId }));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'getCustomerOrCreate', error.message, error.stack);
      dispatch(appStateActions.showLoadingModal({ title: 'Lỗi', message: error.message }));
    }

    return customerId;
  };
}

export function getCustomer(getState) {
  let { customerInfo } = getState().cart;
  let { name, phone, asia_crm_id, gcafe_id } = customerInfo;
  let customer = { customer_name: name, customer_phone: phone };
  if (gcafe_id) customer.gcafe_id = gcafe_id;
  if (asia_crm_id) customer.asia_crm_id = asia_crm_id;
  return customer;
}
