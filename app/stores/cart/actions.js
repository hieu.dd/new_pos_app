import { AsyncStorage } from 'react-native';
import uuidv4 from 'uuid/v4';
import { Tracking, Service, Util } from 'teko-js-sale-library';
import firebase from 'firebase';

import TRACK from '../../config/TrackingPrototype';
import { getPromotionQuantityLeft } from '../../modules/promotion';
import * as types from './action-types';
import * as screenStateActions from '../screenState/actions';
import * as strings from '../../resources/strings';
import * as cartUtils from '../../utils/cart';
import Storage, { constants } from '../../utils/storage';
import RootNavigationService from '../../services/navigation/RootNavigationService';

export function addProductToCart(product, onAddProductResult) {
  return async (dispatch, getState) => {
    product = { ...product, uuid: uuidv4() };

    if (product.promotion && product.promotion.from !== 'none') {
      const { promotion } = product;
      const { promotionsNew, promotionsOld } = getState().firebase;
      const cart = getState().cart;
      let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
        promotionsNew,
        promotionsOld,
      });
      let promoQuantityInCart = cartUtils.getAppliedPromotionQuantityInCart(
        cart,
        promotion.from,
        promotion.programKey,
        promotion.key
      );

      if (product.quantity + promoQuantityInCart > quantityLeft) {
        onAddProductResult && onAddProductResult(false, strings.exceed_promotion_quantity);
        return;
      }
    }

    dispatch({
      type: types.ADD_PRODUCT_TO_CART,
      product: product,
    });
    onAddProductResult(true, 'Thêm vào giỏ hàng thành công');

    dispatch(storeCart());
  };
}

export function changeProduct(product, onResult) {
  return async (dispatch, getState) => {
    if (!product.uuid) {
      product = { ...product, uuid: uuidv4() };
    }

    const cart = getState().cart;
    let existedProduct = cart.items.find(item => item.uuid === product.uuid);
    let oldQuantity = existedProduct ? existedProduct.quantity : 0;

    if (product.promotion && product.promotion.from !== 'none') {
      if (cart.voucher !== null) {
        onResult && onResult(false, 'Giỏ hàng hiện đang có PMH');
        return;
      }

      const { promotion } = product;
      const { promotionsNew, promotionsOld } = getState().firebase;
      let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
        promotionsNew,
        promotionsOld,
      });
      let promoQuantityInCart = cartUtils.getAppliedPromotionQuantityInCart(
        cart,
        promotion.from,
        promotion.programKey,
        promotion.key
      );

      if (product.quantity + promoQuantityInCart - oldQuantity > quantityLeft) {
        onResult && onResult(false, strings.exceed_promotion_quantity);
        return;
      }
    }

    dispatch({
      type: types.CHANGE_PRODUCT,
      product: product,
    });
    onResult && onResult(true);

    dispatch(storeCart());
  };
}

export function finishedChangeCart(items) {
  Tracking.trackEvent(TRACK.EVENT.FINISH_CHANGE_CART, Util.Logger.measureAndClear('focusInCart'), items);
}

export function updateQuantityProduct(uuid, quantity, onResult) {
  return async (dispatch, getState) => {
    let product = getState().cart.items.find(item => item.uuid === uuid);
    let oldQuantity = product.quantity;

    if (product.promotion && product.promotion.from !== 'none') {
      const { promotion } = product;
      const { promotionsNew, promotionsOld } = getState().firebase;
      const cart = getState().cart;
      let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
        promotionsNew,
        promotionsOld,
      });
      let promoQuantityInCart = cartUtils.getAppliedPromotionQuantityInCart(
        cart,
        promotion.from,
        promotion.programKey,
        promotion.key
      );

      if (quantity + promoQuantityInCart - oldQuantity > quantityLeft) {
        onResult && onResult(false, strings.exceed_promotion_quantity);
        return;
      }
    }

    dispatch({
      type: types.UPDATE_QUANTITY_PRODUCT,
      uuid,
      quantity,
    });
    Tracking.trackEvent(TRACK.EVENT.UPDATE_QUANTITY_PRODUCT_IN_CART, uuid, product.sku, product, quantity, oldQuantity);
    dispatch(storeCart());
  };
}

export function storeCart() {
  return async (dispatch, getState) => {
    let cart = getState().cart;
    if (cartUtils.shouldStoreCart(cart)) {
      //do not save tempCustomerInfo
      cart.saveTempCustomerInfo = {};
      AsyncStorage.setItem(Storage.getUserKeyName(constants.CART_STORAGE_KEY), JSON.stringify(cart));
    }
  };
}

export function loadCartFromStorage() {
  return async (dispatch, getState) => {
    try {
      let cart = await AsyncStorage.getItem(Storage.getUserKeyName(constants.CART_STORAGE_KEY));
      cart = JSON.parse(cart);

      dispatch({
        type: types.LOAD_CART_FROM_STORAGE,
        cart,
      });
    } catch (error) {
      //
    }
  };
}

export function clearCart() {
  return async (dispatch, getState) => {
    dispatch({
      type: types.CLEAR_CART,
    });
    dispatch(storeCart());
  };
}

export function adjustPrice(productUuid, newPrice) {
  return dispatch => {
    dispatch({
      type: types.ADJUST_PRICE,
      productUuid,
      newPrice,
    });
  };
}

export function removeAdjustPrice(uuid) {
  return dispatch => {
    dispatch({
      type: types.REMOVE_ADJUST_PRICE,
      uuid,
    });
  };
}

export function saveCustomerInfo(customerInfo) {
  return dispatch => {
    dispatch({
      type: types.SAVE_CUSTOMER_INFO,
      customerInfo,
    });
  };
}

export function saveTempCustomerInfo(state) {
  return dispatch => {
    dispatch({
      type: types.SAVE_TEMP_CUSTOMER_INFO,
      state,
    });
  };
}

export function startEditingCart() {
  return {
    type: types.START_EDITING_CART,
  };
}

export function submitEndEditingCart() {
  return async (dispatch, getState) => {
    dispatch({
      type: types.SUBMIT_END_EDITING_CART,
    });
    dispatch(storeCart());
  };
}

export function cancelEditingCart() {
  return {
    type: types.CANCEL_EDITING_CART,
  };
}

export function startAddingProductToCombo(addProduct, hideCategoryFilter = true) {
  return dispatch => {
    dispatch(
      screenStateActions.updateScreenState('ProductDetail', { addProduct, hidePromotion: true, isAddingProductToCombo: true })
    );
    dispatch(
      screenStateActions.updateScreenState('ProductList', {
        shouldSearchInScreen: true,
        hideCategoryFilter,
        goToDetail: params => {
          RootNavigationService.navigate({ routeName: 'ProductDetail', key: 'product-detail-combo' }, params);
        },
      })
    );
  };
}

export function stopAddingProductToCombo() {
  return dispatch => {
    dispatch(
      screenStateActions.updateScreenState('ProductDetail', {
        addProduct: null,
        hidePromotion: false,
        isAddingProductToCombo: false,
      })
    );
    dispatch(
      screenStateActions.updateScreenState('ProductList', {
        shouldSearchInScreen: false,
        hideCategoryFilter: false,
        goToDetail: null,
      })
    );
  };
}

export function changeCombo(combo, onResult) {
  return async (dispatch, getState) => {
    const { uuid, quantity } = combo;
    let comboInCart = getState().cart.combos.find(item => item.uuid === uuid);
    let oldQuantity = comboInCart ? comboInCart.quantity : 0;

    if (combo.promotion && combo.promotion.from !== 'none') {
      const { promotion } = combo;
      const { promotionsNew, promotionsOld } = getState().firebase;
      const cart = getState().cart;
      let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
        promotionsNew,
        promotionsOld,
      });
      let promoQuantityInCart = cartUtils.getAppliedPromotionQuantityInCart(
        cart,
        promotion.from,
        promotion.programKey,
        promotion.key
      );

      if (quantity + promoQuantityInCart - oldQuantity > quantityLeft) {
        onResult && onResult(false, strings.exceed_promotion_quantity);
        return;
      }
    }

    dispatch({
      type: types.CHANGE_COMBO,
      combo,
    });
    let cart = getState().cart;
    if (cartUtils.shouldStoreCart(cart)) {
      AsyncStorage.setItem(Storage.getUserKeyName(constants.CART_STORAGE_KEY), JSON.stringify(cart));
    }
  };
}

export function changeComboQuantity(uuid, quantity, onResult) {
  return async (dispatch, getState) => {
    let comboInCart = getState().cart.combos.find(item => item.uuid === uuid);
    let oldQuantity = comboInCart.quantity;

    if (comboInCart.promotion && comboInCart.promotion.from !== 'none') {
      const { promotion } = comboInCart;
      const { promotionsNew, promotionsOld } = getState().firebase;
      const cart = getState().cart;
      let quantityLeft = getPromotionQuantityLeft(promotion.from, promotion.programKey, promotion.key, {
        promotionsNew,
        promotionsOld,
      });
      let promoQuantityInCart = cartUtils.getAppliedPromotionQuantityInCart(
        cart,
        promotion.from,
        promotion.programKey,
        promotion.key
      );

      if (quantity + promoQuantityInCart - oldQuantity > quantityLeft) {
        onResult && onResult(false, strings.exceed_promotion_quantity);
        return;
      }
    }

    dispatch({
      type: types.CHANGE_COMBO_QUANTITY,
      uuid,
      quantity,
    });
    let cart = getState().cart;
    if (cartUtils.shouldStoreCart(cart)) {
      AsyncStorage.setItem(Storage.getUserKeyName(constants.CART_STORAGE_KEY), JSON.stringify(cart));
    }
  };
}

export function removeCombo(uuid, keepProduct) {
  return dispatch => {
    dispatch({
      type: types.REMOVE_COMBO,
      uuid,
      keepProduct,
    });

    dispatch(storeCart());
  };
}

export function validateCartPromotion() {
  return (dispatch, getState) => {
    const state = getState();
    const currentCart = cartUtils.getCurrentCart(state.cart);

    let verifiedData = cartUtils.verifyPromotionsInCart(currentCart, state.firebase, state.product.product_details);
    if (verifiedData.length > 0) {
      dispatch(forceRemoveItemsAndCombos(verifiedData));
    }
  };
}

export function forceRemoveItemsAndCombos(removeData) {
  return dispatch => {
    dispatch({
      type: types.FORCE_REMOVE_ITEMS_AND_COMBOS,
      removeData,
    });

    dispatch(storeCart());
  };
}

export function changeVoucher(voucher) {
  return {
    type: types.CHANGE_VOUCHER,
    voucher,
  };
}

export function changeItemAdditionalDiscount({ applyFor, uuid, value, reason }) {
  return {
    type: types.CHANGE_ITEM_ADDITIONAL_DISCOUNT,
    applyFor,
    uuid,
    value,
    reason,
  };
}

export function changeGrandDiscount({ value }) {
  return {
    type: types.CHANGE_GRAND_DISCOUNT,
    value,
  };
}

export function removeDenyProducts(skus) {
  return async dispatch => {
    dispatch({
      type: types.REMOVE_DENY_PRODUCTS,
      skus,
    });

    dispatch(storeCart());
  };
}

export function loadCartFromFirebase(phone) {
  return async dispatch => {
    let snapshot = await firebase
      .database()
      .ref('/users/' + phone + '/cart')
      .once('value');
    if (snapshot.val()) {
      dispatch({
        type: types.LOAD_CART_FROM_FIREBASE,
        cart: snapshot.val(),
      });
      //save cart to storage also
      dispatch(storeCart());
    }
  };
}
