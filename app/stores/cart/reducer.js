import { cloneDeep } from 'lodash';

import * as types from './action-types';
import * as cartUtils from '../../utils/cart';
import { combosEqual } from '../../utils/combo';
import { flatBenefit } from '../../modules/promotion/benefit';
import { equalPromotion, getNoPromotion } from '../../modules/promotion';

const initialState = {
  items: [],
  combos: [],
  totalInvoice: 0,
  customerInfo: {},
  tempCustomerInfo: {},
  additional: {
    discount: {
      value: 0,
    },
  },
  productQuantity: 0,
  isEmptyCart: true,
  voucher: null,
  editingCart: null,
};

export default function reduce(state = initialState, action = {}) {
  let items = [...state.items];
  let newState = state;

  switch (action.type) {
    case types.CLEAR_CART:
      newState = cloneDeep(initialState);
      break;

    case types.ADD_PRODUCT_TO_CART:
      newState = reduceWithEditingCart(state, action, addProductToCart);
      break;

    case types.CHANGE_PRODUCT:
      newState = reduceWithEditingCart(state, action, changeProduct);
      break;

    case types.UPDATE_QUANTITY_PRODUCT:
      newState = reduceWithEditingCart(state, action, updateProductQuantity);
      break;

    case types.LOAD_CART_FROM_STORAGE:
      newState = {
        ...state,
        ...postReduce(action.cart || initialState),
      };
      break;

    case types.LOAD_PRODUCT_CART_FROM_STORAGE:
      newState = {
        ...state,
        items: action.items,
      };
      break;

    case types.DELETE_PRODUCT_CART:
      newState = {
        ...state,
        items: items.filter(item => item.uuid != action.uuid),
      };
      break;

    case types.ADD_FOUND_CUSTOMERS:
      newState = {
        ...state,
        foundCustomers: action.foundCustomers,
      };
      break;

    case types.SAVE_CUSTOMER_INFO:
      newState = { ...state, customerInfo: action.customerInfo };
      break;

    case types.SAVE_TEMP_CUSTOMER_INFO:
      newState = { ...state, tempCustomerInfo: action.state };
      break;

    case types.START_EDITING_CART:
      newState = startEditingCart(state);
      break;

    case types.CANCEL_EDITING_CART:
      newState = cancelEditingCart(state);
      break;

    case types.SUBMIT_END_EDITING_CART:
      newState = submitEditingValue(state);
      break;

    case types.CHANGE_GRAND_DISCOUNT:
      newState = reduceWithEditingCart(state, action, changeGrandDiscount);
      break;

    case types.CHANGE_ITEM_ADDITIONAL_DISCOUNT:
      newState = reduceWithEditingCart(state, action, changeItemAdditionalDiscount);
      break;

    case types.CHANGE_COMBO:
      newState = reduceWithEditingCart(state, action, changeCombo);
      break;

    case types.CHANGE_COMBO_QUANTITY:
      newState = reduceWithEditingCart(state, action, changeComboQuantity);
      break;

    case types.REMOVE_COMBO:
      newState = reduceWithEditingCart(state, action, removeCombo);
      break;

    case types.FORCE_REMOVE_ITEMS_AND_COMBOS:
      newState = reduceWithEditingCart(state, action, forceRemoveItemsAndCombos);
      break;

    case types.CHANGE_VOUCHER:
      newState = reduceWithEditingCart(state, action, changeVoucher);
      break;

    case types.REMOVE_DENY_PRODUCTS:
      return reduceWithEditingCart(state, action, removeDenyProducts);

    case types.LOAD_CART_FROM_FIREBASE:
      return { ...initialState, ...action.cart };

    default:
      newState = state;
  }

  return newState;
}

export function reduceWithEditingCart(state, action, reduce) {
  if (state.editingCart) {
    return {
      ...state,
      editingCart: postReduce(reduce(state.editingCart, action)),
    };
  } else {
    return postReduce(reduce(state, action));
  }
}

function postReduce(state) {
  let isEmptyCart = cartUtils.checkEmptyCart(state);

  return {
    ...state,
    totalInvoice: cartUtils.calculateTotalCartInvoice(state),
    isEmptyCart,
    productQuantity: cartUtils.countProductInCart(state),
    voucher: !isEmptyCart ? state.voucher : null,
    additional: !isEmptyCart ? state.additional : null,
  };
}

export function changeProduct(state, action) {
  let { items } = state;
  let { product } = action;

  let canMergeItemIndex = items.findIndex(
    item => item.uuid !== product.uuid && item.sku === product.sku && equalPromotion(item.promotion, product.promotion)
  );
  let existedItem = items.find(item => item.uuid === product.uuid);
  product = addFlattedBenefit(product, existedItem);

  if (!existedItem && canMergeItemIndex === -1) {
    items = [...items, product];
  } else {
    items = items
      .map((item, index) => {
        if (index === canMergeItemIndex) {
          return {
            ...item,
            quantity: item.quantity + product.quantity,
            asiaPromotion: product.asiaPromotion,
            image: item.image || product.image || '',
          };
        } else if (item.uuid === product.uuid) {
          return canMergeItemIndex === -1 ? product : null;
        } else {
          return item;
        }
      })
      .filter(item => item && item.quantity > 0);
  }

  return {
    ...state,
    items,
  };
}

export function removeProduct(state, action) {
  let { items } = state;
  let { uuid } = action;
  let newItems = items.filter(item => item.uuid !== uuid);

  return {
    ...state,
    items: newItems.length !== items.length ? newItems : items,
  };
}

export function addProductToCart(state, action) {
  let { items } = state;
  let { product } = action;

  let existedItemIndex = items.findIndex(item => item.sku === product.sku && equalPromotion(item.promotion, product.promotion));
  if (existedItemIndex === -1) {
    items = [...items, product];
  } else {
    items = items.map((item, index) => {
      if (index === existedItemIndex) {
        return {
          ...item,
          quantity: item.quantity + product.quantity,
          asiaPromotion: product.asiaPromotion,
          image: item.image || product.image || '',
        };
      } else {
        return item;
      }
    });
  }

  return {
    ...state,
    items,
  };
}

export function updateProductQuantity(state, action) {
  let { items } = state;
  let newState = null;

  if (action.quantity === -1) {
    newState = {
      ...state,
      items: items.filter(item => item.uuid != action.uuid),
    };
  } else {
    newState = {
      ...state,
      items: items.map(item => {
        if (item.uuid === action.uuid && action.quantity >= 0 && action.quantity !== item.quantity) {
          return { ...item, quantity: action.quantity };
        } else {
          return item;
        }
      }),
    };
  }

  return newState;
}

export function updateGiftQuantityInCartItem(item, itemQuantity) {
  let gifts = item.gifts || [];

  return gifts.map(gift => ({ ...gift, quantity: itemQuantity * gift.quantityPerItem }));
}

export function startEditingCart(state) {
  let newState = { ...state, editingCart: null };
  delete newState['editingCart'];

  return { ...newState, editingCart: cloneDeep(newState) };
}

export function cancelEditingCart(state) {
  return {
    ...state,
    editingCart: null,
  };
}

export function submitEditingValue(state) {
  const editingCart = state.editingCart || state;

  return {
    ...editingCart,
    editingCart: null,
  };
}

export function changeGrandDiscount(state, action) {
  return {
    ...state,
    additional: {
      discount: {
        value: action.value,
      },
    },
  };
}

export function changeItemAdditionalDiscount(state, action) {
  let { items, combos } = state;
  const { applyFor, uuid, value, reason, discountIndex } = action;

  if (applyFor === 'combo') {
    combos = changeAdditionalDiscount(combos, uuid, value, reason, discountIndex);
  } else {
    items = changeAdditionalDiscount(items, uuid, value, reason, discountIndex);
  }

  return { ...state, items, combos };
}

function changeAdditionalDiscount(items, uuid, value, reason) {
  let newItems = [];
  for (let item of items) {
    if (item.uuid === uuid) {
      let additional = item.additional || {};
      newItems.push({ ...item, additional: { ...additional, discount: { value, reason } } });
    } else {
      newItems.push(item);
    }
  }

  return newItems;
}

function changeCombo(state, action) {
  const { items, combos } = state;
  const { combo } = action;

  let canMergeItemIndex = combos.findIndex(item => combosEqual(item, combo));
  let existedCombo = combos.find(item => item.uuid === combo.uuid);
  let newCombos = [];

  let comboAndItemsAfterIgnored = ignoreComboProductsFromItems(combo, items);
  let newCombo = addFlattedBenefit(comboAndItemsAfterIgnored.combo, existedCombo);
  newCombo = { ...newCombo, productQuantityInCombo: cartUtils.getTotalProductQuantityInCombo(newCombo) };
  let newItems = comboAndItemsAfterIgnored.items;

  if (!existedCombo && canMergeItemIndex === -1) {
    newCombos = [...combos, newCombo];
  } else {
    newCombos = combos
      .map((item, index) => {
        if (index === canMergeItemIndex) {
          return {
            ...item,
            quantity: item.quantity + combo.quantity,
          };
        } else if (item.uuid === combo.uuid) {
          return canMergeItemIndex === -1 ? { ...item, ...combo } : null;
        } else {
          return item;
        }
      })
      .filter(item => item && item.quantity > 0);
  }

  return { ...state, combos: newCombos, items: newItems };
}

function addFlattedBenefit(item, prevItem) {
  if (!prevItem || item.promotion.flattedBenefits !== prevItem.promotion.flattedBenefits) {
    return { ...item, flattedBenefits: flatBenefit(item.promotion.benefit) };
  } else {
    return item;
  }
}

function ignoreComboProductsFromItems(combo, items) {
  const { products, extraProducts, quantity } = combo;
  if (quantity <= 0) {
    return { combo, items };
  }

  let newItems = items;
  let ignoredProductInCombo = ignoreProductInCombo(newItems, products, quantity);
  let newProducts = ignoredProductInCombo.products;
  newItems = ignoredProductInCombo.items;

  ignoredProductInCombo = ignoreProductInCombo(newItems, extraProducts, quantity);
  let newExtraProducts = ignoredProductInCombo.products;
  newItems = ignoredProductInCombo.items;

  return { combo: { ...combo, products: newProducts, extraProducts: newExtraProducts }, items: newItems };
}

function ignoreProductInCombo(items, products, comboQuantity) {
  let comboUsedProductInCart = hasComboUsedProductInCart(products);
  let newItems = items;
  let newProducts = products;
  let hasChangedItems = false;
  if (comboUsedProductInCart) {
    newItems = items
      .map(item => {
        let itemInProduct = products.find(product => product.uuid === item.uuid);
        if (itemInProduct) {
          hasChangedItems = true;
          return { ...item, quantity: item.quantity - itemInProduct.quantity * comboQuantity };
        } else {
          return item;
        }
      })
      .filter(item => item.quantity > 0);

    newProducts = products.map(removeUuidInComboProduct);
  }

  return { items: hasChangedItems ? newItems : items, products: newProducts };
}

function hasComboUsedProductInCart(products) {
  return products.some(item => item.uuid);
}

function removeUuidInComboProduct(product) {
  return { ...product, uuid: null };
}

function changeComboQuantity(state, action) {
  const { combos } = state;
  const { uuid, quantity } = action;
  let newCombos = combos.map(item => {
    if (item.uuid === uuid && item.quantity !== quantity) {
      return { ...item, quantity };
    } else {
      return item;
    }
  });

  return { ...state, combos: newCombos };
}

function removeCombo(state, action) {
  const { combos } = state;
  const { uuid, keepProduct } = action;
  let existedCombo = combos.find(item => item.uuid === uuid);
  let newCombos = combos.filter(item => item.uuid !== uuid);
  let newState = { ...state, combos: newCombos };

  if (keepProduct) {
    let productsToAddToCart = cartUtils.getProductsInComboToAddToCart(existedCombo);
    for (let product of productsToAddToCart) {
      newState = changeProduct(newState, { product });
    }
  }

  return newState;
}

function changeVoucher(state, action) {
  let newState = { ...state, voucher: action.voucher };
  if (action.voucher !== null) {
    newState = removeAllProductPromotionInCart(newState);
  }

  return newState;
}

function removeProductPromotion(state, { uuid }) {
  let { items } = state;
  let newState = state;
  let existedItem = items.find(item => item.uuid === uuid);
  if (existedItem && existedItem.promotion && existedItem.promotion.from !== 'none') {
    newState = changeProduct(state, { product: { ...existedItem, promotion: getNoPromotion() } });
  }

  return newState;
}

export function removeAllProductPromotionInCart(cart) {
  let { items, combos } = cart;
  let newCart = cart;
  for (let item of items) {
    newCart = removeProductPromotion(newCart, item);
  }
  for (let combo of combos) {
    newCart = removeCombo(newCart, { uuid: combo.uuid, keepProduct: true });
  }

  return newCart;
}

export function forceRemoveItemsAndCombos(state, action) {
  const { removeData } = action;
  let newState = state;
  for (let item of removeData) {
    if (item.type === 'item') {
      newState = removeProduct(newState, { uuid: item.uuid });
    } else {
      newState = removeCombo(newState, { uuid: item.uuid, keepProduct: false });
    }
  }

  return newState;
}

function removeDenyProducts(state, action) {
  let { items, combos } = state;
  let { skus } = action;
  items = items.filter(item => !skus.includes(item.sku));
  combos = combos.filter(combo => {
    let productsInCombo = cartUtils.getProductsInCombo(combo);
    return productsInCombo.every(product => !skus.includes(product.sku));
  });

  return {
    ...state,
    items,
    combos,
  };
}
