export default {
  items: [
    {
      uuid: 'b8e34a4b-2355-41e6-afc4-9b5b5c8797df',
      sku: '1700773',
      name: 'Máy tính xách tay/ Laptop Acer AS VX5-591G-70XM (NH.GM2SV.001) (Đen)',
      category: '01-N001-01-01-01',
      image:
        'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/3/5/35124_ltac407_1_1.jpg',
      price: 19890000,
      originalPrice: 23290000,
      asiaPromotion: null,
      promotion: {
        name: 'Không dùng khuyến mãi',
        key: 'none',
        from: 'none',
        benefit: null,
      },
      quantity: 1,
      flattedBenefits: [],
      additional: {
        discount: {
          value: 1000000,
          reason: {
            key: 'GG002',
            label: 'Giảm giá cho khách hàng thân thiết',
          },
        },
      },
    },
    {
      uuid: '7de27a9c-7308-46de-8314-0f266eb1e9d3',
      sku: '1700773',
      name: 'Máy tính xách tay/ Laptop Acer AS VX5-591G-70XM (NH.GM2SV.001) (Đen)',
      category: '01-N001-01-01-01',
      image:
        'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/3/5/35124_ltac407_1_1.jpg',
      price: 19890000,
      originalPrice: 23290000,
      asiaPromotion: {
        name: 'Sản phẩm tặng kèm (asia)',
        key: 'asia',
        from: 'asia',
        benefit: {
          type: 'benefit',
          path: '',
          quantity: 1,
          childrenType: 'allOf',
          value: null,
          children: [
            {
              type: 'benefit',
              path: 'children/0',
              updatePath: null,
              quantity: 1,
              childrenType: null,
              value: {
                gift: {
                  sku: '1200237',
                  quantity: 1,
                },
              },
              children: null,
            },
          ],
        },
      },
      promotion: {
        uuid: '6a6ee8bd-5aff-4ed3-9e89-935f37168678',
        programKey: 'MUAGIAMGIA_NGAPQUATANG_112018',
        benefit: {
          type: 'benefit',
          path: '',
          quantity: 1,
          childrenType: 'oneOf',
          value: null,
          children: [
            {
              type: 'benefit',
              path: 'children/0',
              updatePath: null,
              quantity: 1,
              childrenType: null,
              value: {
                grand_discount: 300000,
              },
              children: null,
            },
            {
              type: 'benefit',
              path: 'children/1',
              updatePath: null,
              quantity: 0,
              childrenType: null,
              value: {
                gift: {
                  quantity: 1,
                  sku: 1810177,
                },
              },
              children: null,
            },
            {
              type: 'benefit',
              path: 'children/2',
              quantity: 0,
              childrenType: 'allOf',
              value: null,
              children: [
                {
                  type: 'benefit',
                  path: 'children/2/children/0',
                  quantity: 0,
                  childrenType: 'oneOf',
                  value: null,
                  children: [
                    {
                      type: 'benefit',
                      path: 'children/2/children/0/children/0',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1501962,
                        },
                      },
                      children: null,
                    },
                    {
                      type: 'benefit',
                      path: 'children/2/children/0/children/1',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1702904,
                        },
                      },
                      children: null,
                    },
                    {
                      type: 'benefit',
                      path: 'children/2/children/0/children/2',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1503244,
                        },
                      },
                      children: null,
                    },
                  ],
                },
                {
                  type: 'benefit',
                  path: 'children/2/children/1',
                  updatePath: null,
                  quantity: 0,
                  childrenType: null,
                  value: {
                    gift: {
                      quantity: 1,
                      sku: 1303061,
                    },
                  },
                  children: null,
                },
                {
                  type: 'benefit',
                  path: 'children/2/children/2',
                  quantity: 0,
                  childrenType: 'oneOf',
                  value: null,
                  children: [
                    {
                      type: 'benefit',
                      path: 'children/2/children/2/children/0',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1603119,
                        },
                      },
                      children: null,
                    },
                    {
                      type: 'benefit',
                      path: 'children/2/children/2/children/1',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1603047,
                        },
                      },
                      children: null,
                    },
                  ],
                },
                {
                  type: 'benefit',
                  path: 'children/2/children/3',
                  updatePath: null,
                  quantity: 0,
                  childrenType: null,
                  value: {
                    grand_discount: 100000,
                  },
                  children: null,
                },
              ],
            },
          ],
        },
        updatePath:
          'promotions-dpos/promotions_new/MUAGIAMGIA_NGAPQUATANG_112018/data/MUAGIAMGIA_NGAPQUATANG_112018_LAPTOP_15_20M/quantity_left',
        from: 'promotion_new',
        name: 'Mưa giảm giá - Ngập quà tặng',
        description: 'Laptop từ 15 đến 20 triệu',
        key: 'MUAGIAMGIA_NGAPQUATANG_112018_LAPTOP_15_20M',
        total_value: 300000,
        quantity_left: 1298,
      },
      quantity: 1,
      flattedBenefits: [
        {
          value: {
            grand_discount: 300000,
          },
          updatePath: null,
          quantity: 1,
        },
      ],
    },
  ],
  combos: [
    {
      uuid: '13d18b2c-68d8-4e03-b62f-ae922aeecbce',
      name: 'Mưa giảm giá - Ngập quà tặng',
      description: 'PC dưới 10 triệu',
      price: 21014000,
      products: [
        {
          sku: '1809032',
          name: 'Bộ nhớ DDR4 G.Skill 8GB (3000) F4-3000C16S-8GTZR (Đen)',
          category: '02-N004-01',
          price: 2495000,
          originalPrice: 2495000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/1/_/1_37_36.jpg',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
        {
          sku: '1809184',
          name: 'Bo mạch chính/ Mainboard MSI H310M Pro-VL',
          category: '02-N002-04',
          price: 1789000,
          originalPrice: 1789000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/1/_/1_39_22.jpg',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
        {
          sku: '1806157',
          name: 'Bộ vi xử lý/ CPU Intel Core i5-8600 Processor (9M Cache, up to 4.3GHz)',
          category: '02-N003-02-07',
          price: 6730000,
          originalPrice: 6730000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/i/5/i5-8000-1_1.jpg',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
        {
          sku: '1808460',
          name: 'ổ cứng HDD WD 8TB WD81PURZ (Tím)',
          category: '02-N005-02-02',
          price: 7500000,
          originalPrice: 7500000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/1/_/1_37_41.jpg',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
        {
          sku: '18110002',
          name: 'Thùng máy/ Case Deepcool Matrexx 55-RGB 3Fan',
          category: '02-N009-99',
          price: 1650000,
          originalPrice: 1650000,
          image: '',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
        {
          sku: '18110003',
          name: 'Nguồn/ Power Acbel 80Plus 450',
          category: '02-N010-02',
          price: 850000,
          originalPrice: 850000,
          image: '',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: {
              type: 'benefit',
              path: '',
              quantity: 1,
              childrenType: 'allOf',
              value: null,
              children: [
                {
                  type: 'benefit',
                  path: 'children/0',
                  updatePath: null,
                  quantity: 1,
                  childrenType: null,
                  value: {
                    gift: {
                      sku: '1207655',
                      quantity: 1,
                    },
                  },
                  children: null,
                },
              ],
            },
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
      ],
      extraProducts: [],
      promotion: {
        benefit: {
          type: 'benefit',
          path: '',
          quantity: 1,
          childrenType: 'oneOf',
          value: null,
          children: [
            {
              type: 'benefit',
              path: 'children/0',
              quantity: 1,
              childrenType: 'allOf',
              value: null,
              children: [
                {
                  type: 'benefit',
                  path: 'children/0/children/0',
                  updatePath: null,
                  quantity: 1,
                  childrenType: null,
                  value: {
                    gift: {
                      quantity: 1,
                      sku: 1202983,
                    },
                  },
                  children: null,
                },
                {
                  type: 'benefit',
                  path: 'children/0/children/1',
                  quantity: 1,
                  childrenType: 'oneOf',
                  value: null,
                  children: [
                    {
                      type: 'benefit',
                      path: 'children/0/children/1/children/0',
                      updatePath: null,
                      quantity: 1,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1501962,
                        },
                      },
                      children: null,
                    },
                    {
                      type: 'benefit',
                      path: 'children/0/children/1/children/1',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1702904,
                        },
                      },
                      children: null,
                    },
                    {
                      type: 'benefit',
                      path: 'children/0/children/1/children/2',
                      updatePath: null,
                      quantity: 0,
                      childrenType: null,
                      value: {
                        gift: {
                          quantity: 1,
                          sku: 1503244,
                        },
                      },
                      children: null,
                    },
                  ],
                },
              ],
            },
            {
              type: 'benefit',
              path: 'children/1',
              updatePath: null,
              quantity: 0,
              childrenType: null,
              value: {
                promotion_discount: 200000,
              },
              children: null,
            },
          ],
        },
        from: 'promotion_new',
        programKey: 'MUAGIAMGIA_NGAPQUATANG_112018',
        key: 'MUAGIAMGIA_NGAPQUATANG_112018_PC_10M',
        name: 'Mưa giảm giá - Ngập quà tặng',
        description: 'PC dưới 10 triệu',
        updatePath:
          'promotions-dpos/promotions_new/MUAGIAMGIA_NGAPQUATANG_112018/data/MUAGIAMGIA_NGAPQUATANG_112018_PC_10M/quantity_left',
      },
      quantity: 1,
      flattedBenefits: [
        {
          value: {
            gift: {
              quantity: 1,
              sku: 1202983,
            },
          },
          updatePath: null,
          quantity: 1,
        },
        {
          value: {
            gift: {
              quantity: 1,
              sku: 1501962,
            },
          },
          updatePath: null,
          quantity: 1,
        },
      ],
      productQuantityInCombo: 6,
    },
    {
      uuid: '2ec77872-9e05-42fc-9a2b-aded63cb161a',
      name: 'Mưa giảm giá - Ngập quà tặng',
      description: 'Màn hình từ 4 đến 6 triệu',
      price: 4548000,
      products: [
        {
          sku: '1402479',
          name: "Màn hình LCD Dell 17'' E1715S",
          category: '03-N001-04',
          price: 2350000,
          originalPrice: 2350000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/m/a/man-hinh-may-tinh-dell-17-60hz-e1715s-1.jpg',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 1,
          productInSaleQuantity: 0,
        },
      ],
      extraProducts: [
        {
          sku: '1702857',
          name: 'Chuột máy tính Trust GXT 170 Heron RGB',
          price: 1099000,
          originalPrice: 1099000,
          image:
            'http://test.shop.teksvr.com/media/catalog/product/cache/0/small_image/200x/9df78eab33525d08d6e5fb8d27136e95/uploads/product/p_13899/2017/10/10/13899.png',
          asiaPromotion: {
            name: 'Sản phẩm tặng kèm (asia)',
            key: 'asia',
            from: 'asia',
            benefit: null,
          },
          quantity: 2,
          productInSaleQuantity: 0,
          category: '03-N004-16',
        },
      ],
      promotion: {
        benefit: {
          type: 'benefit',
          path: '',
          quantity: 1,
          childrenType: 'oneOf',
          value: null,
          children: [
            {
              type: 'benefit',
              path: 'children/0',
              quantity: 1,
              childrenType: 'oneOf',
              value: null,
              children: [
                {
                  type: 'benefit',
                  path: 'children/0/children/0',
                  updatePath: null,
                  quantity: 1,
                  childrenType: null,
                  value: {
                    gift: {
                      quantity: 1,
                      sku: 1809454,
                    },
                  },
                  children: null,
                },
                {
                  type: 'benefit',
                  path: 'children/0/children/1',
                  updatePath: null,
                  quantity: 0,
                  childrenType: null,
                  value: {
                    gift: {
                      quantity: 1,
                      sku: 1809455,
                    },
                  },
                  children: null,
                },
                {
                  type: 'benefit',
                  path: 'children/0/children/2',
                  updatePath: null,
                  quantity: 0,
                  childrenType: null,
                  value: {
                    gift: {
                      quantity: 1,
                      sku: 1809456,
                    },
                  },
                  children: null,
                },
              ],
            },
            {
              type: 'benefit',
              path: 'children/1',
              updatePath: null,
              quantity: 0,
              childrenType: null,
              value: {
                grand_discount: 100000,
              },
              children: null,
            },
          ],
        },
        from: 'promotion_new',
        programKey: 'MUAGIAMGIA_NGAPQUATANG_112018',
        key: 'MUAGIAMGIA_NGAPQUATANG_112018_LCD_4_6M',
        name: 'Mưa giảm giá - Ngập quà tặng',
        description: 'Màn hình từ 4 đến 6 triệu',
        updatePath:
          'promotions-dpos/promotions_new/MUAGIAMGIA_NGAPQUATANG_112018/data/MUAGIAMGIA_NGAPQUATANG_112018_LCD_4_6M/quantity_left',
      },
      quantity: 1,
      additional: {
        discount: {
          value: 1000000,
          reason: {
            key: 'GG001',
            label: 'Giảm giá cho đơn hàng lớn, dự án',
          },
        },
      },
      flattedBenefits: [
        {
          value: {
            gift: {
              quantity: 1,
              sku: 1809454,
            },
          },
          updatePath: null,
          quantity: 1,
        },
      ],
    },
  ],
  totalInvoice: 63488000,
  customerInfo: {},
  additional: {
    discount: {
      value: 10000,
    },
  },
  productQuantity: 6,
  isEmptyCart: true,
  voucher: null,
  editingCart: null,
};
