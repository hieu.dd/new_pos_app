import * as types from './action-types';

export const DEFAULT_ACCESS_TOKEN = 'c78c28ecf1b44b519dbc13feaf9d10ef';

export const initialState = {
  anonymousId: null,
  userInfo: {},
  accessToken: '',
  status: '',
  isLogging: false,
  logInWith: '',
  isLogged: false,
  uid: '',
  error: false,
  request_token: '',
};

const originalInitialState = JSON.parse(JSON.stringify(initialState));

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.SET_ANONYMOUS_ID:
      return { ...state, anonymousId: action.anonymousId };
    case types.START_LOGIN:
      return { ...state, isLogging: true, logInWith: action.logInWith };
    case types.FINISH_LOGIN:
      return { ...state, isLogging: false, logInWith: '' };
    case types.LOGIN_FAILED:
      return { ...state, isLogging: false, status: action.err, error: true };
    case types.SAVE_ACCESS_TOKEN:
      return { ...state, accessToken: action.accessToken };
    case types.SET_AUTHENTICATED_USER:
      return { ...state, uid: action.user.uid, userInfo: { ...state.userInfo, ...action.user }, isLogged: true };
    case types.LOG_OUT:
      return { ...originalInitialState, anonymousId: state.anonymousId };

    case types.SAVE_USER_INFO:
      return { ...state, userInfo: { ...state.userInfo, ...action.data } };
    case types.SAVE_REQUEST_TOKEN:
      return { ...state, request_token: action.request_token };
    default:
      return state;
  }
}
