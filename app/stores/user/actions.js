import { Service, Tracking, Util, getConfig } from 'teko-js-sale-library';

import TRACK from '../../config/TrackingPrototype';

export const getCountryByLocation = (location, callback) => {
  return async (dispatch, getState) => {
    let province_id;
    let region_id = null;
    let province_name;

    try {
      //get address from lat lng using BING Map API
      let ret = await Service.User.getAddressAlt(location.latitude, location.longitude);
      //get city
      if (ret.data.resourceSets.length > 0 && ret.data.resourceSets[0].estimatedTotal > 0) {
        let resources = ret.data.resourceSets[0].resources.sort((a, b) => b.name - a.name);
        let city = resources[0].address.adminDistrict;
        if (city === 'Thanh Pho Ho Chi Minh') city = 'Tp. Ho Chi Minh';
        if (city === 'Ba Ria-Vung Tau') city = 'Ba Ria - Vung Tau';
        let province = Util.GeoLoc.getProvinces().find(it =>
          Util.Text.convertVNstring(it.label.toLowerCase()).includes(city.toLowerCase())
        );
        province_id = province && province.key;
        province_name = province.label;
        region_id = parseInt(province.region_id);
        callback && callback(ret.data.resourceSets[0]);
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'getCountryByLocation', error.message, error.stack);
    }
  };
};
