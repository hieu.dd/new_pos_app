import * as types from './action-types';

const initialState = {
  network: {
    isConnected: true,
  },
  app: {
    state: undefined,
  },
  config: {},
  loadingModal: {
    title: null,
    loadingText: null,
    message: '',
    detail: '',
    visible: false,
    backgroundColor: null,
    actions: [],
  },
  animationModal: {
    visible: false,
    title: '',
    message: '',
    detail: '',
    animationType: null,
    onPress: null,
  },
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CHANGE_CONNECTION_STATUS:
      return {
        ...state,
        network: {
          isConnected: action.isConnected,
        },
      };

    case types.CHANGE_APP_STATE:
      return {
        ...state,
        app: {
          ...state.app,
          state: action.state,
        },
      };

    case types.SHOW_HIDE_LOADING_MODAL: {
      let loadingModal = {
        ...state.loadingModal,
        title: action.title,
        message: action.message,
        detail: action.detail,
        loadingText: action.loadingText,
        visible: action.visible,
        backgroundColor: action.backgroundColor,
        actions: action.actions || [],
      };

      return { ...state, loadingModal };
    }

    case types.SHOW_HIDE_ANIMATION_MODAL:
      return {
        ...state,
        animationModal: {
          ...state.animationModal,
          visible: action.visible,
          animationType: action.animationType,
          message: action.message,
          title: action.title || '',
          detail: action.detail || '',
          actions: action.actions || null,
        },
      };

    case types.SYNC_FIREBASE_CONFIG:
      return {
        ...state,
        config: action.data,
      };

    default:
      return state;
  }
}
