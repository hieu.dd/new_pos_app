import { AsyncStorage } from 'react-native';
import moment from 'moment';
import md5 from 'md5';
import { Service, getConfig } from 'teko-js-sale-library';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../../config/TrackingPrototype';
import * as types from './action-types';
import { UNIX_10_MINUTES } from '../../constants';
import { database } from 'firebase';

export function fetchCategories() {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: types.FETCH_CATEGORIES_START,
      });

      let ret = await Service.Product.getCategories();
      if (ret.ok) {
        dispatch({
          type: types.FETCH_CATEGORIES_RESULT,
          success: true,
          data: ret.data.filter(item => item.visible).sort((a, b) => a.sort_weight - b.sort_weight),
        });
      } else throw new Error(ret.data);
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchCategories', error.message, error.stack);

      dispatch({
        type: types.FETCH_CATEGORIES_RESULT,
        success: false,
        data: error,
      });
    }
  };
}

export function getPriceOthersCompany(sku) {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: types.FETCH_PRICE_OTHERS_COMPANY_START,
      });
      let timestamp = moment().unix();
      let strParams = 'sku=' + String(sku) + 'timestamp=' + timestamp + getConfig().PLAN_KEY;
      let sign = md5(strParams);
      let ret = await Service.Product.getPriceOtherCompany(String(sku), timestamp, sign);
      if (!ret.ok) {
        throw new Error(ret.data);
      }
      dispatch({
        type: types.FETCH_PRICE_OTHERS_COMPANY_RESULT,
        data: ret.data.data.data[0].core_products,
        success: true,
      });
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'getPriceOthersCompany', error.message, error.stack);

      dispatch({
        type: types.FETCH_PRICE_OTHERS_COMPANY_RESULT,
        data: error,
        success: true,
      });
    }
  };
}

export function getStatusProduct(sku) {
  return async (dispatch, getState) => {
    try {
      let ret = await Service.Product.getStatusProduct(sku);
      if (ret.ok) {
        let data = ret.data;
        data.status = data.status_srm;
        if (!data.status_srm) {
          throw new Error(data.status_srm);
        }
        dispatch({
          type: types.FETCH_PRODUCT_STATUS_RESULT,
          sku,
          data: data,
        });
      } else {
        throw new Error(ret.data);
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'getStatusProduct', error.message, error.stack);

      dispatch({
        type: types.FETCH_PRODUCT_STATUS_RESULT,
        sku,
        data: { status: '-1', status_detail: 'Không xác định', error },
      });
    }
  };
}

export function fetchProductStatusList(skus) {
  return async (dispatch, getState) => {
    try {
      let ret = await getProductStatusListInMultiPages(skus.join());
      dispatch(fetchProductStatusListResult(ret.data.results));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchProductStatusList', error.message, error.stack);

      let productStatusList = skus.map(sku => ({ sku, status: '-1', status_detail: 'Không xác định', error }));
      dispatch(fetchProductStatusListResult(productStatusList));
    }
  };
}

export async function getProductStatusListInMultiPages(skus) {
  let getProductStatusListCalls = [];
  for (let i = 0; i < skus.length; i += 20) {
    let skusToGet = skus.slice(i, i + 20);
    getProductStatusListCalls.push(Service.Product.getProductStatusList(skusToGet.join()));
  }

  let results = await Promise.all(getProductStatusListCalls);
  let productStatusList = [];
  for (let result of results) {
    if (!result.ok) {
      throw new Error(result.data);
    } else {
      productStatusList = productStatusList.concat(result.data.results);
    }
  }

  return productStatusList.map(item => {
    const { status_srm, ...rest } = item;
    return { ...rest, status: status_srm };
  });
}

export function fetchProductStatusListResult(productStatusList) {
  return {
    type: types.FETCH_PRODUCT_STATUS_LIST_RESULT,
    productStatusList,
  };
}

export function fetchImportPrice(sku) {
  return async (dispatch, getState) => {
    let timestamp = moment().unix();
    try {
      let importPrice = getState().product.import_prices[sku];

      if (importPrice && (!importPrice.isLoading || timestamp - importPrice.timestamp < UNIX_10_MINUTES)) return;

      dispatch(fetchImportPriceStart(sku));
      let strParams = 'sku=' + sku + 'timestamp=' + timestamp + getConfig().PLAN_KEY;
      let sign = md5(strParams);
      let ret = await Service.Product.getImportPrice(sku, timestamp, sign);
      if (ret.ok) {
        let data = ret.data.data.data;
        let importPrice = data.length === 1 ? data[0].import_price : -1;
        dispatch(fetchImportPriceResult(sku, importPrice, timestamp));
      } else {
        throw new Error(ret.data);
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchImportPrice', error.message, error.stack);
      dispatch(fetchImportPriceResult(sku, -1, timestamp));
    }
  };
}

export function fetchImportPriceStart(sku) {
  return {
    type: types.FETCH_IMPORT_PRICE_START,
    sku,
  };
}

export function fetchImportPriceResult(sku, price, timestamp) {
  return {
    type: types.FETCH_IMPORT_PRICE_RESULT,
    sku,
    price,
    timestamp,
  };
}

export function changeCurrentCategory(category) {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: types.CHANGE_CATEGORY,
        category,
      });
    } catch (error) {
      // console.log(error);
    }
  };
}

export function saveOptions(moreOptions) {
  return {
    type: types.SAVE_MORE_OPTIONS,
    moreOptions,
  };
}

export function saveQueryProduct(query) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: types.SAVE_QUERY, query });
    } catch (error) {
      // console.log(error);
    }
  };
}

export function fetchProductDetails(sku, item) {
  return async (dispatch, getState) => {
    try {
      let details = getState().product.product_details;
      let currentTime = new Date().getTime();
      // nếu đã có detail thì sau 5p mới get lại để lấy tồn kho
      if (details[sku] && currentTime - details[sku].timeUpdate < 300000) return;
      dispatch({
        type: types.FETCH_PRODUCT_DETAIL_START,
        sku,
      });
      let ret = await Service.Product.getProductAsiaDetail(sku);
      if (ret.ok) {
        dispatch({
          type: types.FETCH_PRODUCT_DETAIL_RESULT,
          success: true,
          data: { ...item, ...ret.data, price: ret.data.price_w_vat },
          sku,
        });
      } else throw new Error(ret.data);
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchProductDetails', error.message, error.stack);

      dispatch({
        type: types.FETCH_PRODUCT_DETAIL_RESULT,
        success: false,
        sku,
        data: error,
      });
    }
  };
}

export function fetchProducts(page, category_id, name) {
  return {
    type: types.FETCH_PRODUCTS,
    page,
    category_id,
    name,
  };
}

export function fetchProductsStart() {
  return {
    type: types.FETCH_PRODUCTS_START,
  };
}

export function saveProductSearchParams(params) {
  return {
    type: types.SAVE_PRODUCT_SEARCH_PARAMS,
    params,
  };
}

export function trackingSearchProducts(selectedSku, skuLocation) {
  return async (dispatch, getState) => {
    try {
      let params = getState().product.search_params;
      let ret = await Service.Product.trackingSearchProducts({ ...params, selectedSku, skuLocation });
    } catch (error) {
      //
    }
  };
}

export function fetchProductsResult(success, products, suggestions, next_page, moreOptions = null) {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: types.FETCH_PRODUCTS_RESULT,
        success,
        data: products,
        suggestions,
        next_page,
        moreOptions,
      });
      // for (let product of products) {
      //   dispatch(fetchProductDetails(product.sku));
      // }
    } catch (error) {
      // console.log(error);
    }
  };
}

export const fetchHistoriesSearch = callback => {
  return async dispatch => {
    try {
      let historiesSearch = await AsyncStorage.getItem('historiesSearch');
      callback(JSON.parse(historiesSearch));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchHistoriesSearch', error.message, error.stack);
    }
  };
};

export function setHistoriesSearch(historiesSearch) {
  return async (dispatch, getState) => {
    try {
      let histories = JSON.stringify(historiesSearch);
      await AsyncStorage.setItem('historiesSearch', histories);
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'setHistoriesSearch', error.message, error.stack);
    }
  };
}

export function fetchMoreProductsStart() {
  return {
    type: types.FETCH_MORE_PRODUCTS_START,
  };
}

export function fetchMoreProductsResult(success, products, suggestions, next_page) {
  return async (dispatch, getState) => {
    try {
      dispatch({
        type: types.FETCH_MORE_PRODUCTS_RESULT,
        success,
        data: products,
        suggestions,
        next_page,
      });
      // for (let product of products) {
      //   dispatch(fetchProductDetails(product.sku));
      // }
    } catch (error) {
      // console.log(error);
    }
  };
}

export function fetchProductListMagentoDetail(skus) {
  return async (dispatch, getState) => {
    try {
      let { magento_details } = getState().product;
      skus = skus.filter(sku => !magento_details[sku]);

      if (skus.length === 0) return;

      let ret = await Service.Product.getProductMagentoDetail(skus.join());
      if (ret.ok && ret.data.data) {
        dispatch({
          type: types.FETCH_MAGENTO_PRODUCT_LIST_DETAIL,
          productList: ret.data.data,
        });
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchProductMagentoDetail', error.message, error.stack);
    }
  };
}

export function fetchProductMagentoDetail(sku) {
  return async (dispatch, getState) => {
    try {
      let ret = await Service.Product.getProductMagentoDetail(sku);
      if (ret.ok && ret.data.data && ret.data.data[0]) {
        dispatch({
          type: types.FETCH_MAGENTO_PRODUCT_DETAIL,
          data: ret.data.data[0],
          sku,
          success: true,
        });
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'fetchProductMagentoDetail', error.message, error.stack);
    }
  };
}
