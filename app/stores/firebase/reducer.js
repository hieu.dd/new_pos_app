import * as types from './action-types';

const initialState = {
  promotionsOld: {},
  hasInitializedPromotionsOld: false,
  promotionsNew: {},
  hasInitializedPromotionsNew: false,
  vouchers: {},
  payment: {},
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.SYNC_FIREBASE_DATA_RESULT:
      let paths = action.path.split('/');
      let newState = { ...state };
      let cursor = newState;
      paths.forEach((p, i) => {
        if (i === paths.length - 1) {
          cursor[p] = action.data;
        } else {
          cursor[p] = { ...cursor[p] };
          cursor = cursor[p];
        }
      });

      return newState;

    case types.SET_INITIALIZE_PROMOTIONS_OLD_DONE:
      return { ...state, hasInitializedPromotionsOld: true };

    case types.SET_INITIALIZE_PROMOTIONS_NEW_DONE:
      return { ...state, hasInitializedPromotionsNew: true };

    default:
      return state;
  }
}
