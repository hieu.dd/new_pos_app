export const LOCK_TIME_OUT = 900000; // 15m
export const LOCAL_VOUCHER_KEY = '@PV-LOCAL-VOUCHER-KEY@';
export const DEFAULT_REASON = 'GG000008';

export const REASON_LIST = [
  {
    key: 'GG000001',
    label: 'Đơn hàng lớn được sự xác nhận của ngành hàng (>100 triệu đối với SR HCM&BD, > 70tr đối với các SR còn lại)',
  }, // không cộng
  { key: 'GG000002', label: 'Khách hàng so sánh giá đối thủ' },
  { key: 'GG000003', label: 'Ưu đãi cho nhân viên trong Công ty' }, // không cộng
  { key: 'GG000005', label: 'Giải quyết gấp các khiếu nại của KH' },
  { key: 'GG000006', label: 'Giảm giá cho đơn hàng lớn' },
  { key: 'GG000010', label: 'Do thay đổi giá so với giá đã chốt đơn với KH trước đó' },
  { key: 'GG000011', label: 'SP có CTKM nhưng hệ thống lỗi, không áp dụng được KM' }, // không cộng
  { key: 'GG000013', label: 'Giảm giá cho hàng thanh lý, trưng bay (CTKM chưa được set trên hệ thống)' }, // không cộng
  { key: 'GG000014', label: 'Giảm giá cho khách hàng thân thiết' },
];

export const PROMO_LOCK_DURATION = {
  pending: 600000,
  default: 30000,
};

export const DN_TYPE = {
  NEW: 'dn_moi',
  OLD: 'dn_cu',
  NORMAL: 'dn_thuong',
};

export const CUSTOMER_TYPE = {
  INDIVIDUAL: 0,
  ENTERPRISE: 1,
};

export const AGE = [
  { key: 0, label: 'Dưới 18 tuổi' },
  { key: 1, label: 'Từ 18 đến 25 tuổi' },
  { key: 2, label: 'Từ 25 đến 30 tuổi' },
  { key: 3, label: 'Trên 30 tuổi' },
];

export const ASIA_REASON = [
  { key: 0, label: 'Khách trả hàng, đổi hàng mới' },
  { key: 1, label: 'Khách muốn đặt cọc đơn hàng' },
  { key: 2, label: 'Khách muốn mua trả góp' },
  { key: 3, label: 'App bị lỗi khi đặt bằng OM' },
  { key: 4, label: 'POS thanh toán của thu ngân bị lỗi' },
  { key: 5, label: 'Khác' },
];

export const PRODUCT_STATUS = [
  {
    status: 1,
    name: 'Bình thường',
  },
  {
    status: 2,
    name: 'Hàng trưng bày',
  },
  {
    status: 3,
    name: 'Hàng tạm hết',
  },
  {
    status: 4,
    name: 'Hàng sắp về',
  },
  {
    status: 5,
    name: 'Hàng ngưng kinh doanh',
  },
  {
    status: 6,
    name: 'Hàng bán nốt lô là sẽ hết hàng',
  },
];

export const ORDER_HISTORY_TAB = {
  PENDING: 0,
  DENIED: 1,
  DRAFT: 2,
  FINISH: 3,
};

export const UNIX_10_MINUTES = 600;
