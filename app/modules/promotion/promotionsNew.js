import { DateTimeValidator } from 'teko-promotion-parser/dist/src/DateTime';

import * as path from './path';
import { getNormalBenefit, getAllOfBenefit, getOneOfBenefit, getMaxBenefitQuantity } from './benefit';

export function isActiveProgramForAgent(program, now) {
  return isActiveProgram(program, null, 'agent', now);
}

export function isActiveProgram(program, storeId, channel, now) {
  return (
    (isValidStoreId(program, storeId) || isValidChannel(program, channel)) && DateTimeValidator.validate(program.dateTime, now)
  );
}

function isValidStoreId(program, storeId) {
  if (!storeId) return false;

  return !program.branches || program.branches.indexOf(storeId) !== -1;
}

function isValidChannel(program, channel) {
  if (!channel || !program.channels) return false;

  return program.channels.indexOf(channel) !== -1;
}

export function getBenefitFromPromoNew(promoNew, programKey, initialQuantity = 0) {
  let promoKey = promoNew.path ? promoNew.path.split('/')[0] : promoNew.key;

  return mapPromotionsNewBenefit(promoNew.benefit, '', path.getPromotionsNewBenefitPath(programKey, promoKey), initialQuantity);
}

function mapPromotionsNewBenefit(benefit, benefitPath, firebasePath, quantity) {
  if ('is_all_of' in benefit) {
    return getAllOfBenefit(
      benefitPath,
      quantity,
      benefit.is_all_of.map((item, idx) =>
        mapPromotionsNewBenefit(
          item,
          `${benefitPath ? `${benefitPath}/` : ''}children/${idx}`,
          `${firebasePath}/is_all_of/${idx}`,
          quantity
        )
      )
    );
  } else if ('is_one_of' in benefit) {
    return getOneOfBenefit(
      benefitPath,
      quantity,
      benefit.is_one_of.map((item, idx) =>
        mapPromotionsNewBenefit(
          item,
          `${benefitPath ? `${benefitPath}/` : ''}children/${idx}`,
          `${firebasePath}/is_one_of/${idx}`,
          idx === 0 ? quantity : 0
        )
      )
    );
  } else {
    let updatePath = null;
    if ('gift' in benefit && benefit.gift.quantity_left) {
      updatePath = `${firebasePath}/gift/quantity_left`;
    }
    quantity = getMaxBenefitQuantity(benefit, quantity);

    return getNormalBenefit(benefitPath, quantity, { ...benefit }, updatePath);
  }
}
