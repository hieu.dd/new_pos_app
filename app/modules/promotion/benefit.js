'use strict';

import { isEqual } from 'lodash';

import * as path from './path';
import { equalByKeys } from './utils';

export function scaleBenefit(benefit, quantity) {
  if (benefit.quantity === quantity) {
    return benefit;
  } else {
    return {
      ...benefit,
      quantity: quantity,
      children: scaleChildren(benefit.children, quantity, benefit),
    };
  }
}

export function scaleChildren(children, quantity, parent) {
  if (!children) return children;
  let oneOfChildIndex = -1;
  if (parent && parent.childrenType === 'oneOf') {
    oneOfChildIndex = children.findIndex(child => child.quantity > 0);
    oneOfChildIndex = oneOfChildIndex !== -1 ? oneOfChildIndex : 0;
  }

  if (oneOfChildIndex === -1) {
    return children.map(child => scaleBenefit(child, quantity));
  } else {
    return children.map((child, index) => {
      if (index === oneOfChildIndex) {
        return scaleBenefit(child, quantity);
      } else {
        return scaleBenefit(child, 0);
      }
    });
  }
}

export function changeBenefitQuantity(rootBenefit, path, quantity) {
  if (!rootBenefit) return rootBenefit;

  if (!path) {
    return {
      ...rootBenefit,
      quantity,
      children: scaleChildren(rootBenefit.children, quantity, rootBenefit),
    };
  }

  let paths = path.split('/');
  let benefit = getValidBenefitFromPaths(rootBenefit, paths);
  if (!benefit) throw new Error('Path was not valid');

  return changeQuantityFromPaths(rootBenefit, quantity, paths, 0);
}

function getValidBenefitFromPaths(root, paths) {
  let node = root;
  for (let key of paths) {
    node = node[key];
  }

  if (node && node.type === 'benefit') {
    return node;
  } else {
    return null;
  }
}

function changeQuantityFromPaths(node, quantity, paths, currentPathIndex) {
  if (currentPathIndex === paths.length) {
    return {
      ...node,
      quantity,
      children: scaleChildren(node.children, quantity, node),
    };
  } else {
    let currentKey = paths[currentPathIndex];

    if (Array.isArray(node)) {
      return node.map((item, index) => {
        if (String(index) === String(currentKey)) {
          return changeQuantityFromPaths(item, quantity, paths, currentPathIndex + 1);
        } else {
          return item;
        }
      });
    } else {
      return {
        ...node,
        [currentKey]: changeQuantityFromPaths(node[currentKey], quantity, paths, currentPathIndex + 1),
      };
    }
  }
}

export function changeOneAllOfBenefitQuantity(rootBenefit, path) {
  if (!rootBenefit) return rootBenefit;

  if (!path) {
    if (rootBenefit.quantity !== 0) {
      return rootBenefit;
    } else {
      return scaleBenefit(rootBenefit, 1);
    }
  }

  let paths = path.split('/');
  let benefit = getValidBenefitFromPaths(rootBenefit, paths);
  if (!benefit) throw new Error('Path was not valid');

  if (benefit.quantity > 0) return rootBenefit;

  return changeOneAllOfBenefitQuantityFromPaths(rootBenefit, paths, 0, rootBenefit.quantity || 1);
}

function changeOneAllOfBenefitQuantityFromPaths(benefit, paths, currentPathIndex, rootQuantity) {
  if (currentPathIndex === paths.length) {
    return scaleBenefit(benefit, rootQuantity);
  } else {
    let currentKey = paths[currentPathIndex];
    let newChildren = benefit.children || null;

    if (currentKey === 'children') {
      let nextKey = paths[currentPathIndex + 1]; // nextKey is index of child to change value (string)
      if (benefit.childrenType === 'oneOf') {
        newChildren = benefit.children.map((child, index) => {
          if (index === parseInt(nextKey)) {
            return changeOneAllOfBenefitQuantityFromPaths(child, paths, currentPathIndex + 2, rootQuantity);
          } else {
            return scaleBenefit(child, 0);
          }
        });
      } else {
        newChildren = benefit.children.map((child, index) => {
          if (index === parseInt(nextKey)) {
            return changeOneAllOfBenefitQuantityFromPaths(child, paths, currentPathIndex + 2, rootQuantity);
          } else {
            return scaleBenefit(child, rootQuantity);
          }
        });
      }
    }

    return {
      ...benefit,
      quantity: rootQuantity,
      children: newChildren,
    };
  }
}

export function getNormalBenefit(path, quantity, value = null, updatePath = null) {
  return {
    type: 'benefit',
    path,
    updatePath,
    quantity,
    childrenType: null,
    value,
    children: null,
  };
}

export function getAllOfBenefit(path, quantity, children = null) {
  return {
    type: 'benefit',
    path,
    quantity,
    childrenType: 'allOf',
    value: null,
    children,
  };
}

export function getOneOfBenefit(path, quantity, children = null) {
  return {
    type: 'benefit',
    path,
    quantity,
    childrenType: 'oneOf',
    value: null,
    children,
  };
}

export function getBenefitFromPromoNew(promoNew, programKey) {
  let quantity = 1;
  let promoKey = promoNew.path ? promoNew.path.split('/')[0] : promoNew.key;

  return mapPromotionsNewBenefit(promoNew.benefit, '', path.getPromotionsNewBenefitPath(programKey, promoKey), quantity);
}

function mapPromotionsNewBenefit(benefit, benefitPath, firebasePath, quantity) {
  if ('is_all_of' in benefit) {
    return getAllOfBenefit(
      benefitPath,
      quantity,
      benefit.is_all_of.map((item, idx) =>
        mapPromotionsNewBenefit(item, `${benefitPath}/children/${idx}`, `${firebasePath}/is_all_of/${idx}`, quantity)
      )
    );
  } else if ('is_one_of' in benefit) {
    return getOneOfBenefit(
      benefitPath,
      quantity,
      benefit.is_one_of.map((item, idx) =>
        mapPromotionsNewBenefit(
          item,
          `${benefitPath}/children/${idx}`,
          `${firebasePath}/is_one_of/${idx}`,
          idx === 0 ? quantity : 0
        )
      )
    );
  } else {
    let updatePath = null;
    if ('gift' in benefit && benefit.gift.quantity_left) {
      updatePath = `${firebasePath}/gift/quantity_left`;
    }
    quantity = getMaxBenefitQuantity(benefit, quantity);

    return getNormalBenefit(benefitPath, quantity, { ...benefit }, updatePath);
  }
}

export function mapBenefitFromAsiaGifts(gifts) {
  if (!gifts || gifts.length === 0) return null;
  let children = gifts.map((gift, index) => {
    if (gift.altSku) {
      let nestedChildren = [];
      nestedChildren.push(
        getNormalBenefit(`children/${index}/children/0`, { gift: { sku: gift.sku, quantity: gift.quantity } })
      );
      nestedChildren.push(
        getNormalBenefit(`children/${index}/children/1`, { gift: { sku: gift.altSku, quantity: gift.quantity } })
      );

      return getOneOfBenefit(`children/${index}`, 1, children);
    } else {
      return getNormalBenefit(`children/${index}`, 1, { gift: { sku: gift.sku, quantity: gift.quantity } });
    }
  });

  return getAllOfBenefit('', 1, children);
}

export function getMaxBenefitQuantity(benefit, productQuantity) {
  let quantity = productQuantity;
  if (benefit.gift && (benefit.gift.quantity_left || benefit.gift.quantity_left === 0)) {
    quantity = Math.min(quantity, benefit.gift.quantity_left);
  }
  return quantity;
}

export function flatBenefitWithQuantity(benefit, productQuantity) {
  return getFlattedBenefitsWithQuantity(flatBenefit(benefit), productQuantity);
}

export function flatBenefit(benefit) {
  if (!benefit || benefit.quantity <= 0) return [];

  if (benefit.children && benefit.children.length > 0) {
    return benefit.children
      .reduce((flattedBenefits, child) => [...flattedBenefits, ...flatBenefit(child)], [])
      .filter(item => item);
  } else if (benefit.value) {
    return [{ value: benefit.value, updatePath: benefit.updatePath, quantity: benefit.quantity }];
  } else {
    return [];
  }
}

export function getFlattedBenefitsWithQuantity(flattedBenefits, productQuantity) {
  return flattedBenefits
    .map(benefit => {
      if (benefit.value) {
        const { value, quantity, updatePath } = benefit;

        if (value.gift) {
          return {
            sku: value.gift.sku,
            quantity: Math.min(
              value.gift.quantity * quantity * productQuantity,
              value.gift.quantity_left || Number.MAX_SAFE_INTEGER
            ),
            updatePath,
            quantityLeft: value.gift.quantity_left,
          };
        } else {
          return {
            ...value,
            quantity: quantity * productQuantity,
          };
        }
      } else {
        return null;
      }
    })
    .filter(item => item);
}

export function getGiftsAndFirebaseUpdateFromBenefit(flattedBenefits) {
  let firebase = {};
  let gifts = flattedBenefits
    .filter(item => item.hasOwnProperty('gift'))
    .map(item => {
      if (item.quantity === 0) return null;
      let giftSku = item.gift.sku || item.gift.gift;
      firebase[giftSku] = item.updatePath;
      return {
        sku: giftSku,
        quantity: item.quantity * (item.gift.quantity || 1),
      };
    })
    .filter(item => item);

  return { gifts, firebase };
}

export function equalBenefit(a, b) {
  if (a == b) return true;
  if (!a || !b) return false;
  return equalByKeys(a, b, 'type', 'quantity', 'childrenType') && equalBenefitValue(a, b) && equalBenefitChildren(a, b);
}

export function equalBenefitValue(a, b) {
  // currently, only check value
  return (
    ((a.value === null || a.value === undefined) && (b.value === null || b.value === undefined)) || isEqual(a.value, b.value)
  );
}

export function equalBenefitChildren(a, b) {
  if ((a.children === null || a.children === undefined) && (b.children === null || b.children === undefined)) return true;
  if (a.children === b.children) return true;
  if (!a || !b) return false;

  return (
    a.children.length === b.children.length &&
    a.children.every(ai => b.children.find(bi => equalBenefit(ai, bi))) &&
    b.children.every(bi => a.children.find(ai => equalBenefit(ai, bi)))
  );
}
