import { isEqual } from 'lodash';

export function getProductPrice(product) {
  return product.price_w_vat || product.price || 0;
}

export function equalByKeys(a, b, ...keys) {
  return keys.every(
    key => ((a[key] === null || a[key] === undefined) && (b[key] === null || b[key] === undefined)) || a[key] == b[key]
  );
}

export function getObjectProperty(object, ...keys) {
  let property = object;
  for (let key of keys) {
    if (!property) break;

    property = property[key];
  }

  return property;
}

export function getValidConditionForProduct(product, condition, ignoreQuantity = true) {
  if (condition.hasOwnProperty('is_one_of')) {
    for (let subCon of condition.is_one_of) {
      let validCondition = getValidConditionForProduct(product, subCon, ignoreQuantity);
      if (validCondition) {
        return validCondition;
      }
    }
  } else if (condition.hasOwnProperty('category')) {
    let { category } = condition;
    if (checkCategoryCondition(product, condition.category, ignoreQuantity)) {
      return condition.category;
    }
  } else if (condition.hasOwnProperty('product')) {
    if (checkProductCondition(product, condition.product, ignoreQuantity)) {
      return condition.product;
    }
  }

  return null;
}

export function checkProductCondition(product, productCondition, ignoreQuantity = true) {
  return (
    product.sku == productCondition.sku &&
    (ignoreQuantity || !productCondition.quantity || product.quantity === productCondition.quantity)
  );
}

export function checkCategoryCondition(product, categoryCondition, ignoreQuantity = true) {
  return (
    product.category &&
    (product.category === categoryCondition.code ||
      (categoryCondition.include_children && product.category && product.category.indexOf(categoryCondition.code) != -1)) &&
    (ignoreQuantity || !categoryCondition.quantity || product.quantity === categoryCondition.quantity)
  );
}

export function getQuickApplyProduct(condition) {
  let productAndCategory = getProductAndCategory(condition);
  let satisfiedProducts = productAndCategory.filter(item => item.hasOwnProperty('sku'));
  if (satisfiedProducts.length === 1) {
    return satisfiedProducts[0];
  }

  return null;
}

export function getProductAndCategory(condition) {
  let productAndCategory = [];

  if (condition.hasOwnProperty('category')) {
    productAndCategory.push({ category: condition.category.code, quantity: condition.category.quantity });
  } else if (condition.hasOwnProperty('product')) {
    productAndCategory.push({ sku: condition.product.sku, quantity: condition.product.quantity });
  } else if (condition.hasOwnProperty('is_one_of')) {
    for (let subCondition of condition.is_one_of) {
      let con = getProductAndCategory(subCondition);
      productAndCategory = [...productAndCategory, ...con];
    }
  }

  return productAndCategory;
}

export function checkUserScope(promotion = {}, scope = {}) {
  const { branches, channels, groups } = promotion;
  const { storeId, channel, group } = scope;

  return checkStoreId(branches, storeId) || (checkChannel(channels, channel) && checkGroup(groups, group));
}

export function checkStoreId(branches, storeIdToCheck) {
  if (!storeIdToCheck) return false;

  return !branches || branches.indexOf(storeIdToCheck) !== -1;
}

export function checkChannel(channels, channelToCheck) {
  if (!channelToCheck || !channels) return false;

  return channels.indexOf(channelToCheck) !== -1;
}

export function checkGroup(groups, groupToCheck = null) {
  return !groups || groups.indexOf(groupToCheck) !== -1;
}

export function checkValue(valueToTest, operator, value) {
  if (operator === 'is_equal_to') {
    return valueToTest === value;
  } else if (operator === 'is_not_equal_to') {
    return valueToTest !== value;
  } else if (operator === 'is_greater_than') {
    return valueToTest > value;
  } else if (operator === 'is_greater_or_equal_to') {
    return valueToTest >= value;
  } else if (operator === 'is_smaller_than') {
    return valueToTest < value;
  } else if (operator === 'is_smaller_or_equal_to') {
    return valueToTest <= value;
  } else {
    return false;
  }
}

export function compareProductConditions(con1, con2) {
  return isEqual(con1, con2);
}

export function canAddMoreProductForCondition(condition, addedProducts) {
  let maxCountEachCondition = getMaxCountEachCondition(condition);
  if (maxCountEachCondition > 0) {
    let quantity = getProductQuantity(addedProducts);
    return quantity < maxCountEachCondition;
  }

  return true;
}

export function getMaxCountEachCondition(condition) {
  let maxCountEachCondition = 0;
  if (condition.category) {
    maxCountEachCondition = condition.category.max_count_each;
  } else if (condition.category) {
    maxCountEachCondition = condition.product.max_count_each;
  } else {
    maxCountEachCondition = condition.max_count_each;
  }

  return maxCountEachCondition || 0;
}

function getProductQuantity(products) {
  return products.reduce((quantity, item) => quantity + item.quantity, 0);
}

export function positiveOrDefaultValue(value, defaultValue = 999) {
  return value >= 0 ? value : defaultValue;
}
