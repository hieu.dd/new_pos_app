import * as benefitUtils from './benefit';
import { getProductPrice } from './utils';
import { getPromotionsOldQuantityPath } from './path';

export function getPromotionsOld(product, promotions, now, expectedPromoKey) {
  let promoKey = null;
  let promos = promotions || {};

  for (let key in promos) {
    if (expectedPromoKey && key !== expectedPromoKey) continue;

    let promotion = promos[key];
    if (!isValidPromotionForProduct(product, promotion, now)) continue;
    if (checkInPromoTime(promotion.time, now)) {
      if (!promoKey || promos[promoKey].priority < promotion.priority) {
        promoKey = key;
      }
    }
  }

  if (promoKey) {
    let productPromotion = promos[promoKey].data[product.sku];
    let benefit = getBenefitFromPromoOld(productPromotion, product);
    let { note, limited_quantity, quantity } = productPromotion;

    return {
      benefit,
      note,
      from: 'promotion_old',
      limited_quantity,
      programKey: promos[promoKey].key,
      key: String(product.sku),
      name: promos[promoKey].name,
      quantity_left: quantity,
      priority: promos[promoKey].priority,
      updatePath: getPromotionsOldQuantityPath(promoKey, product.sku),
    };
  }

  return null;
}

export function isValidPromotionForProduct(product, promotion, now) {
  return isValidPromotion(promotion, now) && promotion.data[product.sku] && promotion.data[product.sku].quantity > 0;
}

export function isValidPromotion(promotion, now) {
  return (
    promotion &&
    promotion.data &&
    promotion.date.startDate <= now.timestamp &&
    promotion.date.endDate >= now.timestamp &&
    (promotion.channels && promotion.channels.includes('agent')) &&
    (!promotion.day || promotion.day.includes(now.day))
  );
}

function checkInPromoTime(promoTime, now) {
  let isInPromoTime = false;

  if (promoTime && Array.isArray(promoTime)) {
    for (let time of promoTime) {
      if (time.startTime <= now.hour && time.endTime >= now.hour) {
        isInPromoTime = true;
        break;
      }
    }
  } else {
    isInPromoTime = true;
  }

  return isInPromoTime;
}

function getBenefitFromPromoOld(promotionOld, product) {
  if (!promotionOld || !product) return null;

  let { discountPercent, promotionDiscount, promotionPrice, promotionSkus } = promotionOld;
  let originalPrice = getProductPrice(product);
  let gifts = promotionSkus ? promotionSkus.map(convertPromoOldGift) : [];
  let discount = 0;
  let discountObj = {};

  if (promotionDiscount) {
    discount = promotionDiscount;
  } else if (promotionPrice && promotionPrice < originalPrice) {
    discount = originalPrice - promotionPrice;
  } else if (discountPercent && !isNaN(discountPercent) && discountPercent < 100) {
    discount = Math.floor(originalPrice * parseInt(discountPercent) / 100);
    discountObj.discount_percentage = discountPercent;
  }

  let benefitChildren = [];
  if (gifts.length > 0) {
    benefitChildren = gifts.map(gift => benefitUtils.getNormalBenefit(`children/${benefitChildren.length}`, 1, gift));
  }
  if (discount > 0) {
    benefitChildren.push(
      benefitUtils.getNormalBenefit(`children/${benefitChildren.length}`, 0, { promotion_discount: discount, ...discountObj })
    );
  }

  if (benefitChildren.length > 0) {
    return benefitUtils.getAllOfBenefit('', 0, benefitChildren);
  } else {
    return null;
  }
}

function convertPromoOldGift(giftSku) {
  return {
    gift: {
      sku: giftSku,
      quantity: 1,
    },
  };
}
