import * as _ from 'lodash';

import { getPromotionsNewQuantityLeftPath } from './path';
import { checkValue, compareProductConditions } from './utils';

export const combo_status = {
  valid: 'valid',
  invalid_product: 'invalid_product',
  lack_product: 'lack_product',
  less_than: 'less_than',
  larger_than: 'larger_than',
};

export const combo_type = {
  old: 'old',
  new: 'new',
};

export function hasSameProductCondition(c1, c2) {
  let productConditions1 = getProductsConditionInCombo(c1);
  let productConditions2 = getProductsConditionInCombo(c2);

  return compareProductConditions(productConditions1, productConditions2);
}

export function checkComboStatus(combo, products, extraProducts = []) {
  let productConditions = getProductsConditionInCombo(combo);
  let comboType = getComboType(combo);
  let isValidProducts = true;
  if (comboType === combo_type.old) {
    isValidProducts =
      productConditions.length === products.length &&
      productConditions.every(condition =>
        products.some(product => product.price && getValidConditionForProduct(product, condition, false))
      );
  } else {
    isValidProducts = productConditions.some(condition =>
      products.some(product => product.price && getValidConditionForProduct(product, condition, false))
    );
  }
  let comboStatus = combo_status.valid;

  if (products.find(p => !p.price || parseInt(p.price) === 0)) {
    comboStatus = combo_status.invalid_product;
  }

  if (isValidProducts && !checkComboQuantityCondition(combo, products)) {
    isValidProducts = false;
  }

  if (isValidProducts) {
    let { min_value, max_value } = combo;
    let totalValue = calculateComboValue({ products, extraProducts: extraProducts });

    if (min_value && totalValue < min_value) {
      comboStatus = combo_status.less_than;
    } else if (max_value && totalValue > max_value) {
      comboStatus = combo_status.larger_than;
    }
  } else {
    comboStatus = combo_status.lack_product;
  }

  return comboStatus;
}

export function mapPromotionForCombo(combo, benefit) {
  return {
    benefit,
    from: 'promotion_new',
    programKey: combo.programKey,
    key: combo.key,
    name: combo.name,
    description: combo.description,
    updatePath: combo.hasOwnProperty('quantity_left') ? getPromotionsNewQuantityLeftPath(combo.programKey, combo.key) : null,
  };
}

export function checkValidComboStatus(comboStatus) {
  return comboStatus === combo_status.valid || comboStatus === combo_status.larger_than;
}

export function getComboType(combo) {
  if (combo.productCondition.combo) {
    return combo_type.new;
  } else {
    return combo_type.old;
  }
}

export function checkComboNew(combo) {
  return getComboType(combo) === combo_type.new;
}

export function getComboScreen(combo) {
  let comboType = getComboType(combo);
  switch (comboType) {
    case combo_type.old:
      return 'ComboPromotion';
    case combo_type.new:
      return 'ComboNew';
    default:
      return 'ComboPromotion';
  }
}

export function getComboSize(combo) {
  if (getComboType(combo) === combo_type.new) {
    return getMaxProductQuantityInCombo(combo);
  } else {
    return getProductsConditionInCombo(combo).length;
  }
}

export function getMaxProductQuantityInCombo(combo) {
  let comboCondition = combo.productCondition.combo;
  let maxProductQuantityInCombo = -1;
  if (comboCondition) {
    let { quantity_in_combo } = comboCondition;
    if (quantity_in_combo) {
      if (quantity_in_combo.is_smaller_than) {
        maxProductQuantityInCombo = quantity_in_combo.is_smaller_than - 1;
      } else if (quantity_in_combo.is_smaller_or_equal_than) {
        maxProductQuantityInCombo = quantity_in_combo.is_smaller_or_equal_than;
      } else if (quantity_in_combo.is_equal_to) {
        maxProductQuantityInCombo = quantity_in_combo.is_equal_to;
      }
    }
  }

  return maxProductQuantityInCombo;
}

export function getProductsConditionInCombo(combo) {
  if (combo.productCondition.combo) {
    return combo.productCondition.combo.products.is_one_of;
  } else {
    return combo.productCondition.is_all_of;
  }
}

export function isValidComboValue(combo, products, extraProducts) {
  if (!checkComboQuantityCondition(combo, products)) {
    return false;
  }

  let { min_value, max_value } = combo;
  let totalValue = calculateComboValue({ products, extraProducts });
  if ((min_value && totalValue < min_value) || (max_value && totalValue > max_value)) {
    return false;
  } else {
    return true;
  }
}

export function calculateComboValue({ products, extraProducts }) {
  return (
    products.reduce((price, item) => price + item.price * item.quantity, 0) +
    extraProducts.reduce((price, item) => price + item.price * item.quantity, 0)
  );
}

export function checkComboQuantityCondition(combo, products) {
  let quantityCondition = combo.productCondition.combo && combo.productCondition.combo.quantity_in_combo;
  if (quantityCondition) {
    let productQuantityInCombo = getProductQuantityInCombo(products);
    return Object.keys(quantityCondition).every(key => checkValue(productQuantityInCombo, key, quantityCondition[key]));
  }

  return true;
}

export function getProductQuantityInCombo(products, extraProducts = []) {
  return (
    products.reduce((quantity, product) => quantity + product.quantity, 0) +
    extraProducts.reduce((quantity, product) => quantity + product.quantity, 0)
  );
}

export function suggestProductsInCartForCombo(products, combo) {
  if (getComboType(combo) === combo_type.new) {
    return suggestProductsInCartForNewCombo(products, combo);
  }

  let conditionProductsList = suggestConditionProductsForCombo(getProductsConditionInCombo(combo), products, 0);

  return conditionProductsList.length > 0 ? _.maxBy(conditionProductsList, products => products.length) : [];
}

export function suggestProductsInCartForNewCombo(products, combo) {
  let maxQuantityInCombo = getMaxProductQuantityInCombo(combo);
  let productConditions = getProductsConditionInCombo(combo);
  let suggestProducts = [];
  for (let condition of productConditions) {
    let maxCountProductForCondition = getMaxCountEachCondition(condition);
    let satisfiedProducts = getProductsForCondition(products, condition);
    satisfiedProducts.sort(sortByPrice);
    satisfiedProducts = getConditionProductsInQuantity(satisfiedProducts, maxCountProductForCondition);
    suggestProducts = suggestProducts.concat(satisfiedProducts);
  }

  if (maxQuantityInCombo > 0) {
    suggestProducts.sort(sortByPrice);
    suggestProducts = getConditionProductsInQuantity(suggestProducts, maxQuantityInCombo);
  }

  return suggestProducts;
}

export function suggestConditionProductsForNewCombo(comboProductConditions, products) {
  let suggestProducts = [];

  for (let product of products) {
    let isValid = comboProductConditions.some(condition => getValidConditionForProduct(product, condition, false));
    if (isValid) {
      suggestProducts = [...suggestProducts, { ...product, quantity: 1 }];
    }
  }

  return suggestProducts;
}

export function sortByPrice(a, b) {
  return b.price - a.price;
}

export function getConditionProductsInQuantity(products, quantity) {
  if (quantity <= 0) {
    return products;
  }

  let productsInQuantity = [];
  for (let product of products) {
    if (quantity <= 0) {
      break;
    }

    if (quantity >= product.quantity) {
      productsInQuantity.push({ ...product, quantity: product.quantity });
      quantity -= product.quantity;
    } else {
      productsInQuantity.push({ ...product, quantity: quantity });
      quantity = 0;
    }
  }

  return productsInQuantity;
}

export function makeProductsToCheckComboForProductSale(currentCart) {
  if (!currentCart) return [];

  let productsWithoutSale = getProducts(currentCart, true);
  return productsWithoutSale.map(product => ({
    ...product,
    quantity: product.quantity,
  }));
}

export function getProducts(cart) {
  if (!cart) return [];

  let productsObj = {};
  cart.items.forEach(item => {
    if (productsObj[item.sku]) {
      productsObj[item.sku].quantity += item.quantity;
    } else {
      productsObj[item.sku] = {
        ...item,
        promotion: null,
        // uuid: uuidv4(), promotion: getEmptyPromotion()
      };
    }
  });

  return _.values(productsObj);
}

/**
 *
 * @param {*} comboProductConditions
 * @param {*} productsLeft
 * @param {*} conditionIndex
 *
 * productsLeft format:
 * [
 *  {
 *    sku, quantity, name, price, category
 *  }
 * ]
 */
export function suggestConditionProductsForCombo(comboProductConditions, productsLeft, conditionIndex = 0) {
  if (comboProductConditions.length <= conditionIndex) {
    return [];
  }
  let productsForComboList = [];
  let satisfiedProducts = getProductsForCondition(productsLeft, comboProductConditions[conditionIndex]);
  if (satisfiedProducts.length > 0) {
    for (let productForCombo of satisfiedProducts) {
      let productsLeftIgnoreProduct = calculateProductsLeftIgnoreProduct(productsLeft, productForCombo);
      let subProductsForComboList = suggestConditionProductsForCombo(
        comboProductConditions,
        productsLeftIgnoreProduct,
        conditionIndex + 1
      );
      if (subProductsForComboList.length === 0) {
        productsForComboList.push([productForCombo]);
      } else {
        productsForComboList = productsForComboList.concat(subProductsForComboList.map(item => [...item, productForCombo]));
      }
    }
  } else {
    productsForComboList = suggestConditionProductsForCombo(comboProductConditions, productsLeft, conditionIndex + 1);
  }

  return productsForComboList;
}

export function calculateProductsLeftIgnoreProduct(productsLeft, product) {
  return productsLeft.filter(item => item.uuid !== product.uuid || !(!item.uuid && !product.uuid && item.sku === product.sku));
}

export function getProductsLeftsIgnoreProductsInCombo(originalProducts, products = [], extraProducts = [], comboQuantity = 1) {
  let productsLeft = originalProducts;
  productsLeft = productsLeft.filter(item => !products.find(product => product.uuid === item.uuid));
  productsLeft = productsLeft.filter(item => !extraProducts.find(product => product.uuid === item.uuid));

  return productsLeft;
}

export function getProductsForCondition(products, condition) {
  let productsForCondition = [];
  let maxCountProduct = getMaxProductQuantityForCondition(condition);
  for (let product of products) {
    let validCondition = getValidConditionForProduct(product, condition, true);
    if (validCondition) {
      let minQuantityNeeded = validCondition.quantity || Math.min(maxCountProduct, product.quantity) || 1;
      productsForCondition.push({
        // condition: validCondition,
        uuid: product.uuid,
        sku: product.sku,
        name: product.name,
        category: product.category,
        price: product.price,
        quantity: minQuantityNeeded,
        asiaPromotion: product.asiaPromotion,
      });
    }
  }

  return productsForCondition;
}

export function getValidConditionForProduct(product, condition, ignoreQuantity = true) {
  if (condition.hasOwnProperty('is_one_of')) {
    for (let subCon of condition.is_one_of) {
      let validCondition = getValidConditionForProduct(product, subCon, ignoreQuantity);
      if (validCondition) {
        return validCondition;
      }
    }
  } else if (condition.hasOwnProperty('category')) {
    if (checkCategoryCondition(product, condition.category, ignoreQuantity)) {
      return condition.category;
    }
  } else if (condition.hasOwnProperty('product')) {
    if (checkProductCondition(product, condition.product, ignoreQuantity)) {
      return condition.product;
    }
  }

  return null;
}

export function isGrandTotalCombo(combo) {
  return (
    combo.condition &&
    combo.condition.value_condition &&
    combo.condition.value_condition &&
    combo.condition.value_condition.grand_total
  );
}

export function isGrandTotalCondition(valueCondition) {
  return valueCondition && valueCondition.hasOwnProperty('grand_total');
}

export function checkProductCondition(product, productCondition, ignoreQuantity = true) {
  return (
    product.sku === productCondition.sku &&
    (ignoreQuantity || !productCondition.quantity || product.quantity === productCondition.quantity)
  );
}

export function checkCategoryCondition(product, categoryCondition, ignoreQuantity = true) {
  return (
    product.category &&
    (product.category === categoryCondition.code ||
      (categoryCondition.include_children && product.category && product.category.indexOf(categoryCondition.code) != -1)) &&
    (ignoreQuantity || !categoryCondition.quantity || product.quantity === categoryCondition.quantity)
  );
}

export function getMaxCountEachCondition(condition) {
  let maxCountEachCondition = 0;
  if (condition.category) {
    maxCountEachCondition = condition.category.max_count_each;
  } else if (condition.category) {
    maxCountEachCondition = condition.product.max_count_each;
  } else {
    maxCountEachCondition = condition.max_count_each;
  }

  return maxCountEachCondition || 0;
}

export function getMaxProductQuantityForCondition(condition) {
  if (condition.category) {
    return condition.category.max_count_each_product;
  } else if (condition.category) {
    return condition.product.max_count_each_product;
  } else {
    return 0;
  }
}
