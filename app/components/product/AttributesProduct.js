import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import PRODUCT_ATTR_KEYS from '../../config/product_specification.json';
import { screen, colors, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import PropTypes from 'prop-types';

export default class AttributesProduct extends Component {
  state = { visibleAttributes: false };

  onCollapse = () => {
    this.setState({ visibleAttributes: !this.state.visibleAttributes });
  };

  transformAttributes(attributes) {
    let transformedAttributes = Object.keys(attributes)
      .map((key, index) => {
        if (!PRODUCT_ATTR_KEYS[key] || !attributes[key] || attributes[key] === '') {
          return null;
        } else {
          return { label: PRODUCT_ATTR_KEYS[key], value: attributes[key] };
        }
      })
      .filter(item => item !== null);

    return transformedAttributes;
  }

  renderSpecificationTable(data) {
    return (
      <View>
        <View style={{ marginTop: scale(8) }}>
          {data.map((item, index) => {
            return (
              <View style={{ paddingTop: scale(8), flexDirection: 'row' }} key={index}>
                <Text style={styles.specification_key}>{item.label}</Text>
                <Text style={styles.specification_key}>{item.value}</Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  }

  render() {
    let { product } = this.props;
    let { visibleAttributes } = this.state;
    return (
      <View
        style={{
          padding: scale(16),
          backgroundColor: colors.white_three,
          borderWidth: scale(1),
          borderColor: colors.white_two,
        }}
      >
        <TouchableOpacity onPress={this.onCollapse} style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={[textStyles.footnote_medium, { color: colors.black, flex: 1, textAlignVertical: 'center' }]}>
            Thông số kỹ thuật
          </Text>
          <Icon
            name={visibleAttributes ? 'chevron-up' : 'chevron-down'}
            type="material-community"
            size={scale(16)}
            color={colors.darkGray}
          />
        </TouchableOpacity>
        {visibleAttributes ? this.renderSpecificationTable(product ? this.transformAttributes(product.attributes) : []) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  specification_key: {
    fontSize: scale(14),
    flex: 1,
    textAlignVertical: 'center',
    fontFamily: 'sale-text-regular',
  },
});

AttributesProduct.propTypes = {
  product: PropTypes.object.isRequired,
};
