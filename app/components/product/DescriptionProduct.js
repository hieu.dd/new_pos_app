import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { screen, colors, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { Icon } from 'react-native-elements';

class DescriptionProduct extends Component {
  state = {
    visibleDescription: false,
  };
  onCollapse = () => {
    this.setState({
      visibleDescription: !this.state.visibleDescription,
    });
  };
  render() {
    const { description } = this.props;
    const { visibleDescription } = this.state;
    return (
      <View
        style={{
          padding: scale(16),
          backgroundColor: colors.white_three,
          borderWidth: scale(1),
          borderColor: colors.white_two,
        }}
      >
        <TouchableOpacity onPress={this.onCollapse} style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={[textStyles.footnote_medium, { color: colors.black, flex: 1, textAlignVertical: 'center' }]}>
            Mô tả sản phẩm
          </Text>
          <Icon
            name={visibleDescription ? 'chevron-up' : 'chevron-down'}
            type="material-community"
            size={scale(16)}
            color={colors.darkGray}
          />
        </TouchableOpacity>
        {visibleDescription ? (
          <Text style={[textStyles.body1, { marginTop: scale(16) }]}>{description || 'Không có mô tả sản phẩm'}</Text>
        ) : null}
      </View>
    );
  }
}

export default DescriptionProduct;

DescriptionProduct.propTypes = {
  description: PropTypes.string,
};

DescriptionProduct.defaultProps = {
  description: '',
};
