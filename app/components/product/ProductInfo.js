import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';

import ImageWrapper from '../ImageWrapper';
import { screen, colors, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { categories_icon } from '../../config/categories_icon';
export class ProductInfo extends PureComponent {
  onPress = () => {
    this.props.onPress && this.props.onPress();
  };

  render() {
    const { sku, name, containerStyle, category_code, categories, magento_details } = this.props;
    let category = categories.data.find(item => category_code.includes(item.code));
    let image = sku && magento_details[sku] && magento_details[sku].image.base_image;
    return (
      <TouchableOpacity onPress={this.onPress} style={[containerStyle]}>
        <View
          style={{
            width: scale(165),
            paddingVertical: screen.padding.default,
            borderBottomColor: colors.lightGray,
            borderBottomWidth: scale(1),
            paddingHorizontal: scale(12),
            flexDirection: 'row',
          }}
        >
          <Image
            style={{ width: scale(12), height: scale(12), marginRight: scale(6) }}
            source={categories_icon[category.code]}
          />
          <Text style={[textStyles.small, { color: colors.black }]}>{category && category.name.toUpperCase()}</Text>
        </View>
        <View style={styles.imageContainer}>
          <ImageWrapper source={image} style={styles.image} />
        </View>

        <Text numberOfLines={3} style={[textStyles.footnote, { color: 'black', marginHorizontal: scale(12) }]}>
          {name}
        </Text>
        {/* {sku ? (
          <View style={{ marginTop: scale(2) }}>
            <Text style={textStyles.footnote}>
              Mã SP: <Text style={{ color: colors.secondary }}>{sku}</Text>
            </Text>
          </View>
        ) : null} */}
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {
    categories: state.product.categories,
    magento_details: state.product.magento_details,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductInfo);

ProductInfo.propTypes = {
  sku: PropTypes.string,
  name: PropTypes.string,
  onPress: PropTypes.func,
  containerStyle: View.propTypes.style,
};

const styles = StyleSheet.create({
  imageContainer: {
    paddingVertical: scale(8),
    alignItems: 'center',
  },
  image: {
    height: scale(141),
    width: scale(141),
  },
});
