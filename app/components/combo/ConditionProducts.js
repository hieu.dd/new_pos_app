import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, FlatList, StyleSheet } from 'react-native';

import { getAsiaPromotion, getNow } from '../../modules/promotion';
import * as comboUtils from '../../modules/promotion/combo';
import * as promotionUtils from '../../modules/promotion/utils';
import commonStyles, { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { getProductDetailBySku, getImageSource } from '../../utils/product';
import LayoutButton from '../../components/button/LayoutButton';
import ProductInComboItem from './ProductInComboItem';
import { fetchProductListMagentoDetail } from '../../stores/product/actions';
import TitleLabel from '../../components/label/TitleLabel';
function getProductAndCategory(condition) {
  let productAndCategory = [];

  if (condition.hasOwnProperty('category')) {
    productAndCategory.push({
      category: condition.category.code,
      name: condition.category.name,
      quantity: condition.category.quantity,
    });
  } else if (condition.hasOwnProperty('product')) {
    productAndCategory.push({ sku: condition.product.sku, quantity: condition.product.quantity });
  } else if (condition.hasOwnProperty('is_one_of')) {
    for (let subCondition of condition.is_one_of) {
      let con = getProductAndCategory(subCondition);
      productAndCategory = [...productAndCategory, ...con];
    }
  }

  return productAndCategory;
}

export class ConditionProducts extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    let { products, productDetails } = nextProps;
    let hasQuickApplyProduct = false;
    let mappedProducts = nextProps.products.map(item => {
      if (item.isQuickApplyProduct && !item.price) {
        let productDetail = getProductDetailBySku(item.sku, productDetails);
        if (productDetail) {
          hasQuickApplyProduct = true;

          return {
            ...item,
            sku: productDetail.sku,
            name: productDetail.name,
            category: productDetail.category,
            image: getImageSource(productDetail),
            price: parseInt(productDetail.price_w_vat),
            originalPrice: parseInt(productDetail.original_price),
            asiaPromotion: getAsiaPromotion(productDetail, null, 1, getNow()),
            promotion: null,
            quantity: item.quantity,
          };
        } else {
          return item;
        }
      } else {
        return item;
      }
    });

    if (hasQuickApplyProduct) {
      return { products: mappedProducts };
    } else {
      return { products };
    }
  }

  state = {
    products: [],
  };

  componentDidMount = () => {
    if (this.state.products !== this.props.products) {
      this.props.onSetProducts(this.state.products);
    }
    this.props.fetchProductListMagentoDetail(this.state.products.map(item => item.sku));
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return (
      nextProps.products !== this.props.products ||
      nextProps.products !== nextState.products ||
      nextProps.combo !== this.props.combo
    );
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.products !== this.props.products) {
      this.props.onSetProducts(this.state.products);
    }
  };

  getProductConditionTitle = (productCondition, conditionIndex) => {
    let productAndCategory = getProductAndCategory(productCondition);
    let categoryConditions = productAndCategory.filter(condition => condition.hasOwnProperty('category'));

    let conditionTitle = productCondition.name;
    if (!conditionTitle && categoryConditions && categoryConditions.length > 0) {
      conditionTitle = categoryConditions[0].name || categoryConditions[0].category;
    }

    return conditionTitle || '';
  };

  renderConditionProductItem = ({ item, index }) => {
    let { products } = this.state;
    let conditionTitle = this.getProductConditionTitle(item, index);
    let isQuickApplyProduct = promotionUtils.getQuickApplyProduct(item);
    let addedProduct = products.find(product => promotionUtils.getValidConditionForProduct(product, item, false));

    if (addedProduct) {
      return (
        <ProductInComboItem
          item={addedProduct}
          detailList={this.props.detailList}
          loadProductDetail={this.props.loadProductDetail}
          loadGiftDetail={this.props.loadGiftDetail}
          productDetail={addedProduct}
          disabledEdit={isQuickApplyProduct}
          onPressEdit={isQuickApplyProduct ? null : () => this.props.onEditConditionProduct(item, addedProduct)}
          onPressDelete={isQuickApplyProduct ? null : () => this.props.onDeleteConditionProduct(addedProduct)}
        />
      );
    } else {
      return (
        <View style={styles.item}>
          <LayoutButton
            title={`Thêm ${conditionTitle}`}
            buttonStyle={{ height: scale(44), borderRadius: scale(8) }}
            titleStyle={{ fontFamily: 'sale-text-medium' }}
            onPress={() => this.props.onAddConditionProduct(item)}
          />
        </View>
      );
    }
  };

  keyExtractor = (item, index) => String(index);

  render() {
    const { combo } = this.props;
    const { products } = this.state;
    let productConditions = comboUtils.getProductsConditionInCombo(combo);

    return (
      <View>
        <TitleLabel text={'Sản phẩm trong combo'} />
        <FlatList
          numColumns={2}
          keyExtractor={this.keyExtractor}
          data={productConditions}
          extraData={products}
          renderItem={this.renderConditionProductItem}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    productDetails: state.product.product_details,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProductListMagentoDetail: sku => dispatch(fetchProductListMagentoDetail(sku)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConditionProducts);

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    // padding: screen.padding.smaller,
    // borderBottomWidth: 1,
    // borderBottomColor: colors.lightGray,
  },
});
