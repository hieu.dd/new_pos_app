import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet, Image, Animated, Easing } from 'react-native';

import { getProductQuantityInCombo } from '../../modules/promotion/combo';
import { scale } from '../../utils/scaling';
import commonStyles, { screen, colors, textStyles } from '../../resources/styles/common';
import CheckButton from '../common/CheckButton';
import ColoredTextLabel from '../label/ColoredTextLabel';
import ComboDetailContainer from '../../containers/combo/ComboDetailContainer';
import TrapezoidButton from '../button/TrapezoidButton';

export class SwipeComboItem extends PureComponent {
  scrollToBottom = () => {
    this.scroller.scrollTo({ x: 0, y: screen.height });
  };

  onScrollToTop = () => {
    this.scroller.scrollTo({ x: 0, y: 0 });
  };

  onGotoDetail = () => {
    let { item } = this.props;
    if (item.combo) {
      this.props.navigation.navigate('ComboDetail', { combo: item.combo });
    } else {
      this.props.navigation.navigate('ProductDetail', { sku: item.sku });
    }
  };

  render() {
    let { item, index } = this.props;
    return (
      <View style={{ overflow: 'visible' }}>
        <Image
          source={{ uri: item.banner_url }}
          style={{
            marginHorizontal: scale(6),
            width: screen.width - scale(72),
            height: screen.height - scale(160),
            marginTop: scale(80),
          }}
        />
        <View
          style={{
            marginTop: scale(-32),
            alignItems: 'flex-end',
            marginRight: scale(22),
            elevation: scale(5),
          }}
        >
          <TrapezoidButton
            color={colors.primary}
            title={'CHI TIẾT'}
            width={scale(142)}
            height={scale(48)}
            left={scale(16)}
            iconRightName="menu-right"
            iconType="material-community"
            iconColor="white"
            onPress={this.onGotoDetail}
          />
        </View>
      </View>
    );
  }
}

export default SwipeComboItem;

SwipeComboItem.propTypes = {};

SwipeComboItem.defaultProps = {
  checkable: false,
};

const styles = StyleSheet.create({});
