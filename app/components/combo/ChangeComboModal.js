import React, { PureComponent } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import PromoManager from 'teko-promotion-parser';

import { checkComboStatus, checkValidComboStatus, hasSameProductCondition } from '../../modules/promotion/combo';
import { screen, colors } from '../../resources/styles/common';
import KeyboardModal from '../modal/KeyboardModal';
import ModalHeader from '../modal/ModalHeader';
import ComboList from './ComboList';
import withActiveCombos from '../../containers/combo/withActiveCombos';

export class ChangeComboModal extends PureComponent {
  filterCombo = item => {
    const { currentCombo } = this.props;
    const { combo } = item;

    return combo.key !== currentCombo.key && hasSameProductCondition(combo, currentCombo);
  };

  onPressComboItem = item => {
    this.props.onSubmit(item.combo);
    this.props.onClose();
  };

  render() {
    const { visible, onClose, products, activeCombos } = this.props;

    return (
      <KeyboardModal visible={visible} onClose={onClose}>
        <View>
          <ModalHeader title={'Thay đổi combo'} leftButton={{ text: 'Hủy', onPress: onClose }} />
          <ComboList
            containerStyle={styles.comboList}
            products={products}
            activeCombos={activeCombos}
            filterCombo={this.filterCombo}
            shouldShowListHeader={false}
            onPressComboItem={this.onPressComboItem}
          />
        </View>
      </KeyboardModal>
    );
  }
}

export default withActiveCombos(ChangeComboModal);

const styles = StyleSheet.create({
  comboList: {
    flex: -1,
    backgroundColor: 'white',
    paddingBottom: screen.padding.smaller,
  },
});
