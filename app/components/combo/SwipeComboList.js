import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { Util } from 'teko-js-sale-library';

import commonStyles, { textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import ComboItem from '../../components/combo/ComboItem';
import Carousel, { ParallaxImage } from '../carousel_master';
import { colors, screen } from '../../resources/styles/common';
import ComboContainer from '../../containers/combo/ComboContainer';
import CustomStatusBar from '../../components/header/CustomStatusBar';
import BackgroundImage from '../../components/BackgroundImage';
import SwipeComboItem from '../../components/combo/SwipeComboItem';

class SwipeComboList extends PureComponent {
  state = {};

  filterComboPartial(searchText, activeCombos, filterCombo) {
    searchText = Util.Text.convertVNstring(searchText.trim()).toLowerCase();

    activeCombos = activeCombos
      .filter(item => item.searchedName.includes(searchText) || item.searchedDescription.includes(searchText))
      .filter(item => filterCombo(item));

    let canApplyImmediateCombos = activeCombos.filter(combo => combo.isValidCombo);
    let restCombos = activeCombos.filter(combo => !combo.isValidCombo);

    return { canApplyImmediateCombos, restCombos };
  }

  getProductRetail = () => {
    let { promotionsOld } = this.props;
    let keys = Object.keys(promotionsOld);
    if (keys[0]) {
      let data = promotionsOld[keys[0]].data;
      return Object.keys(data).map(sku => data[sku]);
    }
    return [];
  };

  getComboKey = combo => {
    return `${combo.programKey}-${combo.key}`;
  };

  keyExtractor = (item, index) => (item && item.combo && this.getComboKey(item.combo)) || String(index);

  renderComboItem = ({ item }) => {
    let { showBanner } = this.props;
    return (
      <ComboItem
        key={item.combo.key}
        item={item}
        showBanner={showBanner}
        checkable={this.props.checkable}
        selectedCombo={this.props.selectedCombo}
        onPress={this.props.onPressComboItem}
      />
    );
  };
  scrollToTop = () => {
    this.scroller.scrollTo({ x: 0, y: screen.height });
  };
  _renderItem = ({ item, index }, parallaxProps) => {
    return <SwipeComboItem item={item} index={index} navigation={this.props.navigation} />;
  };

  onChangePosition = position => {
    let { activeCombos, searchText, filterCombo, promotionsOld } = this.props;
    let productRetails = this.getProductRetail();
    let { canApplyImmediateCombos, restCombos } = this.filterComboPartial('', activeCombos, filterCombo);
    let data = [...restCombos, ...productRetails];

    this.backgroundRef.setUri(data[position] && data[position].banner_url, position);
  };

  getDataPromotions = () => {};

  render = () => {
    let { activeCombos, searchText, filterCombo, promotionsOld } = this.props;
    let productRetails = this.getProductRetail();
    let { canApplyImmediateCombos, restCombos } = this.filterComboPartial('', activeCombos, filterCombo);
    let data = [...restCombos, ...productRetails];
    if (canApplyImmediateCombos.length === 0 && restCombos.length === 0) {
      return (
        <View style={styles.emptyList}>
          <Text style={textStyles.heading1}>Hiện không có combo nào phù hợp</Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <CustomStatusBar />
        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
          <BackgroundImage initUri={restCombos[0] && restCombos[0].banner_url} ref={ref => (this.backgroundRef = ref)} />
        </View>
        <Carousel
          data={data}
          renderItem={this._renderItem}
          sliderWidth={screen.width}
          itemWidth={screen.width - scale(60)}
          // loop={true}
          changePosition={this.onChangePosition}
        />
      </View>
    );
  };
}

export default SwipeComboList;

SwipeComboList.propTypes = {
  selectedCombo: PropTypes.object,
  searchText: PropTypes.string,
  checkable: PropTypes.bool,
  shouldShowListHeader: PropTypes.bool,
  filterCombo: PropTypes.func,
};

SwipeComboList.defaultProps = {
  searchText: '',
  checkable: false,
  shouldShowListHeader: true,
  filterCombo: () => true,
};

const styles = StyleSheet.create({
  emptyList: {
    backgroundColor: 'white',
    height: scale(64),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
