import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import { changeOneAllOfBenefitQuantity } from '../../modules/promotion/benefit';
import { scale } from '../../utils/scaling';
import { colors, screen, textStyles } from '../../resources/styles/common';
import ThreeLevelBenefit from '../promotion/ThreeLevelBenefit';
import TitleLabel from '../label/TitleLabel';
export class ComboPromotion extends PureComponent {
  render() {
    const { benefit, onChangeBenefit } = this.props;

    return (
      <View style={styles.container}>
        <TitleLabel text="Chọn quà tặng kèm" />
        <View style={{ marginBottom: scale(60) }}>
          <ThreeLevelBenefit benefit={benefit} onChangeBenefit={onChangeBenefit} backgroundColor={colors.lightGray} />
        </View>
      </View>
    );
  }
}

export default ComboPromotion;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
  },
});
