import React, { PureComponent } from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';

import commonStyles, { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import LayoutButton from '../../components/button/LayoutButton';
import ProductItem from './ProductItem';

export class ExtraProducts extends PureComponent {
  onUpdateProductQuantity = (quantity, item) => {
    this.props.onUpdateExtraProductQuantity(quantity, item);
  };

  keyExtractor = (item, index) => String(index);

  renderExtraProductItem = ({ item, index }) => {
    let { asiaPromotions } = this.props;
    // let asiaPromo = asiaPromotions.extraProducts[item.sku];

    return (
      <View style={{ borderBottomColor: colors.lightGray, borderBottomWidth: 1 }}>
        <View style={{ paddingHorizontal: screen.padding.smaller, paddingBottom: screen.padding.default }}>
          <ProductItem
            item={item}
            detailList={this.props.detailList}
            loadProductDetail={this.props.loadProductDetail}
            loadGiftDetail={this.props.loadGiftDetail}
            // benefit={asiaPromo && asiaPromo.benefit}
            // productDetail={addedProduct}
            maxQuantity={Number.MAX_SAFE_INTEGER}
            onPressItemQuantity={this.props.onPressExtraProductQuantity}
            onUpdateProductQuantity={this.onUpdateProductQuantity}
            onPressEdit={this.props.onPressEditExtraProduct}
            onPressDelete={this.props.onPressDeleteExtraProduct}
          />
        </View>
      </View>
    );
  };

  render() {
    const { products, onAddExtraProduct } = this.props;

    return (
      <View style={styles.container}>
        {products && products.length > 0 ? (
          <FlatList keyExtractor={this.keyExtractor} data={products} renderItem={this.renderExtraProductItem} />
        ) : null}
        <View
          style={{
            backgroundColor: 'white',
            padding: screen.padding.smaller,
            borderBottomWidth: 1,
            borderBottomColor: colors.lightGray,
          }}
        >
          <LayoutButton
            title={`Thêm sản phẩm vào combo`}
            buttonStyle={{ height: scale(44), borderRadius: scale(8) }}
            titleStyle={{ fontFamily: 'sale-text-medium' }}
            onPress={onAddExtraProduct}
          />
        </View>
      </View>
    );
  }
}

export default ExtraProducts;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginTop: screen.margin.default,
  },
});
