import React, { PureComponent, Fragment } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';

import * as comboUtils from '../../modules/promotion/combo';
import * as promotionUtils from '../../modules/promotion/utils';
import commonStyles, { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import LayoutButton from '../../components/button/LayoutButton';
import ProductItem from './ProductItem';

function getProductAndCategory(condition) {
  let productAndCategory = [];

  if (condition.hasOwnProperty('category')) {
    productAndCategory.push({
      category: condition.category.code,
      name: condition.category.name,
      quantity: condition.category.quantity,
    });
  } else if (condition.hasOwnProperty('product')) {
    productAndCategory.push({ sku: condition.product.sku, quantity: condition.product.quantity });
  } else if (condition.hasOwnProperty('is_one_of')) {
    for (let subCondition of condition.is_one_of) {
      let con = getProductAndCategory(subCondition);
      productAndCategory = [...productAndCategory, ...con];
    }
  }

  return productAndCategory;
}

export class NewConditionProducts extends PureComponent {
  getProductConditionTitle = (productCondition, conditionIndex) => {
    let productAndCategory = getProductAndCategory(productCondition);
    let categoryConditions = productAndCategory.filter(condition => condition.hasOwnProperty('category'));

    let conditionTitle = null;
    if (categoryConditions && categoryConditions.length > 0) {
      conditionTitle = categoryConditions[0].name || categoryConditions[0].category;
    }

    return conditionTitle;
  };

  renderConditionProductItem = ({ item, index }) => {
    let { products, maxProductQuantityInCombo, productQuantityInCombo } = this.props;
    let conditionTitle = this.getProductConditionTitle(item, index);
    let isQuickApplyProduct = false;
    let addedProducts = products.filter(product => promotionUtils.getValidConditionForProduct(product, item, false));
    let canAddMoreProductToCombo = maxProductQuantityInCombo <= 0 || productQuantityInCombo < maxProductQuantityInCombo;
    let canAddMoreProduct = canAddMoreProductToCombo && promotionUtils.canAddMoreProductForCondition(item, addedProducts);
    let maxCountEachCondition = promotionUtils.getMaxCountEachCondition(item);
    let quantityInThisCondition = comboUtils.getProductQuantityInCombo(addedProducts);

    if (!canAddMoreProduct && addedProducts.length === 0) {
      return null;
    }

    return (
      <View>
        {addedProducts.length > 0 ? (
          <View style={styles.item}>
            <Text style={textStyles.subheading}>{`${index + 1}. ${conditionTitle}`}</Text>
            {addedProducts.map((addedProduct, idx) => {
              let maxQuantityInCombo = comboUtils.getMaxProductQuantityForCondition(item);
              maxQuantityInCombo =
                addedProduct.quantity +
                Math.min(
                  promotionUtils.positiveOrDefaultValue(maxQuantityInCombo - addedProduct.quantity),
                  promotionUtils.positiveOrDefaultValue(maxProductQuantityInCombo - productQuantityInCombo),
                  promotionUtils.positiveOrDefaultValue(maxCountEachCondition - quantityInThisCondition)
                );

              return (
                <Fragment key={addedProduct.sku}>
                  {idx > 0 ? (
                    <View style={{ height: 1, backgroundColor: colors.lightGray, marginTop: screen.margin.default }} />
                  ) : null}
                  <ProductItem
                    key={addedProduct.sku}
                    item={addedProduct}
                    detailList={this.props.detailList}
                    loadProductDetail={this.props.loadProductDetail}
                    loadGiftDetail={this.props.loadGiftDetail}
                    maxQuantity={maxQuantityInCombo}
                    productDetail={addedProduct}
                    onUpdateProductQuantity={this.onUpdateProductQuantity}
                    onPressItemQuantity={this.props.onPressProductQuantity}
                    onPressEdit={() => this.props.onEditConditionProduct(item, addedProduct)}
                    onPressDelete={isQuickApplyProduct ? null : () => this.props.onDeleteConditionProduct(addedProduct)}
                  />
                </Fragment>
              );
            })}
          </View>
        ) : null}
        {canAddMoreProduct ? (
          <View style={styles.item}>
            <LayoutButton
              title={`Thêm ${conditionTitle}`}
              buttonStyle={{ height: scale(44), borderRadius: scale(8) }}
              titleStyle={{ fontFamily: 'sale-text-medium' }}
              onPress={() => this.props.onAddConditionProduct(item)}
            />
          </View>
        ) : null}
      </View>
    );
  };

  onUpdateProductQuantity = (quantity, item) => {
    this.props.onUpdateProductQuantity(quantity, item);
  };

  keyExtractor = (item, index) => String(index);

  render() {
    const { combo, products } = this.props;
    let productConditions = comboUtils.getProductsConditionInCombo(combo);

    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={productConditions}
          extraData={products}
          renderItem={this.renderConditionProductItem}
        />
      </View>
    );
  }
}

export default NewConditionProducts;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginTop: screen.margin.default,
  },
  item: {
    backgroundColor: 'white',
    padding: screen.padding.smaller,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
});
