import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { scale, getLetterSpacing } from '../../utils/scaling';
import commonStyles, { colors, textStyles } from '../../resources/styles/common.js';

const STYLE_CONFIG = {
  error: {
    color: colors.primary,
    backgroundColor: colors.veryLightPink,
  },
  default: {
    color: colors.lightGray,
    backgroundColor: 'white',
  },
};

/**
 * @augments {Component<{  title:string,  description:string,  type:oneOfType(['error'])>}
 */
class TextAlert extends PureComponent {
  render() {
    const { title, description, type } = this.props;
    const { color, backgroundColor } = STYLE_CONFIG[type] || STYLE_CONFIG['default'];

    return (
      <View style={[styles.container, { backgroundColor }]}>
        <View style={commonStyles.row}>
          <Icon name="close-circle" type="material-community" size={scale(17)} color={colors.primary} />
          <Text style={[textStyles.body2, { color, marginLeft: scale(5) }]}>{title}</Text>
        </View>
        {description ? <Text style={[styles.description, { color }]}>{description}</Text> : null}
      </View>
    );
  }
}

TextAlert.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  type: PropTypes.oneOf(['default', 'error']),
};

TextAlert.defaultProps = {
  type: 'default',
};

export default TextAlert;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: scale(16),
    paddingHorizontal: scale(24),
  },
  description: {
    marginTop: scale(4),
    fontSize: scale(13),
    lineHeight: scale(18),
    color: colors.darkGray,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: 'sale-text-regular',
    letterSpacing: getLetterSpacing(-0.2),
  },
});
