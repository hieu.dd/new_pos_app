import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import { scale, getLetterSpacing } from '../../utils/scaling';
import { colors, screen } from '../../resources/styles/common';
import Space from '../Space';

const ALERT_CONFIGS = {
  success: {
    iconType: 'material',
    iconName: 'check-circle',
    color: 'rgb(9,173,57)',
    backgroundColor: 'rgb(184,242,201)',
  },
  error: {
    iconType: 'material-community',
    iconName: 'close-circle',
    color: colors.primary,
    backgroundColor: 'rgb(255,216,212)',
  },
};

export default class Alert extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { type } = nextProps;
    let alertConfig = ALERT_CONFIGS[type];
    if (alertConfig !== prevState.alertConfig) {
      return { alertConfig };
    }

    return null;
  }

  state = {
    alertConfig: ALERT_CONFIGS['success'],
  };

  renderAction = () => {
    const { type, actionText } = this.props;
    const { alertConfig } = this.state;

    if (type === 'success') {
      return (
        <View style={styles.actionContainer}>
          <Space width={scale(4)} />
          <Text style={[styles.actionText, { color: alertConfig.color }]}>{actionText}</Text>
          <Icon name="chevron-right" color={alertConfig.color} size={scale(16)} />
        </View>
      );
    } else if (type === 'error') {
      return (
        <View style={styles.actionContainer}>
          <Space width={scale(4)} />
          <Icon name="close" color={colors.primary} size={scale(16)} />
          <Space width={scale(2)} />
          <Text style={[styles.actionText, { color: alertConfig.color }]}>{actionText}</Text>
          <Space width={scale(4)} />
        </View>
      );
    }
  };

  render() {
    const { title, onPress } = this.props;
    const { alertConfig } = this.state;

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={[styles.container, { backgroundColor: alertConfig.backgroundColor }]}>
          <View style={{ flexDirection: 'row' }}>
            <Icon name={alertConfig.iconName} type={alertConfig.iconType} color={alertConfig.color} size={scale(17)} />
            <Text style={[styles.title, { color: alertConfig.color }]}>{title}</Text>
          </View>
          {this.renderAction()}
        </View>
      </TouchableOpacity>
    );
  }
}

Alert.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    height: scale(50),
    flexDirection: 'row',
    backgroundColor: 'rgb(255,216,212)',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: screen.padding.smaller,
  },
  title: {
    marginLeft: scale(4),
    color: colors.primary,
    fontSize: screen.fontSize.medium,
    letterSpacing: getLetterSpacing(-0.2),
    fontFamily: 'sale-text-medium',
  },
  actionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: scale(8),
    height: scale(24),
    paddingHorizontal: scale(4),
  },
  actionText: {
    marginLeft: scale(2),
    color: colors.primary,
    fontSize: scale(13),
    letterSpacing: getLetterSpacing(-0.1),
    fontFamily: 'sale-text-regular',
  },
});
