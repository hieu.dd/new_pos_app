import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Modal, Alert, FlatList, KeyboardAvoidingView } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

import { screen, textStyles, colors } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import LoadingIndicator from '../common/LoadingIndicator';

export default class ChooseCustomerModal extends Component {
  onSubmit = item => {
    this.props.onChooseCustomer(item);
  };

  onSubmitSuggestion = item => {
    this.props.onChooseSuggest(item);
  };

  renderCustomerItem = ({ item, index }) => {
    let { fieldSearch } = this.props;
    return (
      <TouchableOpacity
        style={{ flex: 1, borderBottomColor: colors.lightGray, borderBottomWidth: 1, paddingVertical: scale(6) }}
        onPress={() => this.onSubmit(item)}
        accessibilityLabel={'on_submit_customer'}
      >
        <Text style={{ fontSize: scale(15) }}>{item.name}</Text>
        <Text style={{ fontSize: scale(12), color: colors.darkGray, marginTop: scale(2) }}>{item.phone}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    let { contacts } = this.props;
    return (
      <Modal visible={this.props.visible} onRequestClose={() => {}} animationType="fade" transparent={true}>
        <KeyboardAvoidingView behavior={'padding'} style={styles.modalContainer}>
          <View style={styles.innerContainer} activeOpacity={1}>
            <View style={styles.modalHeader}>
              <Text style={textStyles.heading1}>Chọn khách hàng</Text>
            </View>
            <View style={{ flex: 1, paddingHorizontal: screen.padding.smaller }}>
              <FlatList
                data={contacts}
                renderItem={this.renderCustomerItem} // {type === 'customer' ? this.renderCustomerItem : this.renderSuggestionItem}
                keyExtractor={item => String(item.id)}
                keyboardShouldPersistTaps={'always'}
              />
            </View>
            <TouchableOpacity
              style={{
                height: scale(44),
                borderTopColor: colors.lightGray,
                borderTopWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={this.props.onClose}
              accessibilityLabel={'close_modal_choose_customer'}
            >
              <Text style={[textStyles.heading1, { color: colors.secondary }]}>Hủy</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  innerContainer: {
    flex: 1,
    width: scale(310),
    maxHeight: scale(310),
    backgroundColor: '#ffffff',
    position: 'relative',
    borderRadius: scale(16),
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: scale(44),
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
  },
  viewContainer: {
    width: '60%',
    maxHeight: '80%',
    backgroundColor: 'white',
    padding: screen.padding.medium * 2,
    paddingBottom: screen.padding.medium,
    borderRadius: 10,
    elevation: 10,
  },
});
