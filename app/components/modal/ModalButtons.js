import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';

import { scale } from '../../utils/scaling';
import { colors, textStyles } from '../../resources/styles/common';
import Space from '../Space';

export class ModalButtons extends PureComponent {
  render() {
    const { actions } = this.props;

    return (
      <View style={styles.container}>
        {actions.map((action, index) => (
          <Fragment key={action.text}>
            {index > 0 ? <Space height={'100%'} width={scale(1)} backgroundColor={colors.lightGray} /> : null}
            <TouchableOpacity style={styles.button} onPress={action.onPress}>
              <Text style={[textStyles.heading1, action.highlight ? styles.highlightButtonText : styles.buttonText]}>
                {action.text}
              </Text>
            </TouchableOpacity>
          </Fragment>
        ))}
      </View>
    );
  }
}

ModalButtons.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      onPress: PropTypes.func,
      highlight: PropTypes.bool,
    })
  ),
};

export default ModalButtons;

const styles = StyleSheet.create({
  container: {
    height: scale(50),
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderTopWidth: 1,
    borderTopColor: colors.lightGray,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'sale-text-regular',
    color: colors.secondary,
  },
  highlightButtonText: {
    fontFamily: 'sale-text-semibold',
    color: colors.secondary,
  },
});
