import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import { scale, getLetterSpacing } from '../../utils/scaling';
import { colors, screen } from '../../resources/styles/common';

const labelStylesConfigs = {
  normal: {
    text: { color: colors.darkGray },
    container: { backgroundColor: colors.lightGray },
  },
  success: {
    text: { color: colors.secondary },
    container: { backgroundColor: colors.duckEggBlue },
  },
  error: {
    text: { color: colors.primary },

    container: { backgroundColor: '#ffefed' },
  },
  info: {
    text: { color: colors.leafyGreen },
    container: { backgroundColor: '#e1f8de' },
  },
};

/**
 * @augments {Component<{  text:string,  type:oneOf(['normal', 'success', 'error', 'info'])>}
 */
class ColoredTextLabel extends PureComponent {
  render() {
    const { text, type } = this.props;
    const labelStyle = labelStylesConfigs[type] || labelStylesConfigs['normal'];

    return (
      <View style={[styles.container, labelStyle.container]}>
        <Text style={[styles.text, labelStyle.text]}>{text}</Text>
      </View>
    );
  }
}

export default ColoredTextLabel;

ColoredTextLabel.propTypes = {
  text: PropTypes.string,
  type: PropTypes.oneOf(['normal', 'success', 'error', 'info']),
};

ColoredTextLabel.defaultProps = {
  text: '',
  type: 'normal',
};

const styles = StyleSheet.create({
  container: {
    borderRadius: scale(8),
    paddingVertical: scale(4),
    paddingHorizontal: screen.padding.default,
  },
  text: {
    fontSize: scale(14),
    lineHeight: scale(19),
    fontFamily: 'sale-text-semibold',
    letterSpacing: getLetterSpacing(-0.15),
  },
});
