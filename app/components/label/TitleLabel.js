import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import { scale, getLetterSpacing } from '../../utils/scaling';
import commonStyles, { colors, screen, textStyles } from '../../resources/styles/common';

/**
 * @augments {Component<{  text:string,  type:oneOf(['normal', 'success', 'error', 'info'])>}
 */
class TitleLabel extends PureComponent {
  render() {
    const { text } = this.props;

    return (
      <View style={[styles.container]}>
        <View style={commonStyles.leftRetangle} />
        <Text style={[textStyles.price, { marginLeft: scale(8), fontFamily: 'sale-text-medium' }]}>{text}</Text>
      </View>
    );
  }
}

export default TitleLabel;

TitleLabel.propTypes = {
  text: PropTypes.string,
};

TitleLabel.defaultProps = {
  text: '',
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: scale(12),
    alignItems: 'center',
  },
});
