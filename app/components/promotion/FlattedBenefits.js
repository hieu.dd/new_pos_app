import React, { PureComponent } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

import commonStyles, { colors, textStyles, screen } from '../../resources/styles/common';
import { scale, getLetterSpacing } from '../../utils/scaling';
import BenefitItem from './BenefitItem';

export class FlattedBenefits extends PureComponent {
  render() {
    const { flattedBenefits } = this.props;

    if (flattedBenefits && flattedBenefits.length > 0) {
      return (
        <View style={styles.container}>
          <View style={commonStyles.full}>
            {flattedBenefits.map((benefit, index) => <BenefitItem key={String(index)} benefit={benefit} />)}
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

export default FlattedBenefits;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: scale(8),
    paddingVertical: scale(4),
    paddingHorizontal: screen.padding.small,
    backgroundColor: colors.lightGray,
  },
});
