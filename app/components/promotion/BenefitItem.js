import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Util } from 'teko-js-sale-library';

import Item from './item/Item';
import GiftItem from './item/GiftItem';

export class BenefitItem extends PureComponent {
  getBenefitDescription(benefit) {
    const { value } = benefit;
    if (value.voucher) {
      return value.voucher.name;
    } else if (value.discount_percentage) {
      return `Giảm giá ${value.discount_percentage}%`;
    } else if (value.promotion_discount) {
      return `Giảm giá ${Util.Text.formatPrice(value.promotion_discount)}`;
    } else if (value.grand_discount) {
      return `Giảm giá toàn đơn ${Util.Text.formatPrice(value.grand_discount)}`;
    } else if (value.promotion_price) {
      return `Giá bán khuyến mãi ${Util.Text.formatPrice(value.promotion_price)}`;
    } else {
      return 'Không có thông tin';
    }
  }

  render() {
    const { benefit } = this.props;

    if (!benefit.value) {
      return null;
    } else if (benefit.value.gift) {
      return (
        <GiftItem
          benefit={benefit}
          productDetails={this.props.productDetails}
          fetchProductDetails={this.props.fetchProductDetails}
        />
      );
    } else {
      let quantity = 1;
      let text = this.getBenefitDescription(benefit);

      return <Item benefit={benefit} quantity={quantity} text={text} />;
    }
  }
}

export default BenefitItem;

BenefitItem.propTypes = {
  benefit: PropTypes.object,
};
