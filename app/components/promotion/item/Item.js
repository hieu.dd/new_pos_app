import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import { scale } from '../../../utils/scaling';
import commonStyles, { screen, textStyles, colors } from '../../../resources/styles/common';
import LoadingIndicator from '../../common/LoadingIndicator';

export class Item extends PureComponent {
  getBenefitQuantity = benefit => {
    let quantity = benefit.quantity || 1;

    return quantity;
  };

  render() {
    const { quantity, text, isLoading } = this.props;

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          {isLoading ? (
            <View style={styles.container}>
              <View style={commonStyles.fullAndCenter}>
                <LoadingIndicator />
              </View>
            </View>
          ) : (
            <Text numberOfLines={2} textBreakStrategy={'balanced'} style={textStyles.body2}>
              {text}
            </Text>
          )}
        </View>
        <View
          style={{
            width: scale(22),
            height: scale(16),
            borderRadius: scale(8),
            borderWidth: scale(1),
            borderColor: colors.lightGray,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text>x{quantity}</Text>
        </View>
      </View>
    );
  }
}

export default Item;

Item.propTypes = {
  quantity: PropTypes.number,
  text: PropTypes.string,
  isLoading: PropTypes.bool,
};

Item.defaultProps = {
  isLoading: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    minHeight: scale(40),
    paddingTop: scale(11),
    paddingBottom: scale(10),
    paddingHorizontal: scale(8),
  },
});
