import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import Dash from 'react-native-dash';
import { Icon } from 'react-native-elements';
import { scale } from '../../utils/scaling';
import commonStyles, { screen, colors, textStyles } from '../../resources/styles/common';
import BenefitItem from './BenefitItem';
import CheckButton from '../common/CheckButton';
import Space from '../Space';

/**
 * @augments {Component<{  benefit:object,  onChangeBenefit:Function>}
 */
class ThreeLevelBenefit extends PureComponent {
  renderFirstLevel = benefit => {
    const { backgroundColor } = this.props;

    if (benefit.childrenType === 'allOf') {
      return (
        <TouchableOpacity onPress={() => this.props.onChangeBenefit(benefit.path)}>
          <View style={{ backgroundColor: 'white', marginHorizontal: scale(12) }}>
            {this.renderCheckboxItem(benefit)}
            {benefit.children && benefit.children.length > 0 ? this.renderThirdLevel(benefit) : this.renderBenefitItem(benefit)}
          </View>
        </TouchableOpacity>
      );
    }

    return <View>{benefit.children.map((child, index) => this.renderFirstLevelItem(child, index, benefit.childrenType))}</View>;
  };

  renderCheckboxItem = benefit => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          height: scale(28),
          borderBottomColor: colors.lightGray,
          borderBottomWidth: scale(1),
          paddingHorizontal: scale(8),
        }}
      >
        <Icon name="gift" type="material-community" color={colors.primary} size={scale(12)} />
        <Text style={[textStyles.tiny, { flex: 1, textAlignVertical: 'center', marginHorizontal: scale(4) }]}>Gói quà</Text>
        <CheckButton
          size={scale(16)}
          checked={benefit.quantity > 0}
          disabled={true}
          onPress={() => this.props.onChangeBenefit(benefit.path)}
        />
      </View>
    );
  };

  renderFirstLevelItem = (benefit, index, childrenType) => {
    const { backgroundColor } = this.props;

    return (
      <View key={String(index)}>
        {index > 0 ? <Space height={screen.margin.smaller} /> : null}
        <TouchableOpacity key={String(index)} onPress={() => this.props.onChangeBenefit(benefit.path)}>
          <View style={{ backgroundColor: 'white', marginHorizontal: scale(12) }}>
            {childrenType === 'oneOf' ? this.renderCheckboxItem(benefit) : null}
            {benefit.children && benefit.children.length > 0
              ? this.renderSecondLevel(benefit)
              : this.renderBenefitItem(benefit)}
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderSecondLevel = benefit => {
    if (benefit.childrenType === 'oneOf') {
      return this.renderThirdLevel(benefit);
    }

    return (
      <View style={commonStyles.full}>
        {benefit.children.map((child, index) => this.renderSecondLevelItem(child, index, benefit.childrenType))}
      </View>
    );
  };

  renderSecondLevelItem = (benefit, index, childrenType) => {
    return (
      <View key={String(index)}>
        {index > 0 ? <Dash style={styles.dash} dashGap={5} dashLength={5} dashColor={colors.black} /> : null}
        {benefit.children && benefit.children.length > 0 ? this.renderThirdLevel(benefit) : this.renderBenefitItem(benefit)}
      </View>
    );
  };

  renderThirdLevel = benefit => {
    return (
      <View style={commonStyles.full}>
        {benefit.children.map((child, index) => this.renderThirdLevelItem(child, index, benefit.childrenType))}
      </View>
    );
  };

  renderThirdLevelItem = (benefit, index, childrenType) => {
    return (
      <View key={String(index)} style={commonStyles.row}>
        {this.renderBenefitItem(benefit)}
        {childrenType === 'oneOf' ? (
          <View style={styles.thirdLevelCheckbox}>
            <CheckButton checked={benefit.quantity > 0} onPress={() => this.props.onChangeBenefit(benefit.path)} />
          </View>
        ) : null}
      </View>
    );
  };

  renderBenefitItem = benefit => {
    return <BenefitItem benefit={benefit} />;
  };

  render() {
    const { benefit } = this.props;

    return <View>{this.renderFirstLevel(benefit)}</View>;
  }
}

export default ThreeLevelBenefit;

ThreeLevelBenefit.propTypes = {
  benefit: PropTypes.object,
  backgroundColor: PropTypes.string,
  onChangeBenefit: PropTypes.func,
};

ThreeLevelBenefit.defaultProps = {
  backgroundColor: 'white',
};

const styles = StyleSheet.create({
  dash: {
    marginRight: scale(8),
    height: 1,
    opacity: 0.1,
  },
  thirdLevelCheckbox: {
    alignItems: 'center',
    paddingTop: scale(4),
  },
});
