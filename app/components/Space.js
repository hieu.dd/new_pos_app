import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

export default class Space extends PureComponent {
  render() {
    const { height, width, backgroundColor } = this.props;

    return <View style={{ height, width, backgroundColor }} />;
  }
}

Space.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backgroundColor: PropTypes.string,
};

Space.defaultProps = {
  width: 1,
  height: 1,
  backgroundColor: 'transparent',
};
