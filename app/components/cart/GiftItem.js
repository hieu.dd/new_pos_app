import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import PromotionItem from './PromotionItem';

class GiftItem extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { productDetails, item } = nextProps;
    let gift = productDetails[item.sku];
    let isLoading = !gift || gift.isLoading;

    if (gift !== prevState.gift || isLoading !== prevState.isLoading) {
      return {
        gift,
        isLoading,
      };
    }

    return null;
  }

  state = {
    isLoading: false,
    gift: null,
  };

  componentDidMount() {
    const { item } = this.props;
    const { gift } = this.state;
    if (!gift) {
      this.props.fetchProductDetails(item.sku);
    }
  }

  render() {
    const { item, hasBorderTop } = this.props;
    const { isLoading, gift } = this.state;
    let text = `${(gift && gift.detail && gift.detail.name) || 'Không có thông tin'}`;

    return <PromotionItem isLoading={isLoading} quantity={item.quantity} text={text} hasBorderTop={hasBorderTop} />;
  }
}

export default GiftItem;

GiftItem.propTypes = {
  item: PropTypes.shape({
    sku: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    quantity: PropTypes.number,
  }),
  hasBorderTop: PropTypes.bool,
  productDetails: PropTypes.object,
  fetchProductDetails: PropTypes.func,
};

GiftItem.defaultProps = {
  hasOrderTop: true,
};
