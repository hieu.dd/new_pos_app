import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';

import { scale } from '../../utils/scaling';
import commonStyles, { colors, screen, textStyles } from '../../resources/styles/common';
import FadeIn from '../animation/FadeIn';
import LoadingIndicator from '../common/LoadingIndicator';

export default class PromotionItem extends PureComponent {
  renderContent = () => {
    const { isLoading, quantity, text, onEdit, onDelete, canEditPromotion } = this.props;

    if (isLoading) {
      return (
        <View style={styles.container}>
          <View style={commonStyles.fullAndCenter}>
            <LoadingIndicator />
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Text style={textStyles.subheading}>{`${quantity} x`}</Text>
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={textStyles.body2}>
            {text}
          </Text>
        </View>
        {canEditPromotion ? (
          <FadeIn show={canEditPromotion}>
            <View style={styles.actionsButton}>
              <TouchableOpacity onPress={onEdit}>
                <View style={styles.button}>
                  <Icon name="edit" color={colors.gray} size={scale(20)} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={onDelete}>
                <View style={styles.button}>
                  <Icon name="close" color={colors.gray} size={scale(20)} />
                </View>
              </TouchableOpacity>
            </View>
          </FadeIn>
        ) : null}
      </View>
    );
  };

  render() {
    const { hasBorderTop } = this.props;

    return (
      <View>
        {hasBorderTop ? (
          <Dash style={{ marginHorizontal: screen.margin.smaller, height: 1, opacity: 0.15 }} dashColor={colors.darkGray} />
        ) : null}
        <TouchableOpacity>{this.renderContent()}</TouchableOpacity>
      </View>
    );
  }
}

PromotionItem.propTypes = {
  isLoading: PropTypes.bool,
  hasBorderTop: PropTypes.bool,
  quantity: PropTypes.number,
  text: PropTypes.string,
  canEditPromotion: PropTypes.bool,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
};

PromotionItem.defaultProps = {
  hasBorderTop: true,
};

const styles = StyleSheet.create({
  container: {
    height: scale(34),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: screen.padding.smaller,
  },
  textContainer: {
    flex: 1,
    marginLeft: screen.padding.smaller,
  },
  actionsButton: {
    flexDirection: 'row',
    marginLeft: screen.margin.smaller,
  },
  button: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: screen.padding.default,
  },
});
