import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, TextInput, View, StyleSheet } from 'react-native';
import { Util } from 'teko-js-sale-library';
import { Icon } from 'react-native-elements';

import { properNumberString } from '../../utils';
import { scale, getLetterSpacing } from '../../utils/scaling';
import { screen, colors, textStyles } from '../../resources/styles/common';
import ModalHeader from '../modal/ModalHeader';
import KeyboardModal from '../modal/KeyboardModal';

/**
 * @augments {Component<{  title:string,  placeholder:string,  unitText:string,  visible:boolean,  submitText:string,  cancelText:string,  onSubmit:Function,  onClose:Function.isRequired,  properValue:Function>}
 */
class ChangeValueModal extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { visible } = nextProps;
    if (visible && !prevState.visible) {
      return {
        value: '',
      };
    }

    return null;
  }

  state = {
    value: '',
  };

  componentDidUpdate(prevProps) {
    if (this.props.visible && !prevProps.visible) {
      setTimeout(() => {
        this.valueInputRef && this.valueInputRef.focus();
      }, 100);
    }
  }

  onHandleInputRef = ref => {
    this.valueInputRef = ref;
  };

  onCancel = () => {
    this.props.onClose();
  };

  onSubmit = () => {
    this.props.onClose();
    if (!this.state.value) return;

    let value = parseInt(Util.Text.textWithoutCommas(this.state.value)) || 0;
    this.props.onSubmit(value);
  };

  onChangeValueText = text => {
    if (text === '') {
      this.setState({ value: '' });
      return;
    }

    const { properValue } = this.props;
    let value = properNumberString(Util.Text.textWithoutCommas(text.trim()));
    value = properValue ? properValue(value) : value;
    value = value || '';
    this.setState({ value });
  };

  render() {
    const { visible, placeholder, unitText, iconName, iconType, submitText, cancelText, inputStyle } = this.props;

    return (
      <KeyboardModal visible={visible} onClose={this.props.onClose}>
        <View>
          <ModalHeader
            title={this.props.title}
            leftButton={{ text: cancelText, onPress: this.onCancel }}
            rightButton={{ text: submitText, onPress: this.onSubmit }}
          />
          <View style={styles.content}>
            <Icon name={iconName} type={iconType} size={scale(20)} color={colors.gray} />
            <TextInput
              keyboardType={'numeric'}
              placeholder={placeholder}
              ref={this.onHandleInputRef}
              onBlur={this.onBlurValueInput}
              onFocus={this.onFocusValueInput}
              value={this.state.value === '' ? '' : Util.Text.numberWithCommas(this.state.value)}
              selectionColor={colors.primary}
              underlineColorAndroid={'transparent'}
              style={[styles.input, inputStyle]}
              onChangeText={this.onChangeValueText}
              onSubmitEditing={this.onSubmit}
            />
            <Text style={textStyles.body2}>{unitText}</Text>
          </View>
        </View>
      </KeyboardModal>
    );
  }
}

ChangeValueModal.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  unitText: PropTypes.string,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  visible: PropTypes.bool,
  submitText: PropTypes.string,
  cancelText: PropTypes.string,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func.isRequired,
  properValue: PropTypes.func,
};

ChangeValueModal.defaultProps = {
  title: 'Giảm giá',
  placeholder: 'Nhập số tiền',
  unitText: ' đ',
  iconName: 'attach-money',
  iconType: 'material',
  visible: false,
  submitText: 'Xong',
  cancelText: 'Hủy',
};

export default ChangeValueModal;

const styles = StyleSheet.create({
  content: {
    height: scale(52),
    paddingHorizontal: scale(16),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  input: {
    flex: 1,
    height: '100%',
    color: colors.black,
    fontSize: scale(14),
    fontFamily: 'sale-text-regular',
    paddingLeft: screen.padding.default,
    letterSpacing: getLetterSpacing(-0.15),
  },
});
