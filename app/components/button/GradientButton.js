import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { LinearGradient } from 'expo';

import { scale, getLetterSpacing } from '../../utils/scaling';
import { screen, gradientColors } from '../../resources/styles/common';

export default class GradientButton extends PureComponent {
  state = {
    disabled: false,
  };
  enableButtonTimeout = null;
  disabled = false;

  componentWillUnmount = () => {
    this.enableButtonTimeout && clearTimeout(this.enableButtonTimeout);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.disabled && !prevState.disabled && this.props.disabledDuration) {
      this.enableButtonTimeout && clearTimeout(this.enableButtonTimeout);
      this.enableButtonTimeout = setTimeout(() => {
        this.disabled = false;
        this.setState({ disabled: false });
      }, this.props.disabledDuration);
    }
  };

  onPress = () => {
    if (this.props.disabledDuration) {
      if (!this.disabled) {
        this.disabled = true;
        this.props.onPress && this.props.onPress();
        this.setState({ disabled: true });
      }
    } else {
      this.props.onPress && this.props.onPress();
    }
  };

  render() {
    const { onPress, title, colors, iconName, iconType, image, iconSize, iconColor, containerStyle, textStyle } = this.props;

    return (
      <LinearGradient start={[0, 1]} end={[1, 1]} colors={colors} style={[styles.container, containerStyle]}>
        <TouchableOpacity onPress={this.onPress} style={styles.content} disabled={this.state.disabled}>
          {image ? <Image style={{ height: scale(24), width: scale(24) }} source={image} /> : null}
          {iconName ? <Icon type={iconType} name={iconName} color={iconColor} size={iconSize} style={styles.icon} /> : null}
          <Text style={[styles.title, textStyle]}>{title}</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

GradientButton.propTypes = {
  title: PropTypes.string,
  colors: PropTypes.array,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.number,
  containerStyle: View.propTypes.style,
  onPress: PropTypes.func,
  textStyle: Text.propTypes.style,
};

GradientButton.defaultProps = {
  disabledDuration: 500,
  colors: gradientColors.primary,
  iconType: 'material-community',
  iconSize: screen.iconSize.default,
  color: '#4285f4',
  containerStyle: {},
};

const styles = StyleSheet.create({
  container: {
    height: scale(50),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(8),
  },
  content: {
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(8),
  },
  icon: {
    marginLeft: screen.margin.default,
    marginRight: screen.margin.default,
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: scale(15),
    lineHeight: scale(20),
    letterSpacing: getLetterSpacing(-0.4),
    fontFamily: 'sale-text-semibold',
    marginLeft: screen.margin.default,
    marginRight: screen.margin.default,
  },
});
