import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { scale, getLetterSpacing } from '../../utils/scaling';
import { colors } from '../../resources/styles/common';

export class LayoutButton extends PureComponent {
  render() {
    const { onPress, title, buttonStyle, titleStyle, disabled } = this.props;

    return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <View style={[styles.content, disabled ? styles.disabled : null, buttonStyle]}>
          <Icon name="plus" type="material-community" color={disabled ? colors.gray : colors.secondary} size={scale(15)} />
          <Text style={[styles.title, disabled ? styles.disabledTitle : null, titleStyle]}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default LayoutButton;

LayoutButton.propTypes = {
  title: PropTypes.string,
  buttonStyle: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
};

LayoutButton.defaultProps = {
  disabled: false,
};

const styles = StyleSheet.create({
  content: {
    height: scale(24),
    paddingRight: scale(8),
    paddingLeft: scale(6),
    borderRadius: scale(8),
    borderWidth: 1,
    borderColor: colors.secondary,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  disabled: {
    backgroundColor: colors.lightGray,
    borderColor: colors.lightGray,
  },
  title: {
    color: colors.secondary,
    fontSize: scale(13),
    letterSpacing: getLetterSpacing(-0.2),
    fontFamily: 'sale-text-regular',
    marginLeft: scale(4),
  },
  disabledTitle: {
    color: colors.gray,
  },
});
