import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

import { DangerZone } from 'expo';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import Button from '../button/Button';
import RootNavigationService from '../../services/navigation/RootNavigationService';

const { Lottie } = DangerZone;

const ANIMATIONS = {
  loading: { loop: true, source: require('../../resources/animations/loading/loading-small-red.json') },
  success: { loop: false, source: require('../../resources/animations/success.json') },
  error: { loop: false, source: require('../../resources/animations/error.json') },
};
export default class OrderResultComponent extends Component {
  state = {
    createOrderStatus: null,
    paymentStatus: null,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    let { createOrderStatus, paymentStatus, transaction } = nextProps;
    let derivedState = { createOrderStatus, paymentStatus, transaction };
    return derivedState;
  }

  componentDidMount() {
    this.animationRef && this.animationRef.play();
  }

  handleAnimationRef = ref => {
    this.animationRef = ref;
  };

  componentDidUpdate() {
    this.animationRef && this.animationRef.play();
  }

  onGotoHome = () => {
    RootNavigationService.reset('Main');
  };

  onRetry = () => {
    this.props.onRetry && this.props.onRetry();
  };

  render() {
    let { createOrderStatus, paymentStatus, transaction } = this.state;
    let status = null;
    let alert;
    let message;
    if (paymentStatus) {
      alert = paymentStatus === '00' ? 'Thanh toán thành công' : 'Thanh toán thất bại';
      status = paymentStatus === '00' ? 'success' : 'error';
      message =
        paymentStatus === '00'
          ? 'Đơn hàng của bạn đã được thanh toán'
          : 'Cõ lỗi xảy ra trong quá trình thanh toán\nXin vui lòng thử lại';
    } else {
      alert =
        createOrderStatus === 'success' ? 'Đặt hàng thành công' : createOrderStatus === 'error' ? 'Đặt hàng thất bại' : null;
      status = createOrderStatus === 'success' ? 'success' : createOrderStatus === 'error' ? 'error' : null;
      message =
        createOrderStatus === 'success'
          ? 'Đơn hàng của bạn đã được đặt thành công'
          : 'Có lỗi xảy ra khi đặt đơn hàng\nXin vui lòng thử lại';
    }
    let source = status && ANIMATIONS[status].source;
    if (!status) {
      return <View style={{ flex: 1 }} />;
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <View>
            <Lottie
              ref={this.handleAnimationRef}
              autoPlay={false}
              resizeMode="cover"
              loop={false}
              style={{ width: scale(80), height: scale(80) }}
              source={source}
              autoSize={true}
            />
          </View>
          <Text style={[textStyles.display, { marginTop: scale(20) }]}>{alert}</Text>
          <Text style={[textStyles.body1, { color: colors.gray, marginTop: scale(10), textAlign: 'center' }]}>{message}</Text>
          {status === 'success' ? (
            <Text style={[textStyles.body1, { color: colors.gray, textAlign: 'center' }]}>
              Mã đơn hàng <Text style={{ color: colors.primary, textAlign: 'center' }}>{transaction.code}</Text>
            </Text>
          ) : null}
        </View>
        {status === 'success' ? (
          <Button
            onPress={this.onGotoHome}
            color={colors.primary}
            title={'VỀ TRANG CHỦ'}
            containerStyle={{ width: screen.width, height: scale(50), borderRadius: 0 }}
          />
        ) : null}
        {status === 'error' ? (
          <Button
            onPress={this.onRetry}
            color={colors.primary}
            title={'THỬ LẠI'}
            containerStyle={{ width: screen.width, height: scale(50), borderRadius: 0 }}
          />
        ) : null}
      </View>
    );
  }
}
