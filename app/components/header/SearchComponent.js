import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Text, View, TextInput, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { Icon } from 'react-native-elements';
import { scale } from '../../utils/scaling';

import header_styles from '../../resources/styles/header';
import { colors, screen } from '../../resources/styles/common';
import { LinearGradient } from 'expo';

export class SearchComponent extends Component {
  state = { searchText: '' };

  constructor(props) {
    super(props);

    this.searchInputRef = React.createRef();
  }

  componentDidMount() {
    const { shouldFocusSearch } = this.props;

    if (shouldFocusSearch) {
      setTimeout(() => {
        this.searchInputRef && this.searchInputRef.focus();
      }, 200);
    }
  }

  handleSearchInputRef = ref => {
    this.searchInputRef = ref;
  };

  onChangeText = text => {
    this.setState({ searchText: text });
    this.props.onChangeText && this.props.onChangeText(text);
  };

  onSubmitEditing = () => {
    let searchText = this.state.searchText.trim();
    this.setState({ searchText });
    this.props.onSubmitEditing && this.props.onSubmitEditing();
  };

  onEndEditing = searchText => {};

  onClearSearchText = () => {
    if (Platform.OS === 'ios') {
      this.onChangeText('');
    } else {
      this.setState({ searchText: this.state.searchText + ' ' }, () => {
        setTimeout(() => {
          this.onChangeText('');
        }, 1);
      });
    }
  };

  renderLeftSection() {
    return this.props.leftSection || <View style={{ marginLeft: screen.margin.smaller }} />;
  }

  renderRightSection() {
    return this.props.rightSection || <View style={{ marginLeft: screen.margin.smaller }} />;
  }

  renderSearchHeader() {
    let { placeholder, onSubmitEditing, onEndEditing, inputContainerStyle, iconColor, placeholderTextColor } = this.props;
    return (
      <View style={[header_styles.searchInputContainer, inputContainerStyle]}>
        <Icon name="search" type="material-icon" color={iconColor} />
        <TextInput
          ref={this.handleSearchInputRef}
          value={this.state.searchText}
          style={styles.searchInput}
          underlineColorAndroid="transparent"
          selectionColor={colors.primary}
          placeholder={placeholder}
          onChangeText={this.onChangeText}
          onSubmitEditing={this.onSubmitEditing}
          onEndEditing={onEndEditing}
          placeholderTextColor={placeholderTextColor}
        />
        {this.state.searchText ? (
          <TouchableOpacity style={styles.clearText} onPress={this.onClearSearchText}>
            <Icon name="close-circle" type="material-community" color={colors.darkGray} size={scale(16)} />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }

  render() {
    const { colors, containerStyle } = this.props;

    return (
      <View style={header_styles.container}>
        <LinearGradient start={[0, 1]} end={[1, 1]} colors={colors} style={[styles.header, containerStyle]}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            {this.renderLeftSection()}
            {this.renderSearchHeader()}
            {this.renderRightSection()}
          </View>
        </LinearGradient>
      </View>
    );
  }
}

SearchComponent.propTypes = {
  iconColor: PropTypes.string,
  colors: PropTypes.array,
};

SearchComponent.defaultProps = {
  colors: [colors.tomato, colors.primary],
  iconColor: colors.primary,
};

export default SearchComponent;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: screen.searchBar.height,
    width: '100%',
    paddingBottom: screen.padding.tiny,
    alignItems: 'center',
  },
  searchInput: {
    flex: 5,
    alignItems: 'center',
    paddingHorizontal: screen.padding.default,
    fontSize: screen.fontSize.medium,
    color: colors.black,
  },
  clearText: {
    height: scale(32),
    width: scale(16),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
