import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo';

import header_styles from '../../resources/styles/header';
import { colors, screen } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import CustomStatusBar from './CustomStatusBar';
import Slide from '../animation/Slide';

export default class CustomHeader extends PureComponent {
  renderLeftSection = () => {
    const { leftSection, showAlterLeftSection } = this.props;

    return leftSection ? (
      <Slide delta={-scale(45)} show={!showAlterLeftSection} style={styles.leftSection}>
        {leftSection}
      </Slide>
    ) : null;
  };

  renderAlterLeftSection = () => {
    const { alterLeftSection, showAlterLeftSection } = this.props;

    return alterLeftSection ? (
      <Slide delta={scale(45)} show={showAlterLeftSection} style={styles.leftSection}>
        {alterLeftSection}
      </Slide>
    ) : null;
  };

  renderCenterSection = () => {
    const { title, centerSection } = this.props;

    if (centerSection) {
      return <View style={styles.centerSection}>{centerSection}</View>;
    }
    if (title) {
      return <View style={styles.centerSection}>{this.renderTitle()}</View>;
    }

    return null;
  };

  renderTitle() {
    if (this.props.title) {
      return (
        <View>
          <Text accessibilityLabel="header_title" style={[header_styles.titleText, this.props.titleTextStyle]}>
            {this.props.title}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  renderRightSection = () => {
    const { rightSection, showAlterRightSection } = this.props;

    return rightSection ? (
      <Slide delta={-scale(45)} show={!showAlterRightSection} style={styles.rightSection}>
        {rightSection}
      </Slide>
    ) : null;
  };

  renderAlterRightSection = () => {
    const { showAlterRightSection, alterRightSection } = this.props;

    return alterRightSection ? (
      <Slide delta={scale(45)} show={showAlterRightSection} style={styles.rightSection}>
        {alterRightSection}
      </Slide>
    ) : null;
  };

  render() {
    const { statusBar, colors } = this.props;

    return (
      <View>
        <CustomStatusBar {...statusBar} colors={colors} />
        <View style={header_styles.container}>
          <LinearGradient start={[0, 1]} end={[1, 1]} colors={colors} style={[styles.header]}>
            <View style={{ flex: 1, backgroundColor: 'transparent', overflow: 'hidden' }}>
              {this.renderCenterSection()}
              {this.renderAlterLeftSection()}
              {this.renderLeftSection()}
              {this.renderAlterRightSection()}
              {this.renderRightSection()}
            </View>
          </LinearGradient>
        </View>
      </View>
    );
  }
}

CustomHeader.propTypes = {
  title: PropTypes.string,
  titleTextStyle: PropTypes.object,
  statusBar: PropTypes.object,
  colors: PropTypes.arrayOf(PropTypes.string),
  leftSection: PropTypes.element,
  centerSection: PropTypes.element,
  rightSection: PropTypes.element,
};

CustomHeader.defaultProps = {
  title: '',
  titleTextStyle: {},
  colors: [colors.tomato, colors.primary],
  statusBar: {},
};

const styles = StyleSheet.create({
  header: {
    overflow: 'hidden',
    height: screen.searchBar.height,
    width: '100%',
  },
  leftSection: {
    backgroundColor: 'transparent',
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
    left: 0,
    paddingLeft: scale(6),
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
  },
  centerSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightSection: {
    backgroundColor: 'transparent',
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
    right: 0,
    paddingRight: scale(6),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
});
