import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { scale, getLetterSpacing } from '../utils/scaling';
import { screen, colors, textStyles } from '../resources/styles/common';

/**
 * @augments {Component<{  quantity:number,  minQuantity:number,  maxQuantity:number,  onUpdateQuantity:Function,  onPressItemQuantity:Function,  handleQuantityInputRef:Function>}
 */
class ChangeQuantityItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.quantity,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let quantity = nextProps.quantity;
    return { quantity };
  }

  updateQuantity = quantity => {
    this.props.onUpdateQuantity(quantity);
  };

  onChangeQuantityText = text => {
    let quantity = parseInt(text) || this.props.minQuantity;
    this.props.onUpdateQuantity(quantity);
  };

  decreaseQuantity = () => {
    if (this.state.quantity > this.props.minQuantity) {
      this.updateQuantity(this.state.quantity - 1);
    }
  };

  increaseQuantity = () => {
    if (this.state.quantity < this.props.maxQuantity) {
      this.updateQuantity(this.state.quantity + 1);
    }
  };

  renderQuantity = () => {
    const { onPressItemQuantity, handleQuantityInputRef, editable } = this.props;
    if (onPressItemQuantity) {
      return (
        <TouchableOpacity onPress={onPressItemQuantity} disabled={!editable}>
          <View style={styles.quantity}>
            <Text style={editable ? styles.quantityInput : styles.disabledQuantityInput}>{this.state.quantity}</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.quantity}>
          <TextInput
            ref={handleQuantityInputRef}
            keyboardType="numeric"
            value={String(this.state.quantity)}
            underlineColorAndroid={'transparent'}
            style={editable ? styles.quantityInput : styles.disabledQuantityInput}
            onChangeText={this.onChangeQuantityText}
            editable={editable}
            onEndEditing={() => {
              this.updateQuantity(this.state.quantity);
            }}
          />
        </View>
      );
    }
  };

  render() {
    const { quantity } = this.state;
    const { minQuantity, maxQuantity } = this.props;

    let canIncrease = quantity < maxQuantity;
    let canDecrease = quantity > minQuantity;

    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={canDecrease ? 1 : 0.2} onPress={this.decreaseQuantity} disabled={!canDecrease}>
          <View style={styles.changeQuantityButton}>
            <Icon type="material-community" name="minus" color={canDecrease ? colors.black : colors.gray} size={scale(20)} />
          </View>
        </TouchableOpacity>
        {this.renderQuantity()}
        <TouchableOpacity activeOpacity={canIncrease ? 1 : 0.2} onPress={this.increaseQuantity} disabled={!canIncrease}>
          <View style={styles.changeQuantityButton}>
            <Icon type="material-community" name="plus" color={canIncrease ? colors.black : colors.gray} size={scale(20)} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

ChangeQuantityItem.propTypes = {
  editable: PropTypes.bool, // edit by enter number
  quantity: PropTypes.number,
  minQuantity: PropTypes.number,
  maxQuantity: PropTypes.number,
  onUpdateQuantity: PropTypes.func,
  onPressItemQuantity: PropTypes.func,
  handleQuantityInputRef: PropTypes.func,
};

ChangeQuantityItem.defaultProps = {
  editable: true,
  quantity: 1,
  minQuantity: 1,
  maxQuantity: Number.MAX_SAFE_INTEGER,
};

export default ChangeQuantityItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: scale(34),
    borderWidth: 1,
    borderRadius: scale(6),
    borderColor: colors.lightGray,
    backgroundColor: 'white',
  },
  quantity: {
    height: '100%',
    width: scale(40),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.lightGray,
  },
  quantityInput: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(15),
    letterSpacing: getLetterSpacing(-0.2),
    lineHeight: scale(20),
    color: colors.black,
    textAlign: 'center',
  },
  disabledQuantityInput: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(15),
    letterSpacing: getLetterSpacing(-0.2),
    lineHeight: scale(20),
    color: colors.gray,
    textAlign: 'center',
  },
  changeQuantityButton: {
    flex: 1,
    width: scale(32),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
