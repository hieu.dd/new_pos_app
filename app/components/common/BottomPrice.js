import React from 'react';
import { View, Text } from 'react-native';
import TrapezoidButton from '../button/TrapezoidButton';
import Price from '../common/Price';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';

export const BottomPrice = ({ price, onPress, disabled, title }) => {
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: screen.width,
          height: scale(48),
        },
      ]}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: colors.dark_slate_blue,
          paddingHorizontal: scale(12),
          height: scale(48),
          justifyContent: 'center',
          paddingVertical: scale(12),
        }}
      >
        <Text style={[textStyles.footnote, { color: 'white' }]}>Thành tiền: </Text>
        <Price color={colors.squash} style={[textStyles.price]} price={price} />
      </View>
      <View style={{ position: 'absolute', top: 0, right: 0 }}>
        <TrapezoidButton
          disabled={disabled}
          color={colors.primary}
          title={title}
          textStyle={[textStyles.body1, { color: 'white', fontFamily: 'sale-text-bold' }]}
          width={scale(140)}
          height={scale(48)}
          left={scale(12)}
          iconSize={scale(16)}
          iconRightName="chevron-right"
          iconType="material-community"
          iconColor="white"
          onPress={onPress}
        />
      </View>
    </View>
  );
};
