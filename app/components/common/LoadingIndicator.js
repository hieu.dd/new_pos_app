import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { DangerZone } from 'expo';

import { scale } from '../../utils/scaling';

const { Lottie } = DangerZone;

const ANIMATION_SOURCES = {
  red: require('../../resources/animations/loading/loading-small-red.json'),
  white: require('../../resources/animations/loading/loading-small-white.json'),
};

export class LoadingIndicator extends PureComponent {
  componentDidMount = () => {
    this.indicatorRef && this.indicatorRef.play();
  };

  handleIndicatorRef = ref => {
    this.indicatorRef = ref;
  };

  getLoadingAnimationSource = color => {
    return ANIMATION_SOURCES[color] || ANIMATION_SOURCES.red;
  };

  getStyle = size => {
    return styles[size];
  };

  render() {
    const { size, color } = this.props;
    const loadingAnimationSource = this.getLoadingAnimationSource(color);
    const style = this.getStyle(size);

    return (
      <View style={style}>
        <Lottie resizeMode="cover" ref={this.handleIndicatorRef} style={style} source={loadingAnimationSource} />
      </View>
    );
  }
}

export default LoadingIndicator;

LoadingIndicator.propTypes = {
  size: PropTypes.oneOf(['small', 'large']),
  color: PropTypes.oneOf(['red', 'white']),
};

LoadingIndicator.defaultProps = {
  size: 'small',
  color: 'red',
};

const styles = StyleSheet.create({
  small: {
    height: scale(19),
    width: scale(19),
  },
  large: {
    height: scale(36),
    width: scale(36),
  },
});
