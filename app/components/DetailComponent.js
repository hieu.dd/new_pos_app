import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, Image, Animated, Alert as AlertView } from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements';
import { StackActions } from 'react-navigation';
import { Util } from 'teko-js-sale-library';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../config/TrackingPrototype';
import * as promotions from '../modules/promotion';
import { showLoadingModal } from '../stores/appState/actions';
import { fetchProductDetails } from '../stores/product/actions';
import { addProductToCart, changeProduct } from '../stores/cart/actions';
import { fetchProductMagentoDetail } from '../stores/product/actions';
import * as strings from '../resources/strings';
import commonStyles, { screen, colors, textStyles } from '../resources/styles/common';
import { scale, getLetterSpacing } from '../utils/scaling';
import * as productUtils from '../utils/product';
import * as cartUtils from '../utils/cart';
import DescriptionProduct from './product/DescriptionProduct';
import LoadingIndicator from '../components/common/LoadingIndicator';
import AttributesProduct from './product/AttributesProduct';
import ProductPromotions from './product/ProductPromotions';
import GradientButton from './button/GradientButton';
import GiftItemInDetail from './GiftItemInDetail';
import { BottomPrice } from '../components/common/BottomPrice';
export class DetailComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: 0,
      productQuantity: 1,
      gifts: [],
      promotion: null,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let product = nextProps.product_details[nextProps.sku] && nextProps.product_details[nextProps.sku].detail;

    if (prevState.gifts.length === 0) {
      let gifts = promotions.getAsiaGifts(product, null, 1, promotions.getNow());
      if (gifts.length > 0) return { product, gifts };
    }
    return { product };
  }

  componentDidMount() {
    let product = this.props.product_details[this.props.sku] && this.props.product_details[this.props.sku].detail;
    if (!product) this.props.fetchProductDetails(this.props.sku, this.props.item);
    if (!product || !product.image || !product.attributes) {
      this.props.fetchProductMagentoDetail(this.props.sku);
    }
  }

  componentWillUnmount() {
    this.hideAlertTimeout && clearTimeout(this.hideAlertTimeout);
  }

  componentDidUpdate(prevProps, prevState) {
    //fetch gift items
    let { product_details, sku, fetchProductDetails, fetchProductMagentoDetail } = prevProps;
    let product = product_details[sku] && product_details[sku].detail;
    let gifts = [];
    if ((!prevState.product || prevState.product.isLoading) && product) {
      gifts = promotions.getAsiaGifts(product, null, 1, promotions.getNow());
      // for (let gift of gifts) {
      //   let giftDetail = product_details[gift.sku] && product_details[gift.sku].detail;
      //   if (!giftDetail) fetchProductDetails(gift.sku);
      //   if (!giftDetail || !giftDetail.image) {
      //     fetchProductMagentoDetail(gift.sku);
      //   }
      // }
    }
  }

  addProductToCart = () => {
    let productDetail = this.props.product_details[this.props.sku] && this.props.product_details[this.props.sku].detail;
    let promotion = this.productPromotionsRef ? this.productPromotionsRef.getSelectedPromotion() : promotions.getNoPromotion();

    if (this.props.cart.voucher && !promotions.isEmptyPromotion(promotion)) {
      this.props.showLoadingModal({ message: strings.add_product_with_voucher_warning });
      return;
    }
    if (productDetail && !productDetail.price_w_vat) {
      AlertView.alert('Thông báo!', 'Không thể thêm sản phẩm giá 0đ');
      return;
    }
    let product = {
      sku: productDetail.sku,
      name: productDetail.name,
      category: productDetail.category,
      image: productUtils.getImageSource(productDetail),
      price: parseInt(productDetail.price_w_vat),
      originalPrice: parseInt(productDetail.original_price),
      asiaPromotion: promotions.getAsiaPromotion(productDetail, null, 1, promotions.getNow()),
      promotion: promotion,
      quantity: this.state.productQuantity,
    };
    if (this.props.addProduct) {
      this.props.navigation.navigate('Combo');
      this.props.addProduct(product);
    } else {
      Tracking.trackEvent(TRACK.EVENT.ADD_PRODUCT, productDetail.sku, this.state.productQuantity);
      this.props.changeProduct(product, this.onAddProductResult);
    }
  };

  onAddProductResult = (success, message) => {
    this.reload();
    this.props.navigation.navigate('OrderDetail');
  };

  reload = () => {
    this.productPromotionsRef && this.productPromotionsRef.reload();
  };

  renderBasicInformation = product => {
    let image_source = product.image
      ? { uri: product.image && product.image.base_image }
      : require('../resources/images/no_product.png');
    return (
      <View
        style={{
          marginHorizontal: screen.padding.default,
          marginTop: scale(8),
          borderBottomColor: colors.lightGray,
          borderBottomWidth: 1,
          backgroundColor: 'white',
        }}
      >
        <View style={{ padding: scale(16) }}>
          <Image style={{ width: scale(192), height: scale(192), marginHorizontal: scale(60) }} source={image_source} />

          <Text style={[textStyles.body1]}>{product.seo_name || product.name}</Text>
          <Text style={[textStyles.footnote, { marginVertical: scale(6) }]}>
            Mã SP: <Text style={{ color: colors.secondary }}>{product.sku}</Text>
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={[textStyles.price, { color: colors.primary }]}>{Util.Text.formatPrice(product.price)}</Text>
          </View>
        </View>
        {this.renderDescription(product)}
        {this.renderAttributes(product)}
      </View>
    );
  };

  renderBottomContainer = () => {
    let { product, productQuantity, promotion } = this.state;
    let price = productUtils.getProductPrice(product);
    let totalPrice = (price - cartUtils.getTotalPromotionDiscountItem({ price, promotion })) * productQuantity;

    return <BottomPrice price={totalPrice} disabled={false} title={'Mua ngay'} onPress={this.addProductToCart} />;
  };

  renderGiftsContainer = () => {
    return (
      <View style={[styles.container, { flexDirection: 'row' }]}>
        <View style={styles.giftContainer}>
          {this.state.gifts.map(item => {
            return (
              <GiftItemInDetail
                key={item.sku}
                gift={item}
                product_details={this.props.product_details}
                fetchProductDetails={this.props.fetchProductDetails}
                fetchProductMagentoDetail={this.props.fetchProductMagentoDetail}
              />
            );
          })}
        </View>

        <View style={{ flexDirection: 'row', position: 'absolute', left: scale(24) }}>
          <LinearGradient
            colors={['rgb(223,32,32)', 'rgb(245,71,30)', 'rgb(245,71,30)']}
            style={{
              paddingVertical: scale(4),
              borderRadius: scale(8),
              paddingHorizontal: scale(8),
              height: scale(24),
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <Icon name="gift" type="material-community" color={'white'} size={scale(16)} />
            <Text style={[textStyles.subheading, { color: 'white', marginLeft: scale(4) }]}>Sản phẩm tặng kèm</Text>
          </LinearGradient>
        </View>
      </View>
    );
  };

  handleProductPromotionsRef = connectedRef => {
    this.productPromotionsRef = (connectedRef && connectedRef.getWrappedInstance()) || null;
  };

  renderProductPromotions = () => {
    if (this.props.hidePromotion) {
      return null;
    } else {
      return (
        <View>
          <ProductPromotions
            ref={this.handleProductPromotionsRef}
            product={this.state.product}
            changeProductPromotion={this.changeProductPromotion}
          />
        </View>
      );
    }
  };

  changeProductPromotion = promotion => {
    this.setState({ promotion });
  };

  renderDescription = product => {
    return <DescriptionProduct description={product && product.description} />;
  };

  renderAttributes = product => {
    return <AttributesProduct product={product} />;
  };

  renderContent(product) {
    if (
      !this.props.product_details[this.props.sku] ||
      (this.props.product_details[this.props.sku] && this.props.product_details[this.props.sku].isLoading)
    ) {
      return (
        <View style={styles.emptyContainer}>
          <LoadingIndicator size="large" />
        </View>
      );
    }

    if (!product && this.props.product_details[this.props.sku].error) {
      return <View style={styles.emptyContainer} />;
    }
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ backgroundColor: colors.white_two, flex: 1 }}>
          {this.renderBasicInformation(product)}
          {this.state.gifts.length > 0 ? this.renderGiftsContainer(product) : null}
          {this.renderProductPromotions()}
          <View style={{ width: screen.width, height: scale(70) }} />
        </ScrollView>
        {this.renderBottomContainer()}
      </View>
    );
  }

  render() {
    let product =
      this.props.product_details[this.props.sku] &&
      this.props.product_details[this.props.sku].detail &&
      this.props.product_details[this.props.sku].detail.sku
        ? this.props.product_details[this.props.sku].detail
        : null;
    if (product) product.seo_name = this.props.seo_name;
    // product = { ...product, seo_name: this.props.seo_name || null };
    return <View style={{ flex: 1, backgroundColor: 'white' }}>{this.renderContent(product)}</View>;
  }
}

function mapStateToProps(state, props) {
  return {
    items: state.cart.items,
    product_details: state.product.product_details,
    addProduct: state.screenState.ProductDetail.addProduct,
    hidePromotion: state.screenState.ProductDetail.hidePromotion,
    isAddingProductToCombo: state.screenState.ProductDetail.isAddingProductToCombo,
    cart: cartUtils.getCurrentCart(state.cart),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showLoadingModal: ({ message, actions }) => dispatch(showLoadingModal({ message, actions })),
    fetchProductDetails: (sku, item) => dispatch(fetchProductDetails(sku, item)),
    fetchProductMagentoDetail: sku => dispatch(fetchProductMagentoDetail(sku)),
    changeProduct: (product, onAddProductResult) => dispatch(changeProduct(product, onAddProductResult)),
    addProductToCart: (product, onAddProductResult) => dispatch(addProductToCart(product, onAddProductResult)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailComponent);

const styles = StyleSheet.create({
  emptyContainer: {
    flex: 1,
    paddingTop: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    padding: screen.padding.smaller,
  },
  giftContainer: {
    width: '100%',
    borderColor: 'rgb(245,71,30)',
    borderRadius: scale(10),
    borderWidth: 2,
    padding: scale(17),
  },
});
