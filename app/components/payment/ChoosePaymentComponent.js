import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import * as strings from '../../resources/strings';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { TextInput } from 'react-native-gesture-handler';
import Space from '../../components/Space';
import Button from '../../components/button/Button';
import * as customerUtils from '../../utils/customer';
import { connect } from 'react-redux';
import * as saleActions from '../../stores/sale/actions';
import * as cartActions from '../../stores/cart/actions';
import TitleLabel from '../label/TitleLabel';
import QRCode from 'react-native-qrcode';
import Price from '../common/Price';

export class ChoosePaymentComponent extends Component {
  state = {
    paymentType: 1,
    paymentUrl: '',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    let { currentTransaction } = nextProps;
    if (currentTransaction && currentTransaction.paymentUrl) {
      return {
        paymentUrl: currentTransaction.paymentUrl,
      };
    } else {
      return {
        paymentUrl: null,
      };
    }
  }

  onPress = () => {
    let { currentTransaction } = this.props;
    let { paymentType } = this.state;
    if (paymentType === 1) {
      this.props.createOrder();
      if (currentTransaction.id) {
        this.createOrderSuccess();
      } else {
        this.props.creatOrderOM({
          payment: 'COD',
          onSuccess: this.createOrderSuccess,
          onFailure: this.props.onFailure,
        });
      }
    } else if (paymentType === 2) {
      if (currentTransaction.id) {
        this.props.onQRPayment(currentTransaction.id, currentTransaction.invoice.price);
      } else {
        this.props.creatOrderOM({
          payment: 'VNPAYQR',
          onSuccess: this.createOrderSuccess,
          onFailure: this.props.onFailure,
        });
      }
    }
  };

  createOrderSuccess = () => {
    let { paymentType } = this.state;
    if (paymentType === 1) {
      this.props.onSuccess();
    }
  };

  onVNPAYResult = () => {};

  changeToCOD = () => {
    this.setState({ paymentType: 1 });
  };

  changeToVNPAY = () => {
    this.setState({ paymentType: 2 });
  };

  onCancel = () => {
    this.props.onCancelQRPay();
  };

  renderCODPayment = () => {
    return (
      <TouchableOpacity style={styles.payContainer} onPress={this.changeToCOD}>
        <Image resizeMode="contain" source={require('../../resources/images/delivery_time.png')} style={styles.payImage} />
        <Text style={[textStyles.body1, { textAlign: 'center', marginTop: scale(21) }]}>
          Thanh toán khi nhận hàng - <Text style={{ fontFamily: 'sale-text-medium' }}>COD</Text>
        </Text>
        {this.state.paymentType === 1 ? (
          <View style={{ position: 'absolute', top: scale(10), right: scale(10) }}>
            <Icon name="check-circle" type={'material-community'} color={colors.secondary} size={scale(16)} />
          </View>
        ) : null}
      </TouchableOpacity>
    );
  };

  renderVNPAYQRPayment = () => {
    return (
      <TouchableOpacity style={styles.payContainer} onPress={this.changeToVNPAY}>
        <Image resizeMode="contain" source={require('../../resources/images/vnpay.png')} style={styles.payImage} />
        <Text style={[textStyles.body1, { textAlign: 'center', marginTop: scale(12) }]}>Ứng dụng Mobie Banking quét mã</Text>
        <Text style={[textStyles.heading1, { color: colors.pinkish_red }]}>
          VN<Text style={{ color: colors.peacock_blue }}>PAY-</Text>QR
        </Text>
        {this.state.paymentType === 2 ? (
          <View style={{ position: 'absolute', top: scale(10), right: scale(10) }}>
            <Icon name="check-circle" type={'material-community'} color={colors.secondary} size={scale(16)} />
          </View>
        ) : null}
      </TouchableOpacity>
    );
  };

  renderPaymentMethods = () => {
    return (
      <View style={{ flex: 1 }}>
        <View>
          <TitleLabel text={'Chọn phương thức thanh toán'} />
          <View style={{ flexDirection: 'row', width: screen.width }}>
            {this.renderCODPayment()}
            {this.renderVNPAYQRPayment()}
          </View>
        </View>
        <Button
          onPress={this.onPress}
          color={colors.primary}
          title={'THANH TOÁN'}
          containerStyle={{
            position: 'absolute',
            height: scale(50),
            bottom: 0,
            borderRadius: 0,
            width: screen.width,
          }}
        />
      </View>
    );
  };

  renderQRCode = () => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', width: scale(190), height: scale(210) }}>
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Image
                resizeMode={'stretch'}
                style={{ width: scale(190), height: scale(210) }}
                source={require('../../resources/images/qr_frame.png')}
              />
            </View>

            <QRCode value={this.state.paymentUrl} size={scale(150)} bgColor="black" fgColor="white" />
          </View>
          <Text style={[textStyles.body1, { marginTop: scale(24), marginBottom: scale(8) }]}>Số tiền thanh toán</Text>
          <Price style={[textStyles.medium_bold]} price={this.props.finalInvoice} />
        </View>
        <Button
          onPress={this.onCancel}
          color={colors.primary}
          title={'HỦY'}
          containerStyle={{
            height: scale(50),
            borderRadius: 0,
            width: screen.width,
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.lightGray }}>
        {!this.state.paymentUrl ? this.renderPaymentMethods() : this.renderQRCode()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentTransaction: state.sale.currentTransaction,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    creatOrderOM: ({ payment, onSuccess, onFailure }) => dispatch(saleActions.creatOrderOM({ payment, onSuccess, onFailure })),
    onCancelQRPay: () => dispatch(saleActions.onCancelQRPay()),
    onQRPayment: (id, amount) => dispatch(saleActions.onQRPayment(id, amount)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChoosePaymentComponent);

const styles = StyleSheet.create({
  payContainer: {
    width: scale(165),
    backgroundColor: 'white',
    height: scale(165),
    marginLeft: scale(10),
    alignItems: 'center',
    paddingTop: scale(24),
  },
  payImage: { width: scale(60), height: scale(60) },
});
