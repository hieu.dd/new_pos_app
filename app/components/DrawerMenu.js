import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Image, ScrollView } from 'react-native';
import { connect } from 'react-redux';

export class DrawerMenu extends Component {
  goTo2048 = () => {
    this.props.navigation.navigate('Game2048');
  };

  gotoReport = () => {
    this.props.navigation.navigate('ReportNoShop');
  };

  gotoFlap = () => {
    this.props.navigation.navigate('FlapBird');
  };

  render() {
    return (
      <ScrollView>
        <TouchableOpacity accessibilityLabel="drawer_menu_notification" style={styles.drawerItems} onPress={this.gotoReport}>
          <Text style={styles.drawerItemsLabel}>MAIN</Text>
        </TouchableOpacity>
        <TouchableOpacity accessibilityLabel="drawer_menu_notification" style={styles.drawerItems} onPress={this.goTo2048}>
          <Text style={styles.drawerItemsLabel}>2048</Text>
        </TouchableOpacity>
        <TouchableOpacity accessibilityLabel="drawer_menu_notification" style={styles.drawerItems} onPress={this.gotoFlap}>
          <Text style={styles.drawerItemsLabel}>FLAP BIRD</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerMenu);

const styles = StyleSheet.create({
  textStyle: {
    color: 'orange',
  },
  drawerItems: {
    flexDirection: 'row',
    paddingVertical: 24,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0, 0.16)',
    alignItems: 'center',
  },
});
