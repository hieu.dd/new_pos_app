import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import * as strings from '../resources/strings';
import { colors, screen, textStyles } from '../resources/styles/common';
import { scale } from '../utils/scaling';
import { TextInput } from 'react-native-gesture-handler';
import Space from '../components/Space';
import Button from '../components/button/Button';
import * as customerUtils from '../utils/customer';
import { connect } from 'react-redux';
import * as utils from '../utils';
import * as saleActions from '../stores/sale/actions';
import * as cartActions from '../stores/cart/actions';
import * as customerActions from '../stores/customer/actions';
import { TextField } from 'react-native-material-textfield';
import { Util } from 'teko-js-sale-library';
import TitleLabel from './label/TitleLabel';
import TrapezoidButton from '../components/button/TrapezoidButton';
import Price from '../components/common/Price';
import ChooseCustomerModal from '../components/modal/ChooseCustomerModal';

export class CustomerInformation extends Component {
  state = {
    id: 39146,
    customer_phone: '',
    customer_name: '',
    receiver_phone: '',
    receiver_name: '',
    receiver_province: '',
    receiver_province_code: '',
    receiver_district: '',
    receiver_district_code: '',
    receiver_street: '',
    receiver_commune: '',
    receiver_commune_code: '',
    receiver_date: moment()
      .add(moment().weekday() === 4 ? 4 : 3, 'd')
      .format('DD/MM/YYYY'),
    receiver_time: moment().format('hh:mm A'),
    input: 0,
    indexContact: -1,
    searchCustomerResults: [],
    visibleChooseCustomer: false,
  };

  componentDidMount() {
    if (this.props.tempCustomerInfo) {
      this.setState({
        ...this.props.tempCustomerInfo,
      });
    }
  }

  componentWillUnmount() {
    //do not save when tempCustomerInfo is null, this happens when create order success, we call cartAction.saveTempCustomerInfo(null)
    this.props.saveTempCustomerInfo(this.state);
  }

  onPress = () => {
    let customerInfo = customerUtils.mapCustomerInfoFromState(this.state);
    this.props.saveCustomerInfo(customerInfo);
    this.props.onContinue();
  };

  onChangePhone = text => {
    this.setState({
      customer_phone: text,
    });
  };

  onChangeName = text => {
    this.setState({
      customer_name: text,
    });
  };

  onAddNewAddress = () => {
    this.props.navigation.navigate('NewAddress', {
      customer_id: this.state.id,
      chooseContact: this.onChooseContact,
    });
  };

  onScanCustomer = () => {
    this.props.searchCustomer('0943310394', 'phone', () => {}, this.onChooseCustomer);
  };

  onSearchCustomer = () => {
    if (this.state.customer_phone) {
      this.props.searchCustomer(this.state.customer_phone, 'phone', this.onSearchResults, this.onChooseCustomer);
    }
  };

  onSearchResults = (hasFound, searchResults) => {
    if (hasFound) {
      this.setState({
        visibleChooseCustomer: true,
        modalType: 'customer',
        searchCustomerResults: searchResults,
      });
    }
  };

  renderChooseCustomer = () => {
    let searchText = this.state.customer_phone;
    return (
      <ChooseCustomerModal
        contacts={this.state.searchCustomerResults}
        visible={this.state.visibleChooseCustomer}
        searchText={searchText}
        onChooseCustomer={this.onChooseCustomer}
        onClose={this.onCloseSuggestion}
      />
    );
  };

  onCloseSuggestion = () => this.setState({ visibleChooseCustomer: false });

  goToListAddress = () => {
    this.props.navigation.navigate('ListAddress', {
      customer_id: this.state.id,
      chooseContact: this.onChooseContact,
      index: this.state.indexContact,
    });
  };

  onChooseContact = (contact, index) => {
    let receiver_province_code;
    let receiver_province;
    let receiver_district_code;
    let receiver_district;
    let receiver_commune;
    let receiver_commune_code;
    if (contact) {
      if (contact.province) {
        //some of customer in db has province Hà Tây
        if (contact.province === 'Hà Tây') contact.province = 'Hà Nội';
        let province = Util.GeoLoc.getProvinces().find(
          item => item.label === contact.province || item.key === contact.province
        );
        if (province) {
          receiver_province_code = province.key;
          receiver_province = province.label;
        }
      }
      if (contact.district && receiver_province_code) {
        let district = Util.GeoLoc.getDistrictsOfProvince(receiver_province_code).find(
          item => item.label === contact.district || item.key === contact.district
        );
        if (district) {
          receiver_district_code = district.key;
          receiver_district = district.label;
        }
      }

      if (contact.commune && receiver_district_code) {
        let commune = Util.GeoLoc.getCommunesOfDistrict(receiver_district_code).find(
          item => item.label === contact.commune || item.key === contact.commune
        );

        receiver_commune = commune.label;
        receiver_commune_code = commune.key;
      }
    }
    this.setState({
      receiver_province_code,
      receiver_province,
      receiver_district_code,
      receiver_district,
      receiver_street: contact && contact.address,
      receiver_commune,
      receiver_commune_code,
      indexContact: index,
    });
  };

  onChooseCustomer = customer => {
    let receiver_province_code;
    let receiver_province;
    let receiver_district_code;
    let receiver_district;
    let receiver_commune;
    let receiver_commune_code;
    let contact = customer.contacts && customer.contacts[0];
    let indexContact = -1;
    if (contact) {
      indexContact = 0;
      if (contact.province) {
        //some of customer in db has province Hà Tây
        if (contact.province === 'Hà Tây') contact.province = 'Hà Nội';
        let province = Util.GeoLoc.getProvinces().find(
          item => item.label === contact.province || item.key === contact.province
        );
        if (province) {
          receiver_province_code = province.key;
          receiver_province = province.label;
        }
      }
      if (contact.district && receiver_province_code) {
        let district = Util.GeoLoc.getDistrictsOfProvince(receiver_province_code).find(
          item => item.label === contact.district || item.key === contact.district
        );
        if (district) {
          receiver_district_code = district.key;
          receiver_district = district.label;
        }
      }
      if (contact.commune && receiver_district_code) {
        let commune = Util.GeoLoc.getCommunesOfDistrict(receiver_district_code).find(
          item => item.label === contact.commune || item.key === contact.commune
        );

        receiver_commune = commune.label;
        receiver_commune_code = commune.key;
      }
    }

    this.setState({
      visibleChooseCustomer: false,
      id: customer.id || 39146,
      customer_name: customer.name,
      customer_phone: customer.phone,
      receiver_name: customer.name,
      receiver_phone: customer.phone,
      receiver_province_code,
      receiver_province,
      receiver_district_code,
      receiver_district,
      receiver_street: contact && contact.address,
      receiver_commune,
      receiver_commune_code,
      indexContact,
    });
  };

  renderAddress = () => {
    let { searchResults } = this.props.customer;
    let { receiver_province_code, receiver_district_code, receiver_street, indexContact, id } = this.state;
    let contacts = searchResults.find(item => item.id === id) && searchResults.find(item => item.id === id).contacts;
    let contact = receiver_province_code &&
      receiver_district_code &&
      receiver_street && {
        province: receiver_province_code,
        district: receiver_district_code,
        address: receiver_street,
      };
    return (
      <View>
        <TitleLabel text={'Địa chỉ nhận hàng'} />

        <View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: scale(12),
              marginTop: scale(4),
              backgroundColor: 'white',
              padding: scale(12),
              borderColor: colors.secondary,
              borderWidth: scale(1),
            }}
          >
            <Icon name="map-marker" type="material-community" color={colors.primary} size={scale(16)} />

            <Text style={[textStyles.body1, { flex: 1, textAlignVertical: 'center', marginHorizontal: scale(12) }]}>
              {indexContact > -1 ? customerUtils.getFullAddressOfContact(contact) : 'Nhận hàng tại showroom'}
            </Text>
            <Icon name="check-circle" type="material-community" color={colors.secondary} size={scale(16)} />
          </View>
          {contacts && contacts[0] ? (
            <TouchableOpacity style={styles.rowAction} onPress={this.goToListAddress}>
              <Text style={[textStyles.small, styles.textAction]}>CHỌN ĐỊA CHỈ KHÁC</Text>
              <Icon name="chevron-right" type="meterial" color={colors.primary} size={scale(16)} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.rowAction} onPress={this.onAddNewAddress}>
              <Text style={[textStyles.small, styles.textAction]}>THÊM ĐỊA CHỈ MỚI</Text>
              <Icon name="add" type="meterial" color={colors.primary} size={scale(16)} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };
  renderButtomPrice = () => {
    let { finalInvoice } = this.props || (this.props.navigation && this.props.navigation.state.params);
    let { customer_name, customer_phone, receiver_province, receiver_district, indexContact } = this.state;
    let validateInfor =
      customer_name &&
      Util.Text.validateVNPhoneNumber(customer_phone) &&
      ((receiver_district && receiver_province) || indexContact === -1)
        ? true
        : false;
    return (
      <View
        style={[
          {
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
            left: 0,
            width: screen.width,
            height: scale(48),
          },
        ]}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: colors.dark_slate_blue,
            paddingHorizontal: scale(12),
            height: scale(48),
            justifyContent: 'center',
            paddingVertical: scale(12),
          }}
        >
          <Text style={[textStyles.footnote, { color: 'white' }]}>Thành tiền: </Text>
          <Price color={colors.squash} style={[textStyles.price]} price={finalInvoice} />
        </View>
        <View style={{ position: 'absolute', top: 0, right: 0 }}>
          <TrapezoidButton
            disabled={!validateInfor}
            color={colors.primary}
            title={'TIẾP TỤC'}
            textStyle={[textStyles.body1, { color: 'white', fontFamily: 'sale-text-bold' }]}
            width={scale(140)}
            height={scale(48)}
            left={scale(12)}
            iconSize={scale(16)}
            iconRightName="chevron-right"
            iconType="material-community"
            iconColor="white"
            onPress={this.onPress}
          />
        </View>
      </View>
    );
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.lightGray }}>
        <View>
          <TitleLabel text={'Thông tin khách hàng'} />
          <View
            style={{
              flexDirection: 'row',
              paddingBottom: scale(16),
              borderBottomColor: colors.pinkish_grey,
              borderBottomWidth: scale(0.5),
            }}
          >
            <View>
              <View style={styles.informationInput}>
                <Icon name="phone" type="material-community" color={colors.primary} size={scale(16)} />
                <TextInput
                  placeholder="Số điện thoại"
                  keyboardType="numeric"
                  tintColor={colors.primary}
                  textColor={'black'}
                  placeholderTextColor={colors.darkGray}
                  value={this.state.customer_phone}
                  underlineColorAndroid={'transparent'}
                  onChangeText={this.onChangePhone}
                  onEndEditing={this.onSearchCustomer}
                  style={[textStyles.body1, { paddingLeft: scale(12), flex: 1, textAlignVertical: 'center' }]}
                />
              </View>
              <View style={styles.informationInput}>
                <Icon name="account" type="material-community" color={colors.primary} size={scale(16)} />
                <TextInput
                  placeholder="Tên khách hàng"
                  tintColor={colors.primary}
                  textColor={'black'}
                  underlineColorAndroid={'transparent'}
                  placeholderTextColor={colors.darkGray}
                  value={this.state.customer_name}
                  onChangeText={this.onChangeName}
                  style={[textStyles.body1, { paddingLeft: scale(12), flex: 1, textAlignVertical: 'center' }]}
                />
              </View>
            </View>
            <TouchableOpacity onPress={this.onScanCustomer} style={styles.barcodeContainer}>
              <Icon name="qrcode" type="material-community" size={scale(32)} />
              <Text style={[textStyles.tiny, { color: colors.black, marginTop: scale(8) }]}>SCAN MÃ QR</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Space height={scale(4)} backgroundColor={colors.lightGray} />
        {this.renderAddress()}
        {this.renderButtomPrice()}
        {this.renderChooseCustomer()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    customer: state.customer,
    tempCustomerInfo: state.cart.tempCustomerInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveCustomerInfo: customerInfo => dispatch(cartActions.saveCustomerInfo(customerInfo)),
    searchCustomer: (search, fieldSearch, onSearchResult, onChooseCustomer) =>
      dispatch(customerActions.searchCustomer(search, fieldSearch, onSearchResult, onChooseCustomer)),
    saveTempCustomerInfo: state => dispatch(cartActions.saveTempCustomerInfo(state)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerInformation);

const styles = StyleSheet.create({
  informationInput: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: scale(242),
    height: scale(44),
    alignItems: 'center',
    paddingHorizontal: scale(12),
    marginHorizontal: scale(12),
    borderBottomWidth: scale(1),
    borderBottomColor: colors.lightGray,
  },
  rowAction: {
    alignItems: 'center',
    height: scale(44),
    flexDirection: 'row',
    marginHorizontal: scale(12),
    backgroundColor: 'white',
    paddingHorizontal: scale(12),
    marginTop: scale(10),
  },
  barcodeContainer: {
    height: scale(88),
    width: scale(88),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textAction: {
    flex: 1,
    textAlignVertical: 'center',
    fontFamily: 'sale-text-medium',
    color: colors.primary,
  },
});
